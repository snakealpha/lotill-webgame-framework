package LWGF.framework.uiComponent.tooltip
{
	import flash.geom.Point;
	
	import LWGF.framework.BaseComponent.DisplayComponent;
	
	/**
	 * 所有Tooltip的基类，提供基本的Tooltip支持功能。
	 */
	public class Tooltip extends DisplayComponent
	{
		public function Tooltip()
		{
			super();
		}
		
		/**
		 * Tooltip模式
		 */
		public function get mode():String
		{
			return TooltipMode.Normal;
		}
		
		/**
		 * 用于向Tooltip填入数据的只写属性
		 */
		protected var _data:Object;
		public function set data(value:Object):void
		{
			_data = value;
		}
		public function get data():Object
		{
			return _data;
		}
		
		/**
		 * 该方法将会在鼠标移出Holder的时候调用，对于Mode为Normal的Tooltip，该方法将永远不会被调用
		 */
		public function outRangeOfHolder():void
		{
			
		}
		
		/**
		 * 相对于鼠标指针位置的偏移
		 */
		protected var _offset:Point;
		public function get offset():Point
		{
			return _offset;
		}
	}
}