package LWGF.framework.uiComponent.tooltip
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	
	import LWGF.framework.BaseComponent.layer.Layer;
	import LWGF.framework.global.GlobalInfo;
	import LWGF.framework.utils.objectPool.ObjectPool;

	/**
	 * Tooltip管理器类，用于维护Tooltip的行为
	 */
	public class TooltipManager
	{
		protected static var _instance:TooltipManager;
		
		protected var _tooltipLayer:Layer;
		
		protected var _registeredObjects:Dictionary = new Dictionary();
		protected var _displayedTooltips:Dictionary = new Dictionary();
		
		public static function get instance():TooltipManager
		{
			if(_instance == null)
			{
				_instance = new TooltipManager();
			}
			
			return _instance;
		}
		
		public function TooltipManager()
		{
		}
		
		public function set tooltipLayer(value:Layer):void
		{
			_tooltipLayer = value;
		}
		
		public function addTooltip(object:ITooltipHolder, tooltipClass:Class):void
		{
			_registeredObjects[object] = tooltipClass;
			
			if(object is DisplayObject && (object as DisplayObject).parent == null)
			{
				object.addEventListener(Event.ADDED_TO_STAGE, onObjectAdded);
			}
			else
			{
				object.addEventListener(MouseEvent.ROLL_OVER, onMouseOver);
				
				object.addEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
			}
		}
		
		protected function onObjectAdded(event:Event):void
		{
			var object:ITooltipHolder = event.currentTarget as ITooltipHolder;
			object.addEventListener(MouseEvent.ROLL_OVER, onMouseOver);
			
			object.removeEventListener(Event.ADDED_TO_STAGE, onObjectAdded);
			object.addEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
		}
		
		protected function onRemoved(event:Event):void
		{
			var object:ITooltipHolder = event.currentTarget as ITooltipHolder;
			object.addEventListener(Event.ADDED_TO_STAGE, onObjectAdded);
			
			object.removeEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
			object.removeEventListener(MouseEvent.ROLL_OUT, onMouseOut);
			object.removeEventListener(MouseEvent.ROLL_OVER, onMouseOver);
			object.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
		}
		
		public function removeTooltip(object:ITooltipHolder):void
		{
			if(_displayedTooltips.hasOwnProperty(object))
			{
				hideTooltip(object);
				
				object.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
				object.removeEventListener(MouseEvent.ROLL_OUT, onMouseOut);
				object.removeEventListener(MouseEvent.ROLL_OVER, onMouseOver);
				object.removeEventListener(Event.ADDED_TO_STAGE, onObjectAdded);
				object.removeEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
			}
			
			delete _registeredObjects[object];
		}
		
		public function editTooltip(object:ITooltipHolder, position:Point, data:Object):void
		{
			if(!_displayedTooltips[object])
			{
				return;
			}
			
			var tooltip:Tooltip = _displayedTooltips[object] as Tooltip;
			
			if(position != null)
			{
				tooltip.x = position.x + tooltip.offset.x;
				tooltip.y = position.y + tooltip.offset.y;
			}
			
			if(data != null)
			{
				tooltip.data = data;
			}
		}
		
		public function displayTooltip(object:ITooltipHolder):void
		{
			if(/*_registeredObjects.hasOwnProperty(object) && */!_displayedTooltips.hasOwnProperty(object))
			{
				if(object.tooltipData == null)
				{
					return;
				}
				
				var tooltip:Tooltip = ObjectPool.instance.getObject(_registeredObjects[object] as Class) as Tooltip;
				_displayedTooltips[object] = tooltip;
				editTooltip(object, new Point(GlobalInfo.stage.mouseX, GlobalInfo.stage.mouseY), object.tooltipData);
				
				if(tooltip.mode == TooltipMode.Normal)
				{
					tooltip.mouseEnabled = false;
				}
				else
				{
					tooltip.mouseEnabled = true;
				}
				
				_tooltipLayer.addChild(tooltip);
				
				object.removeEventListener(MouseEvent.ROLL_OVER, onMouseOver);
				
				object.addEventListener(MouseEvent.ROLL_OUT, onMouseOut);
				if(tooltip.mode == TooltipMode.Normal)
				{
					object.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
				}
			}
		}
		
		public function hideTooltip(object:ITooltipHolder):void
		{
			_tooltipLayer.removeChild(_displayedTooltips[object] as DisplayObject);
			
			object.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			object.removeEventListener(MouseEvent.ROLL_OUT, onMouseOut);
			object.addEventListener(MouseEvent.ROLL_OVER, onMouseOver);
			
			ObjectPool.instance.disposeObject(_displayedTooltips[object], _registeredObjects[object]);
			delete _displayedTooltips[object];
		}
		
		protected function onMouseOver(event:MouseEvent):void
		{
			displayTooltip(event.currentTarget as ITooltipHolder);
		}
		
		protected function onMouseMove(event:MouseEvent):void
		{
			var tooltip:Tooltip = _displayedTooltips[event.currentTarget];
			
			if(tooltip.mode == TooltipMode.Normal)
			{
				editTooltip(event.currentTarget as ITooltipHolder, new Point(GlobalInfo.stage.mouseX, GlobalInfo.stage.mouseY), null);
			}
		}
		
		protected function onMouseOut(event:MouseEvent):void
		{
			var tooltip:Tooltip = _displayedTooltips[event.currentTarget];
			
			if(tooltip.mode == TooltipMode.Normal)
			{
				hideTooltip(event.currentTarget as ITooltipHolder);
			}
			else if(tooltip.mode == TooltipMode.Interactive)
			{
				tooltip.outRangeOfHolder();
			}
		}
	}
}