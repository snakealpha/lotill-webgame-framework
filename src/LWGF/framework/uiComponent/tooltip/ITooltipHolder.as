package LWGF.framework.uiComponent.tooltip
{
	import flash.events.IEventDispatcher;

	/**
	 * 实现为持有Tooltip的类的借口
	 */
	public interface ITooltipHolder extends IEventDispatcher
	{
		/**
		 * 获得用于显示的Tooltip的类型
		 */
		function get tooltipClass():Class;
		
		/**
		 * 获得用于显示的tooltip的数据
		 */
		function get tooltipData():Object;
	}
}