package LWGF.framework.uiComponent.tooltip
{
	/**
	 * 用于选择Tooltip的用途与行为
	 */
	public class TooltipMode
	{
		/**
		 * 标准类型Tooltip:
		 * -不响应鼠标事件
		 * -鼠标离开对象即刻消失
		 * -鼠标在对象上移动时，跟随鼠标
		 * -仅用于显示信息
		 */
		public static const Normal:String = "normal";
		
		/**
		 * 交互类型Tooltip：
		 * -响应鼠标事件
		 * -Tooltip类需要自行控制消失时机，管理器仅在鼠标离开对象时调用方法
		 * -鼠标在对象上移动时，Tooltip位置不会改变
		 * -使用于菜单和其他可交互面板
		 */
		public static const Interactive:String = "interactive";
		
		public function TooltipMode()
		{
		}
	}
}