package LWGF.framework.uiComponent
{
	import fl.controls.Button;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import LWGF.framework.uiComponent.tooltip.ITooltipHolder;
	import LWGF.framework.uiComponent.tooltip.TooltipManager;
	
	/**
	 * 订制按钮
	 * 能够使用MovieClip作为皮肤使用，可以简化从既有界面资源创建视图的步骤。
	 * 实现ITooltipHolder接口，用于支持Tooltip弹出。
	 */
	public class Button extends fl.controls.Button implements ITooltipHolder
	{
		protected var _swfSkin:MovieClip; 
		
		protected var _buttonStatus:uint = 1;
		
		public static const Up:uint = 1;
		public static const Over:uint = 2;
		public static const Down:uint = 3;
		public static const Disabled:uint = 4;
		public static const SelectedUp:uint = 5;
		public static const SelectedOver:uint = 6;
		public static const SelectedDown:uint = 7;
		public static const SelectedDisabled:uint = 8;
				
		public function Button()
		{
			super();
			
			buttonMode = true;
			mouseChildren = false;
		}
		
		public function setSwfSkin(swf:MovieClip):void
		{
			_swfSkin = swf;
			
			setStyle("upSkin", swf);
			setStyle("overSkin", swf);
			setStyle("downSkin", swf);
			setStyle("disabledSkin", swf);
			setStyle("selectedUpSkin", swf);
			setStyle("selectedOverSkin", swf);
			setStyle("selectedDownSkin", swf);
			setStyle("selectedDisabledSkin", swf);
			
			setSkin();
			setStatusListener();
			setSize(swf.width, swf.height);
		}
		
		protected function setStatusListener():void
		{
			addEventListener(MouseEvent.ROLL_OVER, 	function (event:MouseEvent):void
													{
														_buttonStatus = Over;
														registerChangeSkin();
													});
			addEventListener(MouseEvent.ROLL_OUT, 	function (event:MouseEvent):void
													{
														_buttonStatus = Up;
														registerChangeSkin();
													});
			addEventListener(MouseEvent.MOUSE_DOWN,	function (event:MouseEvent):void
													{
														_buttonStatus = Down;
														registerChangeSkin();
													});
			addEventListener(MouseEvent.MOUSE_UP,	function (event:MouseEvent):void
													{
														_buttonStatus = Over;
														registerChangeSkin();
													});
		}
		
		protected function registerChangeSkin():void
		{
			addEventListener(Event.ENTER_FRAME,	onChangeSkin);			
		}
		
		protected function onChangeSkin(event:Event):void
		{
			setSkin();
		}
		
		private function setSkin():void
		{
			var baseOffset:uint = 0;
			
			if(selected)
			{
				baseOffset = 4;
			}
			
			if(!enabled)
			{
				_swfSkin.gotoAndStop(Disabled + baseOffset);
			}
			else
			{
				_swfSkin.gotoAndStop(_buttonStatus + baseOffset);
			}
			
			if(hasEventListener(Event.ENTER_FRAME))
			{
				removeEventListener(Event.ENTER_FRAME, onChangeSkin);
			}
			
			label = "";
		}
		
		//用于支持基本的Tooltip功能的代码
		protected var _tooltipCreated:Boolean = false;
		
		protected var _tooltipClass:Class;
		public function get tooltipClass():Class
		{
			return _tooltipClass;
		}
		public function set tooltipClass(value:Class):void
		{
			_tooltipClass = value;
			
			if(_tooltipData != null)
			{
				if(value != null)
				{
					if(!_tooltipCreated)
					{
						TooltipManager.instance.addTooltip(this, _tooltipClass);
						_tooltipCreated = true;
					}
					TooltipManager.instance.editTooltip(this, null, tooltipData);
				}
				else
				{
					TooltipManager.instance.removeTooltip(this);
					_tooltipCreated = false;
				}
			}
		}
		
		protected var _tooltipData:Object;
		public function get tooltipData():Object
		{
			return _tooltipData;
		}
		public function set tooltipData(value:Object):void
		{
			_tooltipData = value;
			
			if(_tooltipClass != null)
			{
				if(value!= null)
				{
					if(!_tooltipCreated)
					{
						TooltipManager.instance.addTooltip(this, _tooltipClass);
						_tooltipCreated = true;
					}
					TooltipManager.instance.editTooltip(this, null, tooltipData);
				}
				else
				{
					TooltipManager.instance.removeTooltip(this);
					_tooltipCreated = false;
				}
			}
		}
	}
}