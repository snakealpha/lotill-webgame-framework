package LWGF.framework.uiComponent.checkButton
{
	import flash.events.Event;

	/**
	 * 按钮群组
	 * 非显示对象，仅为逻辑类
	 * 通过将CheckButton加入该类实现选项类的通用管理
	 */
	public class ButtonGroup
	{
		private var _buttons:Array = [];
		
		private var _isRadio:Boolean = false;
		
		/**
		 * @param isRadio 当该属性为true时，有且仅有一个按钮能够被选中
		 */
		public function ButtonGroup(isRadio:Boolean)
		{
			_isRadio = isRadio;
		}
		
		public function get isRadio():Boolean
		{
			return _isRadio;
		}
		
		public function addButton(value:CheckButton):void
		{
			_buttons.push(value);
			
			if(isRadio)
			{
				value.addEventListener(Event.CHANGE, onRadioButtonChanged);
			}
		}
		
		protected function onRadioButtonChanged(event:Event):void
		{
			for each(var button:CheckButton in _buttons)
			{
				button.changeWithoutDispatchEvent(false);
			}
			
			(event.currentTarget as CheckButton).changeWithoutDispatchEvent(true);
		}
		
		public function getButtonByIndex(index:uint):CheckButton
		{
			return _buttons[index] as CheckButton;
		}
		
		public function getSelectedData():Object
		{
			for each(var button:CheckButton in _buttons)
			{
				if(button.selected)
				{
					return button.data;
				}
			}
			return null;
		}
		
		public function getSelectedDatas():Array
		{
			var res:Array = [];
			
			for each(var button:CheckButton in _buttons)
			{
				if(button.selected)
				{
					res.push(button.data);
				}
			}
			
			return res;
		}
	}
}