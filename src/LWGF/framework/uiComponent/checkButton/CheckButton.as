package LWGF.framework.uiComponent.checkButton
{
	import LWGF.framework.uiComponent.Button;
	
	import flash.events.MouseEvent;

	/**
	 * 用以选择的按钮
	 * CheckButton和RadioButton皆可派生自该类
	 * 需要ButtonGroup类型支持以发挥作用
	 */
	public class CheckButton extends Button
	{
		protected var _data:Object;
		
		public function CheckButton()
		{
			super();
						
			label = "";
			buttonMode = false;
			mouseChildren = false;
			toggle = true;
		}
		
		public function set data(value:Object):void
		{
			_data = value;
		}
		
		public function get data():Object
		{
			return _data;
		}
		
		/**
		 * 该方法直接修改按钮的选中状态，但是不派发事件。
		 * 刷新皮肤的要求会被提出，以保证显示状态是正确的。
		 */
		public function changeWithoutDispatchEvent(value:Boolean):void
		{
			_selected = value;
			registerChangeSkin();
		}
	}
}