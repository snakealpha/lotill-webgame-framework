package LWGF.framework.uiComponent.utils
{
	import LWGF.framework.uiComponent.Button;
	import LWGF.framework.uiComponent.checkButton.ButtonGroup;
	import LWGF.framework.uiComponent.checkButton.CheckButton;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;

	public class ButtonUtils
	{
		public function ButtonUtils()
		{
		}
		
		/**
		 * 将一个MovieClip替换为使用其作为皮肤的按钮
		 */
		public static function replaceMovieClipWithButton(mc:MovieClip):Button
		{
			var button:Button = new Button();
			button.setSwfSkin(mc);
			
			if(mc.parent != null)
			{
				button.x = mc.x;
				button.y = mc.y;
				
				var container:DisplayObjectContainer = mc.parent;
				container.addChildAt(button, container.getChildIndex(mc));
				container.removeChild(mc);
			}
			
			return button;
		}
		
		/**
		 * 将一个MovieClip替换为使用其作为皮肤的可选择按钮
		 * @param mc	将要被替换的资源
		 * @param data	生成的CheckButton的data字段值
		 */
		public static function replaceMovieClipWithCheckedButton(mc:MovieClip, data:Object):CheckButton
		{
			var button:CheckButton = new CheckButton();
			button.setSwfSkin(mc);
			
			if(mc.parent != null)
			{
				button.x = mc.x;
				button.y = mc.y;
				
				var container:DisplayObjectContainer = mc.parent;
				container.addChildAt(button, container.getChildIndex(mc));
				container.removeChild(mc);
			}
			
			button.data = data;
			return button;
		}
		
		/**
		 * 将一系列MovieClip转换为CheckButton并置入同一个ButtonGroup中
		 * @param isRadio	ButtonGroup是否为单选的
		 * @param args		一系列对象的集合。
		 * 					每个对象均必须具有skin和data两个字段：skin字段类型为MovieClip，用于生成的单个CheckButton的皮肤；data字段类型为Object，作为生成的CheckButton的负载
		 */
		public static function getCheckButtonGroup(isRadio:Boolean, ...args):ButtonGroup
		{
			var group:ButtonGroup = new ButtonGroup(isRadio);
			for(var i:uint = 0; i != args.length; i++)
			{
				var checkButton:CheckButton = replaceMovieClipWithCheckedButton(args[i].skin, args[i].data);
				group.addButton(checkButton);
			}
			return group;
		}
	}
}