package LWGF.framework.BaseComponent.layer
{
	import LWGF.framework.BaseComponent.DisplayComponent;

	/**
	 * 层次类
	 */
	public class Layer extends DisplayComponent
	{
		public function Layer()
		{
			super();
		}
		
		/**
		 * 该方法清除层当中的所有显示对象
		 */
		public function clear():void
		{
			while(numChildren != 0)
			{
				removeChildAt(0);
			}
		}
	}
}