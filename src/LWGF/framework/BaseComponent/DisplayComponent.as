package LWGF.framework.BaseComponent
{
	import flash.display.Sprite;
	import flash.events.Event;
	
	import LWGF.framework.global.GlobalInfo;
	import LWGF.framework.uiComponent.tooltip.ITooltipHolder;
	import LWGF.framework.uiComponent.tooltip.TooltipManager;
	
	/**
	 * 图形类基本类
	 * 继承自Sprite，用于提供附加功能
	 */
	public class DisplayComponent extends Sprite implements ITooltipHolder
	{
		public function DisplayComponent()
		{
			super();
			
			hookEvents();
		}
		
		private function hookEvents():void
		{
			GlobalInfo.stage.addEventListener(Event.RESIZE, onStageResize);
		}
		
		private function onStageResize(event:Event):void
		{
			resize(GlobalInfo.stage.stageWidth, GlobalInfo.stage.stageHeight);
		}
		
		/**
		 * 当stage大小变化时调用，使用时应当重写
		 */
		protected function resize(width:Number, height:Number):void
		{
			
		}
		
		public function setSize(width:Number, height:Number):void
		{
			this.width = width;
			this.height = height;
		}
		
		//用于支持基本的Tooltip功能的代码
		protected var _tooltipCreated:Boolean = false;
		
		protected var _tooltipClass:Class;
		public function get tooltipClass():Class
		{
			return _tooltipClass;
		}
		public function set tooltipClass(value:Class):void
		{
			_tooltipClass = value;
			
			if(_tooltipData != null)
			{
				if(value != null)
				{
					if(!_tooltipCreated)
					{
						TooltipManager.instance.addTooltip(this, _tooltipClass);
						_tooltipCreated = true;
					}
					TooltipManager.instance.editTooltip(this, null, tooltipData);
				}
				else
				{
					TooltipManager.instance.removeTooltip(this);
					_tooltipCreated = false;
				}
			}
		}
		
		protected var _tooltipData:Object;
		public function get tooltipData():Object
		{
			return _tooltipData;
		}
		public function set tooltipData(value:Object):void
		{
			_tooltipData = value;
			
			if(_tooltipClass != null)
			{
				if(value!= null)
				{
					if(!_tooltipCreated)
					{
						TooltipManager.instance.addTooltip(this, _tooltipClass);
						_tooltipCreated = true;
					}
					TooltipManager.instance.editTooltip(this, null, tooltipData);
				}
				else
				{
					TooltipManager.instance.removeTooltip(this);
					_tooltipCreated = false;
				}
			}
		}
	}
}