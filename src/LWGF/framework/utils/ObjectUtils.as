package LWGF.framework.utils
{
	import flash.utils.getDefinitionByName;

	public class ObjectUtils
	{
		public function ObjectUtils()
		{
		}
		
		/**
		 * 字段拷贝方法，将source中的字段拷贝到target中，会进行适当的类型转换
		 */
		public static function putObject(source:Object, target:Object):void
		{
			for(var field:String in source)
			{
				if(target.hasOwnProperty(field))
				{
					var type:String = typeof(target[field]);
					if(type == "int" || type == "uint")
					{
						target[field] = parseInt(source[field]);
					}
					else if(type == "number")
					{
						target[field] = parseFloat(source[field]);
					}
					else if(type == "boolean")
					{
						target[field] = (source[field] != null && source[field] != "" && source[field] != "0" && source[field] != "false");
					}
					else if(target[field] is Array)
					{
						continue;
					}
					else
					{
						target[field] = source[field];
					}
				}
			}
		}
	}
}