package LWGF.framework.utils.objectPool
{
	internal class ClassCollection
	{
		private var _class:Class;
		private var _collection:Array = [];
		private var _limite:uint = 25;
		
		public function ClassCollection(classObj:Class)
		{
			_class = classObj;
		}
		
		public function get classObject():Class
		{
			return _class;
		}
		
		public function set limite(value:uint):void
		{
			_limite = value;
		}
		
		public function get limite():uint
		{
			return _limite;
		}
		
		public function push(value:Object):void
		{
			if(_collection.length >=_limite)
			{
				return;
			}
			
			_collection.push(value);
		}
		
		public function pop():Object
		{
			if(_collection.length > 0)
			{
				return _collection.pop();
			}
			else
			{
				var obj:Object = new _class();
				return obj;
			}
		}
	}
}