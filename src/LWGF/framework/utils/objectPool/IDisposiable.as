package LWGF.framework.utils.objectPool
{
	/**
	 * 若需要某类在向对象池释放时，能够预先对自身进行清理，需要实现改接口
	 */
	public interface IDisposiable
	{
		function dispose():void;
	}
}