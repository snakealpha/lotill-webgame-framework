﻿package LWGF.framework.utils.objectPool
{
	import flash.net.getClassByAlias;
	import flash.utils.Dictionary;
	import flash.utils.getDefinitionByName;

	/**
	 * 这是一个对象池
	 */
	public class ObjectPool
	{
		private static var _instance:ObjectPool;
		
		private var classPool:Dictionary = new Dictionary();
		
		public function ObjectPool()
		{
		}
		
		public static function get instance():ObjectPool
		{
			if(_instance == null)
			{
				_instance = new ObjectPool();
			}
			
			return _instance;
		}
		
		/**
		 * 从对象池中获取对象。注意，获取的对象可能含有上次使用时的数据。
		 */
		public function getObject(_class:Class):*
		{
			if(!classPool.hasOwnProperty(_class))
			{
				classPool[_class] = new ClassCollection(_class);
			}
			
			return (classPool[_class] as ClassCollection).pop();
		}
		
		/**
		 * 释放某个对象进入对象池
		 */
		public function disposeObject(object:Object, objClass:Class):void
		{
			var disposer:IDisposiable = object as IDisposiable;
			if(disposer != null)
			{
				disposer.dispose();
			}
			
			if(!classPool.hasOwnProperty(objClass))
			{
				classPool[objClass] = new ClassCollection(objClass);
			}
			(classPool[objClass] as ClassCollection).push(object);
		}
		
		/**
		 * 设置某类对象在对象池中保存实例的最大数量
		 * 默认为25
		 */
		public function setClassMaxNum(classObj:Class, maxNum:uint):void
		{
			if(!classPool.hasOwnProperty(classObj))
			{
				classPool[classObj] = new ClassCollection(classObj);
			}
			(classPool[classObj] as ClassCollection).limite = maxNum;
		}
	}
}