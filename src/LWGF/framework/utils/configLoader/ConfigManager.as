package LWGF.framework.utils.configLoader
{
	import BlackCatWorkshop.ASDispatcher.Dispatcher;
	
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	
	import LWGF.framework.utils.Log;
	import LWGF.framework.utils.ObjectUtils;
	import LWGF.framework.utils.assertManager.AssertManager;
	import LWGF.framework.utils.assertManager.LoadingInfo;
	
	/**
	 * 配置加载与管理器
	 */
	public class ConfigManager
	{		
		public static const ConfigLoaded:String = "config_loaded.";
		
		protected var configs:Dictionary = new Dictionary();
		
		public function ConfigManager()
		{
		}
		
		/**
		 * 加载指定
		 */
		public function load(configUrl:String):void
		{
			AssertManager.load(configUrl, LoadingInfo.Binary, onConfigLoaded);
		}
		
		protected function onConfigLoaded(info:LoadingInfo):void
		{
			var rawConfig:ByteArray = AssertManager.getBinary(info.url);
			rawConfig.uncompress();
			rawConfig.position = 0;
			
			var configStr:String = rawConfig.readMultiByte(rawConfig.bytesAvailable, "utf-8");
			configStr = configStr.replace(/^\s+/g, "");
			XML.ignoreWhitespace = true;
			var configXml:XML = new XML(configStr);
			
			for each(var tab:XML in configXml.children())
			{
				if(configs.hasOwnProperty(tab.name()))
				{
					Log.traceLog("Config Tab Type Conflict. Old Configs Will Be Covered.", "config");
				}
				
				configs[tab.name()] = [];
				for each(var node:XML in tab.children())
				{
					var tarObj:Object = new Object();
					
					for each(var field:XML in node.children())
					{
						tarObj[field.name()] = field.text().toString();
					}
					
					configs[tab.name()].push(tarObj);
				}
			}
			
			Dispatcher.dispatchEvent(ConfigLoaded, info.url, "config");
		}
	}
}