package LWGF.framework.utils.timer
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;

	/**
	 * 整合的时间管理类
	 */
	public class TimerManager
	{
		protected static var _instance:TimerManager;
		
		protected var _timer:Timer = new Timer(33);
		protected var _timeSpan:uint = 33;
		
		protected var _listeners:Array = [];
		
		public function TimerManager()
		{
			_timer.addEventListener(TimerEvent.TIMER, onTimer);
		}
		
		public static function get instance():TimerManager
		{
			if(_instance == null)
			{
				_instance = new TimerManager();
			}
			
			return _instance;
		}
		
		protected function onTimer(event:TimerEvent):void
		{
			loop();
		}
		
		protected function loop():void
		{
			for(var i:uint = 0; i != _listeners.length; i++)
			{
				var listener:TimerListener = _listeners[i] as TimerListener;
				listener.doneTime += _timeSpan;
				if(listener.doneTime >= listener.span)
				{
					listener.timerCallback();
					listener.doneTime = 0;
					listener.doneCount++;
					if(listener.count != 0 && listener.doneCount >= listener.count)
					{
						if(listener.compeleteCallback != null)
						{
							listener.compeleteCallback();
						}
						_listeners.splice(i, 1);
						i--;
					}
				}
			}
			
			if(_listeners.length == 0)
			{
				_timer.stop();
			}
		}
		
		/**
		 * 单个时间片长度，当然也就是时间管理器的最大精度，单位是毫秒
		 * 默认值是33，因此通过该计时器工作，能够保证30fps的动画播放能力
		 */
		public function get timeSpan():uint
		{
			return _timeSpan;
		}
		public function set timeSpan(value:uint):void
		{
			_timeSpan = value;
			
			_timer.delay = value;
		}
		
		/**
		 * 添加计时器。
		 * timerFunction将会在每个时间片完成时调用
		 * completeFunction将会在所有时间片任务完成时调用
		 * 若repeat设置为0，则计时器将不断重复调用
		 */
		public function registerTimer(span:uint, repeat:uint, timerFunction:Function, completeFunction:Function = null):void
		{
			var listener:TimerListener = new TimerListener(span, repeat, timerFunction, completeFunction);
			_listeners.push(listener);
			
			if(!_timer.running)
			{
				_timer.start();
			}
		}
		
		/**
		 * 根据重复间隔和回调返回是否存在定时器
		 */
		public function hasTimer(span:uint, timerFunction:Function):Boolean
		{
			for each(var listener:TimerListener in _listeners)
			{
				if(listener.span == span && listener.timerCallback == timerFunction)
				{
					return true;
				}
			}
			return false;
		}
		
		/**
		 * 取消计时器
		 */
		public function unregisterTimer(span:uint, repeat:uint, timerFunction:Function, completeFunction:Function = null):void
		{
			for(var i:uint = 0; i != _listeners.length; i++)
			{
				var listener:TimerListener = _listeners[i] as TimerListener;
				if(	listener.span == span &&
					listener.count == repeat &&
					listener.timerCallback == timerFunction &&
					listener.compeleteCallback == completeFunction)
				{
					_listeners.splice(i, 1);
					break;
				}
			}
			
			if(_listeners.length == 0)
			{
				_timer.stop();
			}
		}
	}
}