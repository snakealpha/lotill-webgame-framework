package LWGF.framework.utils.timer
{
	public class TimerListener
	{
		private var _span:uint;
		private var _count:uint;
		
		private var _doneCount:uint = 0;
		
		private var _doneTime:uint = 0;
		
		private var _timerCallback:Function;
		private var _completeCallback:Function;
		
		public function TimerListener(span:uint, count:uint = 1, timerCallback:Function = null, completeCallback:Function = null)
		{
			_span = span;
			_count = count;
			_timerCallback = timerCallback;
			_completeCallback = completeCallback;
		}
		
		public function get span():uint
		{
			return _span;
		}
		
		public function get count():uint
		{
			return _count;
		}
		
		public function get doneCount():uint
		{
			return _doneCount;
		}
		public function set doneCount(value:uint):void
		{
			_doneCount = value;
		}
		
		public function get timerCallback():Function
		{
			return _timerCallback;
		}
		
		public function get compeleteCallback():Function
		{
			return _completeCallback;
		}
		
		public function get doneTime():uint
		{
			return _doneTime;
		}
		public function set doneTime(value:uint):void
		{
			_doneTime = value;
		}
	}
}