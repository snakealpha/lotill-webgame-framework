package LWGF.framework.utils.collection
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;

	/**
	 * 含通知的数组类
	 * 该类在进行值的添加，删除和修改操作时，会发出一个CollectionChanged事件。
	 */
	public class NotifyArray extends Array implements IEventDispatcher
	{
		public static const CollectionChanged:String = "collection_changed";
		protected var dispatcher:EventDispatcher = new EventDispatcher();
		
		public function NotifyArray(...parameters)
		{
			super(parameters);
		}
		
		public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void
		{
			dispatcher.addEventListener(type, listener, useCapture, priority, useWeakReference);
		}
		
		public function removeEventListener(type:String, listener:Function, useCapture:Boolean = false):void
		{
			dispatcher.removeEventListener(type, listener, useCapture);
		}
		
		public function dispatchEvent(event:Event):Boolean
		{
			return dispatcher.dispatchEvent(event);
		}
		
		public function hasEventListener(type:String):Boolean
		{
			return dispatcher.hasEventListener(type);
		}
		
		public function willTrigger(type:String):Boolean
		{
			return dispatcher.willTrigger(type);
		}
		
		public function setValue(index:int, value:Object):void
		{
			dispatchEvent(new Event(CollectionChanged));
			this[index] = value;
		}
		
		override public function pop():*
		{
			dispatchEvent(new Event(CollectionChanged));
			return super.pop();
		}
		
		override public function push(...parameters):uint
		{
			dispatchEvent(new Event(CollectionChanged));
			return super.push(arguments);
		}
		
		override public function shift():*
		{
			dispatchEvent(new Event(CollectionChanged));
			return super.shift();
		}
		
		override public function sort(...parameters):*
		{
			dispatchEvent(new Event(CollectionChanged));
			return super.sort(arguments);
		}
		
		override public function sortOn(names:*, options:*=0, ...parameters):*
		{
			dispatchEvent(new Event(CollectionChanged));
			return super.sortOn(arguments);
		}
		
		override public function splice(...parameters):*
		{
			dispatchEvent(new Event(CollectionChanged));
			return super.splice(arguments);
		}
		
		override public function unshift(...parameters):uint
		{
			dispatchEvent(new Event(CollectionChanged));
			return super.unshift(arguments);
		}
	}
}