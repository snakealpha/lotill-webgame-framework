package LWGF.framework.utils.assertManager
{
	import BlackCatWorkshop.ASDispatcher.DataEvent;
	
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;
	
	import LWGF.framework.utils.Log;
	import LWGF.framework.utils.objectPool.IDisposiable;

	/**
	 * 单条加载项
	 */
	public class LoadingInfo extends EventDispatcher implements IDisposiable
	{
		protected var _fileUrl:String;
		
		protected var _type:String;
		// type的可能值
		public static const MovieClip:String = "movie_clip";
		public static const Image:String = "image";
		public static const Config:String = "config";
		public static const Xml:String = "xml";
		public static const Text:String = "text";
		public static const Binary:String = "binary";
		
		protected var _status:String;
		//status的可能值
		public static const Ready:String = "ready.";
		public static const Loading:String = "少女祈祷中...";
		public static const Completed:String = "completed.";
		
		public static const Started:String = "started";;
		public static const Progress:String = "progress";
		public static const Canceled:String = "canceled";
		public static const Failed:String = "failed";
		
		protected var _recentLogId:int = -1;
		
		protected var _data:Object;
		
		protected var _loader:URLLoader/* = new URLLoader()*/;
		protected var _displayLoader:Loader/* = new Loader()*/;
		protected var _process:Number = 0;
		
		protected var _callback:Function;
		protected var _isWeak:Boolean;
		
		public var _domain:ApplicationDomain = ApplicationDomain.currentDomain;
		
		public var extend:Object;
		
		public function LoadingInfo(fileUrl:String = "",
									type:String = "",
									callback:Function = null,
									isWeak:Boolean = false,
									extend:Object = null,
									domain:ApplicationDomain = null)
		{
			_fileUrl = fileUrl;
			_type = type;
			_callback = callback;
			
			_status = Ready;
			_isWeak = isWeak;
			this.extend = extend;
			if(domain != null)
				_domain = domain;
		}
		
		public function get url():String
		{
			return _fileUrl;
		}
		public function set url(value:String):void
		{
			_fileUrl = value;
		}
		
		public function get type():String
		{
			return _type;
		}
		public function set type(value:String):void
		{
			_type = value;
		}
		
		public function get status():String
		{
			return _status;
		}
		
		public function set data(value:Object):void
		{
			_data = value;
		}
		
		public function get data():Object
		{
			return _data;
		}
		
		public function get logId():int
		{
			return _recentLogId;
		}
		
		public function get process():Number
		{
			return _process;
		}
		
		public function get callback():Function
		{
			return _callback;
		}
		public function set callback(value:Function):void
		{
			_callback = value;
		}
		
		public function get isWeak():Boolean
		{
			return _isWeak;
		}
		
		public function get isDisplayObject():Boolean
		{
			if(_type == MovieClip || _type == Image)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		/**
		 * 启动加载
		 * toCoAppdomain为true时，显示对象会被加载到当前应用程序域中。
		 */
		public function start():void
		{
			var urlRequest:URLRequest = new URLRequest(_fileUrl);
						
			if(isDisplayObject)
			{
				loadDisplay(urlRequest);
			}
			else
			{
				loadNormal(urlRequest);
			}
		}
		
		protected function loadNormal(request:URLRequest):void
		{			
			var loadType:String = "";
			switch (_type)
			{
				case Config:
				case Binary:
				{
					loadType = URLLoaderDataFormat.BINARY;
					
					break;
				}
				case Text:
				case Xml:
				{
					loadType = URLLoaderDataFormat.TEXT;
					
					break;
				}
			}
			
			_loader = new URLLoader();
			
			_loader.dataFormat = loadType;
			
			_loader.addEventListener(Event.COMPLETE, onCompleted);
			_loader.addEventListener(IOErrorEvent.IO_ERROR, onIoError);
			_loader.addEventListener(ProgressEvent.PROGRESS, onProcess);
			_loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecrityError);
			
			_loader.load(request);
			
			_status = Loading;
		}
		
		protected function loadDisplay(request:URLRequest):void
		{
			_displayLoader = new Loader();
			
			_displayLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onCompleted);
			_displayLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onIoError);
			_displayLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onProcess);
			_displayLoader.contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecrityError);
						
			_displayLoader.load(request, new LoaderContext(false, _domain));
			
			_status = Loading;
		}
		
		protected function onCompleted(event:Event):void
		{
			if(isDisplayObject)
			{
				_data = _displayLoader.content;
			}
			else
			{
				_data = _loader.data;
			}
			_status = Completed;
			_recentLogId = Log.traceLog("loading for " + url + " is completed.", "loading");
			dispatchEvent(new DataEvent(Completed, this));
			_displayLoader = null;
			_loader = null;
		}
		protected function onIoError(event:IOErrorEvent):void
		{
			_status = Failed;
			_recentLogId = Log.traceLog("loading for " + url + " is failed for io error.", "loading");
			dispatchEvent(new DataEvent(Failed, this));
			
			_loader = null;
			_displayLoader = null;
		}
		protected function onProcess(event:ProgressEvent):void
		{
			_process = event.bytesLoaded / event.bytesTotal;
			_status = Loading;
			dispatchEvent(new DataEvent(Progress, this));
		}
		protected function onSecrityError(event:SecurityErrorEvent):void
		{
			_status = Failed;
			_recentLogId = Log.traceLog("loading for " + url + " is failed for security error.", "loading");
			dispatchEvent(new DataEvent(Failed, this));
			
			_loader = null;
			_displayLoader = null;
		}
		
		public function cancel():void
		{
			if(isDisplayObject)
			{
				_displayLoader.close();
			}
			else
			{
				_loader.close();
			}
			_status = Canceled;
			_recentLogId = Log.traceLog("loading for " + url + " is canceled.", "loading");
			dispatchEvent(new DataEvent(Canceled, this));
			
			_loader = null;
			_displayLoader = null;
		}
		
		public function dispose():void
		{			
			_status = Ready;
			_recentLogId = -1;
			_data = null;
			_process = 0;
			
			_loader = null;
			_displayLoader = null;
			extend = null;
		}
	}
}