package LWGF.framework.utils.assertManager
{
	import BlackCatWorkshop.ASDispatcher.DataEvent;
	import BlackCatWorkshop.ASDispatcher.Dispatcher;
	
	import com.greensock.plugins.CacheAsBitmapPlugin;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.system.ApplicationDomain;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import flash.utils.getDefinitionByName;
	
	import LWGF.framework.utils.ScaleBitmap;

	/**
	 * 队列加载器
	 * 在若干队列中，优先加载loadingQueue中索引值较小的。
	 * 使用基于优先级的非抢断式并行加载，优先级分为5级。
	 */
	public class AssertManager
	{
		protected static var _instance:AssertManager;
		
		protected var _loadedResource:Dictionary = new Dictionary();
		protected var _loadedWeakReferenceResource:Dictionary = new Dictionary(true);
		protected var _cachedResuableInstance:Dictionary = new Dictionary(true);
		
		protected var _loadingQueue:Array = [];
		protected var _concurrentMaxNum:uint = 5;
		protected var _concurrentNum:uint = 0;
		
		public function AssertManager()
		{
			for(var i:uint = 0; i != 5; i++)
			{
				_loadingQueue.push([]);
			}
		}
		
		public static function get instance():AssertManager
		{
			if(_instance == null)
			{
				_instance = new AssertManager();
			}
			
			return _instance;
		}
		
		public function get concurrentNum():uint
		{
			return _concurrentMaxNum;
		}
		
		public function set concurrentNum(value:uint):void
		{
			_concurrentMaxNum = value;
		}
		
		protected function getNextLoadingInfo():LoadingInfo
		{
			var result:LoadingInfo;
			
			for(var queueId:uint = 0; queueId != _loadingQueue.length; queueId++)
			{
				var queue:Array = _loadingQueue[queueId] as Array;
				if(queue.length != 0)
				{
					return queue.shift() as LoadingInfo;
				}
			}
			
			return null;
		}
		
		protected function runNext():Boolean
		{
			var info:LoadingInfo = getNextLoadingInfo();
			if(info != null)
			{
				info.addEventListener(LoadingInfo.Completed, onLoadingFinished); 
				info.addEventListener(LoadingInfo.Progress, onLoadingProcess);
				info.addEventListener(LoadingInfo.Canceled, onFail);
				info.addEventListener(LoadingInfo.Failed, onFail);
				
				info.start();
				return true;
			}
			return false;
		}
		
		protected function onFail(event:Event):void
		{
			_concurrentNum--;
		}
		
		protected function onLoadingFinished(event:DataEvent):void
		{
			var info:LoadingInfo = event.data as LoadingInfo;
			
			if(event.type == LoadingInfo.Completed)
			{
				if(info.type == LoadingInfo.Image)
				{
					if(info.isWeak)
					{
						_loadedWeakReferenceResource[(info.data as Bitmap).bitmapData] = info.url;
					}
					else
					{
						_loadedResource[info.url] = (info.data as Bitmap).bitmapData;
					}
				}
				else
				{
					if(info.isWeak)
					{
						_loadedWeakReferenceResource[info.data] = info.url;
					}
					else
					{
						_loadedResource[info.url] = info.data;
					}
				}
			}
			
			if(info.callback != null)
			{
				info.callback(info);
			}
			
			info.removeEventListener(LoadingInfo.Completed, onLoadingFinished);
			info.removeEventListener(LoadingInfo.Progress, onLoadingProcess);
			info.removeEventListener(LoadingInfo.Canceled, onFail);
			info.removeEventListener(LoadingInfo.Failed, onFail);
			for each(var queue:Array in _loadingQueue)
			{
				for(var i:uint = 0; i != queue.length; i++)
				{
					if(queue[i] == info)
					{
						queue.splice(i, 1);
						i--;
					}
				}
			}
			
			_concurrentNum--;
			
			run();
		}
		
		protected function onLoadingProcess(event:DataEvent):void
		{
			Dispatcher.dispatchEvent(LoadingInfo.Progress, event.currentTarget, "resource")
		}
		
		protected function run():void
		{
			while(_concurrentNum < _concurrentMaxNum)
			{
				if(runNext())
				{
					_concurrentNum++;
				}
				else
				{
					break;
				}
			}
		}
		
		/**
		 * 将一个加载项加入加载队列。
		 * 不能保证立刻开启加载。
		 * @param url		欲加载的的资源的URL
		 * @param type		欲加载的资源的类型
		 * @param callback	加载完成之后将会执行的回掉，必须接受一个LoadingInfo作为参数
		 * @param priority	加载的优先级
		 */
		public static function load(url:String, type:String, callback:Function = null, priority:uint = 2, extend:Object = null, isWeak:Boolean = false, domain:ApplicationDomain = null):void
		{
			var info:LoadingInfo = new LoadingInfo(url, type, callback, isWeak, extend, domain);
			
			if(instance._loadedResource.hasOwnProperty(url))
			{
				info.data = instance._loadedResource[url];
				info.callback(info);
				return;
			}
			else if(instance.isInWeekPool(url))
			{
				info.data = instance.getFromWeakPool(url);
				info.callback(info);
				return;
			}
			
			instance._loadingQueue[priority].push(info);
			
			instance.run();
		}
		
		/**
		 * 通过url获得指定对象的方法群
		 */
		protected function getData(url:String):Object
		{
			if(_loadedResource.hasOwnProperty(url))
			{
				return _loadedResource[url];
			}
			else if(isInWeekPool(url))
			{
				return getFromWeakPool(url);
			}
			else
			{
				return null;
			}
		}
		
		public static function getBitmapData(url:String):BitmapData
		{
			if(instance._loadedResource.hasOwnProperty(url) || instance.isInWeekPool(url))
			{
				return instance.getData(url) as BitmapData;
			}
			else
			{
				return getInstance(url, true) as BitmapData;
			}			
		}
		
		public static function getBitmap(url:String):Bitmap
		{
			if(instance._loadedResource.hasOwnProperty(url) || instance.isInWeekPool(url))
			{
				return new Bitmap(instance.getData(url) as BitmapData);
			}
			else
			{
				return new Bitmap(getInstance(url, true) as BitmapData);
			}			
		}
		
		public static function getScaleBitmap(url:String):ScaleBitmap
		{
			if(instance._loadedResource.hasOwnProperty(url) || instance.isInWeekPool(url))
			{
				return new ScaleBitmap(instance.getData(url) as BitmapData);
			}
			else
			{
				return new ScaleBitmap(getInstance(url, true) as BitmapData);
			}
		}
		
		public static function getMovieClip(url:String):MovieClip
		{
			if(instance._loadedResource.hasOwnProperty(url) || instance.isInWeekPool(url))
			{
				return instance.getData(url) as MovieClip;
			}
			else
			{
				return getInstance(url) as MovieClip;
			}			
		}
		
		public static function getText(url:String):String
		{
			return instance.getData(url) as String;
		}
		
		public static function getBinary(url:String):ByteArray
		{
			return instance.getData(url) as ByteArray;
		}
		
		public static function getXml(url:String):XML
		{
			var rawStr:String = instance._loadedResource[url] as String;
			rawStr = rawStr.replace(/<\?xml.*$/, "");
			return new XML(rawStr);
		}
		
		public static function getInstance(type:String, reusable:Boolean = false):Object
		{
			if(reusable == true)
			{
				if(instance._cachedResuableInstance.hasOwnProperty(type))
				{
					return instance._cachedResuableInstance[type];
				}
			}
			
			var classObj:Class = getDefinitionByName(type) as Class;
			if(classObj != null)
			{
				var instanceObj:Object = new classObj();
				
				if(reusable)
				{
					instance._cachedResuableInstance[type] = instanceObj;
				}
				
				return instanceObj;
			}
			else
			{
				return null;
			}
		}
		
		/**
		 * 确认对象某URL对应的对象是否存在于弱引用池中。
		 * 因为弱引用字典构造和普通字典相反，故需要使用特定方法判断。
		 */
		protected function isInWeekPool(url:String):Boolean
		{
			for(var payload:Object in _loadedWeakReferenceResource)
			{
				if((_loadedWeakReferenceResource[payload] as String) == url)
				{
					return true;
				}
			}
			return false;
		}
		
		/**
		 * 从弱引用池中获得指定url对象。
		 * 因为弱引用字典构造和普通字典相反，故需要使用特定方法。
		 */
		protected function getFromWeakPool(url:String):Object
		{
			for(var payload:Object in _loadedWeakReferenceResource)
			{
				if((_loadedWeakReferenceResource[payload] as String) == url)
				{
					return payload;
				}
			}
			return null;
		}
	}
}