package LWGF.framework.utils
{
	import flash.utils.Dictionary;

	/**
	 * 日志类
	 */
	public class Log
	{
		protected static var _instance:Log;
		
		protected var channels:Dictionary = new Dictionary();
		
		public function Log()
		{
		}
		
		protected static function get me():Log
		{
			if(_instance == null)
			{
				_instance = new Log();
			}
			
			return _instance;
		}
		
		/**
		 * 添加一条追踪日志，返回日志在相应频道中的序号
		 */
		public static function traceLog(log:String, channel:String = "default"):int
		{
			trace(log);
			
			if(!me.channels.hasOwnProperty(channel))
			{
				me.channels[channel] = [];
			}
			
			me.channels[channel].push(log);
			return me.channels[channel].length - 1;
		}
		
		public static function getLog(channel:String = "default", index:int = -1):String
		{
			if(!me.channels.hasOwnProperty(channel))
			{
				return "";
			}
			
			var logs:Array = me.channels[channel] as Array;
			if(index == -1)
			{
				return channel + " Log Index" + (logs.length - 1).toString() + ": " + logs[logs.length - 1] as String;
			}
			else
			{
				return channel + " Log Index" + index.toString() + ": " + logs[index];
			}
		}
	}
}