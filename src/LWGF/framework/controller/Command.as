package LWGF.framework.controller
{
	/**
	 * 命令实体类
	 * 单条命令由命令条文和参数共同构成。
	 */
	public class Command
	{
		private var _command:String;
		private var _argument:Object;
		
		public function Command(command:String, argument:Object)
		{
			_command = command;
			_argument = argument;			
		}
		
		public function get command():String
		{
			return _command;
		}
		
		public function get argument():Object
		{
			return _argument;
		}
	}
}