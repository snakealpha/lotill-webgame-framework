package LWGF.framework.controller
{
	import BlackCatWorkshop.ASDispatcher.DataEvent;
	import BlackCatWorkshop.ASDispatcher.Dispatcher;
	
	import com.protobuf.Message;
	
	import flash.utils.Dictionary;
	import flash.utils.getDefinitionByName;
	
	import LWGF.framework.data.Config;
	import LWGF.framework.data.Entity;
	import LWGF.framework.network.WrappedSocket;
	import LWGF.framework.utils.assertManager.LoadingInfo;
	import LWGF.framework.view.View;

	/**
	 * 控制器基类，同时亦作为单个模块的主轴。
	 * 应为每个模块继承并扩展该类。
	 */
	public class Controller
	{		
		protected var _view:View;
		
		protected var _viewResourceLoaded:Boolean = false;
		
		protected var _cachedUpdates:Array = [];
		
		/**
		 * 控制器中应包含如下三个类对应模块的派生类
		 * 因AS3不支持泛型之故，须在继承控制器时手工向控制器中添加三个派生类
		 */
//		protected var _configs:Config;
//		protected var _entity:Entity;		
//		protected var _command:CommandListener;
		
		public function Controller()
		{
			Dispatcher.addEventListener(WrappedSocket.MessageArrived, onMessageArrived, ModuleName);
		}
		
		private function onMessageArrived(event:DataEvent):void
		{
			message(event.data as Message);
		}
		
		/**
		 * 消息抵达时的处理代码
		 * 需重写
		 * 接收消息的Endpoint中的process方法要比该方法优先获得消息的处理权，而该方法运行时机和Entity.messageArrived时机近似
		 */
		protected function message(msg:Message):void
		{
			
		}
		
		/**
		 * 更新方法
		 * 这是除绑定之外操作视图的唯一方法，当视图没有建立时，操作会被缓存直到视图建立之后执行
		 * command是用于说明要求视图执行何许操作的命令
		 * argument是操作视图的参数
		 */
		public function updateView(command:String, argument:Object):void
		{
			if(_view == null)
			{
				_cachedUpdates.push({command:command, argument:argument});
			}
			else
			{
				_view.update(command, argument);
			}
		}
		
		/**
		 * 获取基本视图类名，初始化视图时使用
		 * 当模块没有附加视图时，应留此属性为空。然而，即便只有一个规模任意小的视图，仍然应当重写该方法。
		 */
		public function get viewClass():Class
		{
			return null;
		}
		
		/**
		 * 该方法返回模块的名字，用于生成Command与NetDispatcher使用的内部事件的频道名称。
		 * 同模块控制器，视图和分配器应当返回相同的模块名
		 */
		public function get ModuleName():String
		{
			return "";
		}
		
		/**
		 * 建立绑定。只在模块初始化之后调用。
		 * 必须重写以创建关联。
		 */
		protected function initBindings():void
		{
			
		}
		
		/**
		 * 视图是否处于显示状态
		 */
		public function get isShown():Boolean
		{
			if(_view == null)
			{
				return false;
			}
			
			return _view.parent != null;
		}
		
		/**
		 * 初始化视图之前进行调用
		 * 注意，当调用该方法时，模块的资源还没加载，且视图类没有实例化
		 */
		public function initView():void
		{
			
		}
		
		/**
		 * 初始化视图之后进行调用
		 * 即便该方法被调用，onOpenedView方法依然会在此方法被调用之后使用
		 */
		public function onCreatedView():void
		{
			initBindings();
			while(_cachedUpdates.length != 0)
			{
				var command:Object = _cachedUpdates.shift();
				_view.update(command.command, command.argument);
			}
		}
		
		protected function createNewView():void
		{
			_view = new viewClass();
			onCreatedView();
		}
		
		/**
		 * 每次打开视图之前调用
		 */
		public function onOpenView():void
		{
			
		}
		
		public function showView():void
		{
			if(_view == null)
			{
				initView();
				if(_viewResourceLoaded == false)
				{
					loadModuleResource();
				}
				else
				{
					createNewView();
					onOpenView();
					_view.show();
				}
			}
			else
			{
				onOpenView();
				_view.show();
			}
		}
		
		public function hideView():void
		{
			_view.hide();
		}
		
		protected function loadModuleResource():void
		{
			
		}
		
		protected function onModuleResourceLoaded(info:LoadingInfo):void
		{
			
		}
	}
}