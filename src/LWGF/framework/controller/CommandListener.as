package LWGF.framework.controller
{
	import BlackCatWorkshop.ASDispatcher.DataEvent;
	import BlackCatWorkshop.ASDispatcher.Dispatcher;
	
	import LWGF.framework.FrameworkConsts;

	/**
	 * 接受视图命令，实施用户行为的类
	 * 重写该类以提供实际的命令行为
	 */
	public class CommandListener
	{		
		public function CommandListener()
		{
			Dispatcher.addEventListener(FrameworkConsts.ViewCommandDispatched, onCommandArrived, moduleName);
		}
		
		protected function get moduleName():String
		{
			return "";
		}
		
		private function onCommandArrived(event:DataEvent):void
		{
			excuteCommand(event.data as Command);
		}
		
		protected function get controller():Controller
		{
			return null;
		}
		
		/**
		 * 实际用于处理命令的方法。
		 * 重写该方法以执行命令。
		 */
		protected function excuteCommand(command:Command):void
		{
			
		}
	}
}