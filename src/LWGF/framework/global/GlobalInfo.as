package LWGF.framework.global
{
	import flash.display.Stage;
	import flash.system.ApplicationDomain;
	
	/**
	 * 适用于可能要移出Starling支持的情况：
	 * 该类包含Starling支持代码
	 */
	public class GlobalInfo
	{
		public static var stage:Stage;
		
		public static var arguments:Object;
//		
//		public static var starlingContext:Starling;
		
		public static var mapResourceDomain:ApplicationDomain = new ApplicationDomain();
		
		public function GlobalInfo()
		{
		}
	}
}