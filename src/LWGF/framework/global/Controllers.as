package LWGF.framework.global
{
	import flash.utils.Dictionary;
	
	import LWGF.framework.controller.Controller;

	/**
	 * 游戏中所有模块的控制器的聚合
	 */
	public class Controllers
	{		
		protected var controllers:Dictionary = new Dictionary(); 
		
		public function Controllers()
		{
			initControllers();
		}
		
		/**
		 * 初始化游戏中的所有控制器
		 */
		protected function initControllers():void
		{
			
		}
		
		/**
		 * 通过模块名获得模块控制器
		 */
		public function getControllerByModuleName(moduleName:String):Controller
		{
			return controllers[moduleName];
		}
		
		public function addController(controller:Controller):void
		{
			controllers[controller.ModuleName] = controller;
		}
	}
}