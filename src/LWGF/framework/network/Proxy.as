package LWGF.framework.network
{
	import com.protobuf.Message;

	public class Proxy
	{
		protected var socket:WrappedSocket;
		
		public function Proxy(socket:WrappedSocket)
		{
			this.socket = socket;
		}
		
		protected function send(msg:Message):void
		{
			socket.sendMessage(msg);
		}
	}
}