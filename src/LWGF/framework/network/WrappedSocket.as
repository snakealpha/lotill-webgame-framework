package LWGF.framework.network
{
	import BlackCatWorkshop.ASDispatcher.DataEvent;
	
	import com.protobuf.Message;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.Socket;
	import flash.utils.ByteArray;
	import flash.utils.Endian;

	public class WrappedSocket extends EventDispatcher
	{
		public static const MessageArrived:String = "a message has been arrived.";
		
		public static const Connected:String = "connected.";
		public static const Closed:String = "closed.";
		public static const Error:String = "error.";
		
		protected var _socket:Socket = new Socket();		
		protected var _recentLogId:int = -1;		
		protected var _channel:String;
		protected var _targetClass:Class;
		
		public function WrappedSocket(channel:String, targetClass:Class)
		{
			_channel = channel;
			_targetClass = targetClass;
			
			_socket.addEventListener(Event.CONNECT, onConnected);
			_socket.addEventListener(ProgressEvent.SOCKET_DATA, onDataArrived);
			_socket.addEventListener(Event.CLOSE, onClosed);
			_socket.addEventListener(IOErrorEvent.IO_ERROR, onIoError);
			_socket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityError);
		}
		
		public function get channel():String
		{
			return _channel;
		}
		
		public function get connected():Boolean
		{
			return _socket.connected;
		}
		
		public function get recentLogId():int
		{
			return _recentLogId;
		}
		
		protected function onConnected(event:Event):void
		{
			dispatchEvent(new DataEvent(Connected));
		}
		
		protected function getMessage():Object
		{
			var msg:Message = new _targetClass();
			msg.mergeFrom(_socket);
			return msg;
		}
		
		public function connect(host:String, port:int):void
		{
			_socket.connect(host, port);
		}
		
		public function disconnect():void
		{
			_socket.close();
		}
		
		public function sendMessage(msg:Message):void
		{
			var buffer:ByteArray = new ByteArray();
			msg.writeTo(buffer);
			_socket.endian = Endian.BIG_ENDIAN;
			_socket.writeShort(buffer.length + 2);
			_socket.writeBytes(buffer);
			_socket.flush();
		}
		
		protected var pendingMessages:Array = [];
		protected function onDataArrived(event:ProgressEvent):void
		{
			while(_socket.bytesAvailable != 0)
			{
				pendingMessages.push(getMessage());				
			}
			
			while(pendingMessages.length != 0)
			{
				dispatchEvent(new DataEvent(MessageArrived, pendingMessages.shift()));
			}
		}
		
		protected function onClosed(event:Event):void
		{
			dispatchEvent(new DataEvent(Closed));
		}
		
		protected function onIoError(event:IOErrorEvent):void
		{
			dispatchEvent(new DataEvent(Error, event.errorID));
		}
		
		protected function onSecurityError(event:SecurityErrorEvent):void
		{
			dispatchEvent(new DataEvent(Error, event.errorID));
		}
	}
}