package LWGF.framework.network
{
	import BlackCatWorkshop.ASDispatcher.Dispatcher;
	
	import com.protobuf.Message;

	/**
	 * 消息端点
	 * 用于接收，处理和转发网络消息包
	 */
	public class MessageEndpoint
	{		
		public function MessageEndpoint()
		{
		}
		
		/**
		 * 返回所有支持的消息ID
		 */
		public function get supportMessageTypes():Array
		{
			return [];
		}
		
		/**
		 * 该方法返回模块的名字，用于生成Command使用的内部事件的频道名称。
		 * 同模块控制器，视图和分配器应当返回相同的模块名
		 */
		public function get moduleName():String
		{
			return "";
		}
		
		public function process(msg:Message):void
		{
			Dispatcher.dispatchEvent(WrappedSocket.MessageArrived, msg, moduleName);
		}
	}
}