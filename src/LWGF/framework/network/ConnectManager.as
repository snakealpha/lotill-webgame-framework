package LWGF.framework.network
{
	import BlackCatWorkshop.ASDispatcher.DataEvent;
	
	import com.protobuf.Message;
	
	import flash.utils.Dictionary;
	
	import LWGF.framework.utils.Log;

	/**
	 * 连接管理器
	 */
	public class ConnectManager
	{
		protected var _sockets:Dictionary = new Dictionary();
		protected var _dispatcher:Dictionary = new Dictionary();
		
		public function ConnectManager()
		{
		}
		
		public function addSocket(channel:String, messageClass:Class):void
		{
			var socket:WrappedSocket = new WrappedSocket(channel, messageClass);
			_sockets[channel] = socket;
			
			socket.addEventListener(WrappedSocket.Connected, onConnected);
			socket.addEventListener(WrappedSocket.MessageArrived, onMessage);
			socket.addEventListener(WrappedSocket.Closed, onClosed);
			socket.addEventListener(WrappedSocket.Error, onError);
		}
		
		public function addDispatcher(dispatcher:MessageEndpoint):void
		{
			for each(var msgId:uint in dispatcher.supportMessageTypes)
			{
				if(!_dispatcher.hasOwnProperty(msgId))
				{
					_dispatcher[msgId] = [];
				}
				
				(_dispatcher[msgId] as Array).push(dispatcher);
			}
		}
		
		public function getSocket(channel:String):WrappedSocket
		{
			return _sockets[channel] as WrappedSocket;
		}
		
		/**
		 * 分配消息...针对不同消息基本结构，需要重写
		 */
		protected function dispatch(msg:Message):void
		{
			
		}
		
		protected function onConnected(event:DataEvent):void
		{
			var socket:WrappedSocket = event.currentTarget as WrappedSocket;
			Log.traceLog(socket.channel + " connected.", "network");
		}
		
		protected function onMessage(event:DataEvent):void
		{			
			var socket:WrappedSocket = event.currentTarget as WrappedSocket;
			Log.traceLog(socket.channel + " received packet.", "network");
			
			dispatch(event.data as Message);
		}
		
		protected function onClosed(event:DataEvent):void
		{
			var socket:WrappedSocket = event.currentTarget as WrappedSocket;
			Log.traceLog(socket.channel + " closed.", "network");
		}
		
		protected function onError(event:DataEvent):void
		{
			var socket:WrappedSocket = event.currentTarget as WrappedSocket;
			Log.traceLog(socket.channel + " failed.", "network");
		}
	}
}