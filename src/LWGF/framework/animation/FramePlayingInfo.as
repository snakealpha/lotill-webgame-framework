package LWGF.framework.animation
{
	/**
	 * 供动画计时器使用的信息类
	 */
	internal final class FramePlayingInfo
	{
		public var frameTime:uint = 0;
		public var passedTime:uint = 0;
		
		public function FramePlayingInfo()
		{
		}
	}
}