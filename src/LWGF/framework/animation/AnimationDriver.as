package LWGF.framework.animation
{
	/**
	 * 用于实现逻辑驱动的动画的类
	 * 对于已经指定驱动器的位图动画，会在每一帧播放时尝试通过该类取得下一帧位于原图上的坐标和帧的经过时间，
	 * 并在动画播放完成时从该类调用完成处理方法。
	 */
	public class AnimationDriver
	{	
		public function AnimationDriver()
		{
		}
		
		/**
		 * 获取下一显示帧的帧信息
		 * currentFrame为需要返回的帧的帧数
		 */
		public function getFrame(currentFrame:uint, loop:Boolean = false):FrameInfomation
		{
			return null
		}
		
		/**
		 * 当动画所有帧播放完成时调用此方法
		 */
		public function animationFinal():void
		{
			
		}
		
		/**
		 * 动画的最大帧数
		 */
		public function get maxFrame():uint
		{
			return 0;
		}
	}
}