package LWGF.framework.animation.defaultDrivers
{
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	
	import LWGF.framework.animation.AnimationDriver;
	import LWGF.framework.animation.FrameInfomation;
	import LWGF.framework.utils.assertManager.AssertManager;
	
	public class MultiFileAnimationDriver extends AnimationDriver
	{
		protected var _images:Array = [];
		protected var _stayTime:uint = 80;
		
		public function MultiFileAnimationDriver()
		{
			super();
		}
		
		/**
		 * 设置每帧图像的资源名称
		 */
		public function set images(value:Array):void
		{
			_images = value;
		}
		
		/**
		 * 设置动画的帧率
		 */
		public function set frameRate(value:uint):void
		{
			_stayTime = 1000/value;
		}
		
		override public function getFrame(currentFrame:uint, loop:Boolean=false):FrameInfomation
		{
			var bm:BitmapData
			
			if(loop)
			{
				bm = AssertManager.getBitmapData(_images[(currentFrame - 1) % maxFrame]);
				return new FrameInfomation(new Rectangle(0, 0, bm.width, bm.height), _stayTime);
			}
			else if(currentFrame > maxFrame)
			{
				return null;
			}
			else
			{
				bm = AssertManager.getBitmapData(_images[(currentFrame - 1) % maxFrame]);
				return new FrameInfomation(new Rectangle(0, 0, bm.width, bm.height), _stayTime);
			}
		}
		
		override public function get maxFrame():uint
		{
			return _images.length;
		}
	}
}