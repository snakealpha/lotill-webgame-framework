package LWGF.framework.animation.defaultDrivers
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import LWGF.framework.animation.AnimationDriver;
	import LWGF.framework.animation.FrameInfomation;
	
	public class SimpleAnimationDriver extends AnimationDriver
	{
		protected var _width:uint = 0;
		protected var _height:uint = 0;
		protected var _stayTime:uint = 80;
		protected var _maxFrame:uint = 0;
		
		protected var _positionOffset:Point = new Point(0, 0);
		
		public function SimpleAnimationDriver()
		{
			super();
		}
		
		public function set maxFrame(value:uint):void
		{
			_maxFrame = value;
		}
		
		override public function get maxFrame():uint
		{
			return _maxFrame;
		}
		
		public function set width(value:uint):void
		{
			_width = value;
		}
		
		public function set height(value:uint):void
		{
			_height = value;
		}
		
		public function set frameRate(value:uint):void
		{
			_stayTime = 1000/value;
		}
		
		public function set stayTime(value:uint):void
		{
			_stayTime = value;
		}
		
		public function get positionOffset():Point
		{
			return _positionOffset;
		}
		
		public function set positionOffsetX(value:int):void
		{
			_positionOffset.x = value;
		}
		public function set positionOffsetY(value:int):void
		{
			_positionOffset.y = value;
		}
		
		override public function getFrame(currentFrame:uint, loop:Boolean=false):FrameInfomation
		{
			var frame:uint;
			if(currentFrame > maxFrame)
			{
				if(loop)
				{
					frame = (currentFrame - 1) % maxFrame;
				}
				else
				{
					return null;
				}
			}
			else
			{
				frame = currentFrame - 1;
			}
			
			return new FrameInfomation(new Rectangle(_width * frame + _positionOffset.x, _positionOffset.y, _width, _height), _stayTime);
		}
	}
}