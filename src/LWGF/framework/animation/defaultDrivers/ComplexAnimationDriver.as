package LWGF.framework.animation.defaultDrivers
{
	import LWGF.framework.animation.AnimationDriver;
	import LWGF.framework.animation.FrameInfomation;
	
	public class ComplexAnimationDriver extends AnimationDriver
	{
		protected var _frameInfo:Array = [];
		
		public function ComplexAnimationDriver()
		{
			super();
		}
		
		/**
		 * 设置动画帧信息
		 * 数组的每个元素都是{rectage:Rectage, stayTime:uint}格式的对象，其中rectage字段代表动画帧在序列图上的区域，stayTime代表该帧动画停留的时间
		 */
		public function set frameInfo(value:Array):void
		{
			_frameInfo = value;
		}
		
		override public function get maxFrame():uint
		{
			return _frameInfo.length;
		}
		
		override public function getFrame(currentFrame:uint, loop:Boolean=false):FrameInfomation
		{
			var info:Object;
			if(loop)
			{
				info =  _frameInfo[(currentFrame - 1) % maxFrame];
				return new FrameInfomation(info.rectage, info.stayTime);
			}
			else if(currentFrame > maxFrame)
			{
				return null;
			}
			else
			{
				info =  _frameInfo[currentFrame - 1];
				return new FrameInfomation(info.rectage, info.stayTime);
			}
		}
	}
}