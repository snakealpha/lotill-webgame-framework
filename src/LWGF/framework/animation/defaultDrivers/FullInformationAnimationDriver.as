package LWGF.framework.animation.defaultDrivers
{
	import LWGF.framework.animation.AnimationDriver;
	import LWGF.framework.animation.FrameInfomation;
	
	public class FullInformationAnimationDriver extends AnimationDriver
	{
		protected var _infoCollection:Array = [];
		
		public function FullInformationAnimationDriver()
		{
			super();
		}
		
		/**
		 * 设置每帧数据的集合
		 */
		public function set infoCollection(value:Array):void
		{
			_infoCollection = value;
		}
		
		override public function get maxFrame():uint
		{
			return _infoCollection.length;
		}
		
		override public function getFrame(currentFrame:uint, loop:Boolean=false):FrameInfomation
		{
			if(loop)
			{
				return _infoCollection[(currentFrame - 1) % maxFrame] as FrameInfomation;
			}
			else if(currentFrame > maxFrame)
			{
				return null;
			}
			else
			{
				return _infoCollection[currentFrame - 1] as FrameInfomation;
			}
		}
	}
}