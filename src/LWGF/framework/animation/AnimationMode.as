package LWGF.framework.animation
{
	/**
	 * 该类用于表示动画运作模式
	 */
	public final class AnimationMode
	{
		/**
		 * 简单模式
		 * 指定动画帧频和单帧大小，自动分切bitmapData由左向右形成位图动画
		 */
		public static const Simple:String = "Simple";
		
		/**
		 * 复杂模式
		 * 指定每一帧的坐标和停留时间
		 */
		public static const Complex:String = "Complex";
		
		/**
		 * 单帧单文件模式
		 * 每一指定文件作为动画的一帧
		 */
		public static const MultiFiles:String = "MultiFiles";
		
		/**
		 * 完全信息模式
		 * 开发者指定动画每一帧的所有信息并传入列表，动画类的赋型驱动类将会遍历该列表驱动动画
		 */
		public static const FullInfomation:String = "FullInfomation";
		
		/**
		 * 外部类驱动模式
		 * 开发者应自行继承AnimationDriver类型，以该类型驱动动画
		 */
		public static const ExternalDrive:String = "ExternalDrive";
		
		public function AnimationMode()
		{
		}
	}
}