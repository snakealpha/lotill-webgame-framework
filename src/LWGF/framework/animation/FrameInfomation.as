package LWGF.framework.animation
{
	import flash.geom.Point;
	import flash.geom.Rectangle;

	/**
	 * 帧信息
	 * 包含帧的资源名称，帧在原图上的坐标，帧的尺寸以及在该帧的停留时间；
	 * 然而，是否初始化这些字段以及是否使用这些字段都是由驱动器和动画的具体实现决定的。
	 */
	public class FrameInfomation
	{
		protected var _frameRectage:Rectangle;
		protected var _stayTime:uint;
		protected var _resourceName:String;
		protected var _offset:Point = new Point(0, 0);
		
		public function FrameInfomation(frameRectage:Rectangle, stayTime:uint, offset:Point = null, resourceName:String = null)
		{
			_frameRectage = frameRectage;
			_stayTime = stayTime;
			_resourceName = resourceName;
			if(offset != null)
			{
				_offset = offset;
			}
		}
		
		/**
		 * 用于表示当前动画帧在原图上的坐标以及大小
		 */
		public function get frameRectage():Rectangle
		{
			return _frameRectage;
		}
		
		/**
		 * 当前帧的停留时间
		 */
		public function get stayTime():uint
		{
			return _stayTime;
		}
		
		/**
		 * 当前帧的资源名称
		 */
		public function get resourceName():String
		{
			return _resourceName;
		}
		
		/**
		 * 位图数据的偏移
		 */
		public function get offset():Point
		{
			return _offset;
		}
	}
}