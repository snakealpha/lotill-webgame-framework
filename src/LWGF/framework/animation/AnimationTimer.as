package LWGF.framework.animation
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;

	/**
	 * 该计时器类专用于控制动画的播放，因此动画可以不受用户调整通用计时器类精度的影响。
	 * 动画类保证的播放帧频为25fps，基本等同于动画剧场版24fps的帧频
	 */
	internal class AnimationTimer
	{
		private static var _instance:AnimationTimer;
		
		public static const TimeSpan:uint = 20;
		private var _timer:Timer = new Timer(TimeSpan);
		private var _animations:Array = [];
		
		public function AnimationTimer()
		{
			_timer.addEventListener(TimerEvent.TIMER, onTimer);
		}
		
		public static function get instance():AnimationTimer
		{
			if(_instance == null)
			{
				_instance = new AnimationTimer();
			}
			
			return _instance;
		}
		
		protected function onTimer(event:TimerEvent):void
		{
			loop();
		}
		
		protected function loop():void
		{
			for(var i:uint = 0; i < _animations.length; i++)
			{
				var listener:IAnimation = _animations[i] as IAnimation;
				listener.playingInfo.passedTime += TimeSpan;
				if(listener.playingInfo.passedTime >= listener.playingInfo.frameTime)
				{
					listener.displayFrame();
				}
			}
		}
		
		public function addAnimation(ani:IAnimation):void
		{
			_animations.push(ani);
			
			if(!_timer.running)
			{
				_timer.start();
			}
		}
		
		public function removeAnimation(ani:IAnimation):void
		{
			for(var i:uint = 0; i != _animations.length; i++)
			{
				if(_animations[i] == ani)
				{
					_animations.splice(i, 1);
					i--;
				}
			}
			
			if(_animations.length == 0)
			{
				_timer.stop();
			}
		}
	}
}