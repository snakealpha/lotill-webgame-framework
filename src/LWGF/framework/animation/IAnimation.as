package LWGF.framework.animation
{
	public interface IAnimation
	{
		function get playingInfo():FramePlayingInfo;
		
		function displayFrame():void;
		
		function set driver(value:AnimationDriver):void;
		
		function get driver():AnimationDriver;
		
		function set currentFrame(value:uint):void;
		
		function get currentFrame():uint;
	}
}