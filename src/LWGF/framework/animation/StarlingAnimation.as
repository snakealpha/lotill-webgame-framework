package LWGF.framework.animation
{
	import LWGF.framework.animation.defaultDrivers.ComplexAnimationDriver;
	import LWGF.framework.animation.defaultDrivers.FullInformationAnimationDriver;
	import LWGF.framework.animation.defaultDrivers.MultiFileAnimationDriver;
	import LWGF.framework.animation.defaultDrivers.SimpleAnimationDriver;
	import LWGF.framework.utils.assertManager.AssertManager;
	
	import starling.display.Image;
	import starling.textures.Texture;
	
	public class StarlingAnimation extends Image implements IAnimation
	{
		protected var _animalMode:String = AnimationMode.Simple;
		
		protected var _driver:AnimationDriver;
		
		protected var _frameInfo:FrameInfomation;
		
		protected var _loop:Boolean = false;
		protected var _isPlaying:Boolean = false;
		protected var _currentFrame:uint = 0;
		
		protected var _imageBuffer:Texture;
		
		public var _playingInfo:FramePlayingInfo = new FramePlayingInfo();
		
		public function StarlingAnimation(width:int, height:int, texture:Texture, mode:String = AnimationMode.Simple)
		{
			super(texture);
			this.width = width;
			this.height = height;
			
			_imageBuffer = texture;
			this.mode = mode;
		}
		
		override public function set texture(value:Texture):void
		{
			_imageBuffer = value;
		}
		override public function get texture():Texture
		{
			return _imageBuffer;
		}
		
		public function get playingInfo():FramePlayingInfo
		{
			return _playingInfo;
		}		
		
		/**
		 * 动画运行的模式
		 * 将会影响到生效的驱动器类
		 * 然而在重设这一属性的同时，之前进行的设置也会失效
		 */
		public function set mode(value:String):void
		{
			_animalMode = value;
			
			if(_animalMode == AnimationMode.Simple)
			{
				driver = new SimpleAnimationDriver();
			}
			else if(_animalMode == AnimationMode.Complex)
			{
				driver = new ComplexAnimationDriver();
			}
			else if(_animalMode == AnimationMode.MultiFiles)
			{
				driver = new MultiFileAnimationDriver();
			}
			else if(_animalMode == AnimationMode.FullInfomation)
			{
				driver = new FullInformationAnimationDriver();
			}
		}
		public function get mode():String
		{
			return _animalMode;
		}
		
		/**
		 * 设置驱动器对象
		 * 视驱动器实现的不同，驱动器可能维持有状态，因此不动态生成类
		 */
		public function set driver(value:AnimationDriver):void
		{
			_driver = value;
		}
		public function get driver():AnimationDriver
		{
			return _driver;
		}
		
		/**
		 * 显示下一帧
		 */
		public function displayFrame():void
		{
			_currentFrame++;
			flushFrameInfo();
			if(_currentFrame == driver.maxFrame)
			{
				driver.animationFinal();
			}
		}
		
		/**
		 * 刷新帧缓存
		 */
		protected function flushFrameAera(frame:FrameInfomation):void
		{
			if(frame.resourceName != null)
			{
				_imageBuffer = Texture.fromBitmapData(AssertManager.getBitmapData(frame.resourceName));
			}
			super.texture = Texture.fromTexture(_imageBuffer, frame.frameRectage);
		}
		
		protected function flushFrameInfo():void
		{
			flushFrameAera(_frameInfo);
			_playingInfo.passedTime = 0;
			_playingInfo.frameTime = _frameInfo.stayTime;
			_frameInfo = driver.getFrame(_currentFrame + 1, _loop);
			if(_frameInfo == null)
			{
				stop();
			}			
		}
		
		/**
		 * 播放动画
		 * from参数为开始播放动画的帧数
		 * 当loop参数为true时，将会循环播放动画
		 */
		public function play(from:uint = 1, loop:Boolean = false):void
		{
			if(_isPlaying)
				return;
			
			currentFrame = from;
			_loop = loop;
			
			_isPlaying = true;
			AnimationTimer.instance.addAnimation(this);
		}
		
		/**
		 * 停止播放动画
		 */
		public function stop():void
		{
			AnimationTimer.instance.removeAnimation(this);
			_isPlaying = false;
		}
		
		/**
		 * 设置当前帧
		 */
		public function set currentFrame(value:uint):void
		{
			if(value > driver.maxFrame)
			{
				_currentFrame = driver.maxFrame;
			}
			else
			{
				_currentFrame = value;
			}
			
			_frameInfo = driver.getFrame(_currentFrame, _loop);
			flushFrameInfo();
		}
		public function get currentFrame():uint
			
		{
			return _currentFrame;
		}
	}
}