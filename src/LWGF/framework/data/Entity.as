package LWGF.framework.data
{
	import BlackCatWorkshop.ASBinding.BindingUtils;
	import BlackCatWorkshop.ASBinding.OneWayBinding;
	import BlackCatWorkshop.ASDispatcher.DataEvent;
	import BlackCatWorkshop.ASDispatcher.Dispatcher;
	
	import com.protobuf.Message;
	
	import flash.utils.Dictionary;
	
	import LWGF.framework.FrameworkConsts;

	/**
	 * 缓存服务端传入数据的数据实体类。
	 * 派生该类以驻留服务端数据。
	 * 另外，该类持有并维护由该类到视图类的绑定。
	 */
	public class Entity
	{				
		protected var _bindings:Dictionary = new Dictionary();
		
		public function Entity()
		{
			Dispatcher.addEventListener(FrameworkConsts.MessageArrived, onMessageArrived, moduleName);
		}
		
		public function get moduleName():String
		{
			return "";
		}
		
		private function onMessageArrived(event:DataEvent):void
		{
			messageArrived(event.data as Message);
		}
		
		protected function messageArrived(msg:Message):void
		{
			
		}
		
		/**
		 * 添加一条绑定
		 */
		public function addBinding(selfField:String, 
								   bindedObj:Object, 
								   bindedField:String, 
								   projectFunction:Function = null, 
								   excutePredicates:Function = null, 
								   postProcess:Function = null):void
		{
			var binding:OneWayBinding = BindingUtils.createOneWayBinding(this, selfField, bindedObj, bindedField, projectFunction, excutePredicates, postProcess);
			
			_bindings[selfField] = binding;
		}
		
		/**
		 * 自动识别是否存在字段绑定并赋值的辅助方法
		 */
		public function setFieldValue(selfField:String, value:Object):void
		{
			if(_bindings.hasOwnProperty(selfField))
			{
				var binding:OneWayBinding = _bindings[selfField] as OneWayBinding;
				binding.source.setValue(value);
			}
			else
			{
				this[selfField] = value;
			}
		}
	}
}