package LWGF.framework.data
{
	import BlackCatWorkshop.ASDispatcher.DataEvent;
	import BlackCatWorkshop.ASDispatcher.Dispatcher;
	
	import LWGF.framework.utils.configLoader.ConfigManager;

	/**
	 * 配置存储器
	 * 继承该类以建立配置提供器
	 */
	public class Config
	{
		public function Config()
		{
			Dispatcher.addEventListener(ConfigManager.ConfigLoaded, onConfigLoaded, "config", 64);
		}
		
		protected function onConfigLoaded(event:DataEvent):void
		{
			if(event.data == configName)
			{
				loadConfig();
			}
		}
		
		/**
		 * 构造时调用
		 * 重写该方法以特定化读取配置
		 */
		protected function loadConfig():void
		{
			
		}
		
		public function get configName():String
		{
			return "";
		}
		
		public function get moduleName():String
		{
			return "";
		}
	}
}