package LWGF.framework.view
{
	import BlackCatWorkshop.ASDispatcher.Dispatcher;
	
	import flash.events.Event;
	
	import LWGF.framework.BaseComponent.DisplayComponent;
	import LWGF.framework.BaseComponent.layer.Layer;
	import LWGF.framework.FrameworkConsts;
	import LWGF.framework.controller.Command;
	
	/**
	 * 视图类
	 * 各窗口或场景级的模块视图皆应继承于此。
	 * 包含由Flash导出的可视原件，令其显示对象属性和Entity数据建立绑定，并使
	 */
	public class View extends DisplayComponent
	{
		private var _layer:Layer;
		
		protected var holdingUpdates:Array = [];
		protected var _needUpdate:Boolean = false;
		
		public function View(layer:Layer)
		{
			_layer = layer;
			
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		private function onAdded(event:Event):void
		{
			if(needUpdate)
			{
				addEventListener(Event.ENTER_FRAME, onRender);
			}
		}
		
		public function get layer():Layer
		{
			return _layer;
		}
		
		/**
		 * 该方法返回模块的名字，用于生成Command使用的内部事件的频道名称。
		 * 同模块控制器，视图和分配器应当返回相同的模块名
		 */
		public function get moduleName():String
		{
			return "";
		}
		
		/**
		 * 该参数用于表明当前视图是否需要在下次渲染的时候更新视图表现
		 * 不应当通过update和onRender以外的方法设置该值
		 */
		protected function get needUpdate():Boolean
		{
			return _needUpdate;
		}
		protected function set needUpdate(value:Boolean):void
		{
			if(value == _needUpdate)
				return;
			
			_needUpdate = value;
			if(value && this.root != null)
			{
				addEventListener(Event.ENTER_FRAME, onRender);
			}
			else
			{
				removeEventListener(Event.ENTER_FRAME, onRender);
			}
		}
		
		/**
		 * 当当前视图需要更新时，在渲染界面时调用该回调遍历执行挂起的命令列表
		 */
		protected function onRender(event:Event):void
		{
			while(holdingUpdates.length != 0)
			{
				var command:Object = holdingUpdates.shift();
				
				updateView(command.command, command.argument);
			}
			
			needUpdate = false;
		}
		
		/**
		 * 视图更新方法
		 * 用于接受外界的更新命令维护视图
		 * 该方法仅仅接受命令，维护命令列表以及将需要更新字段设置为true， 实际的更新操作会在下一次开始渲染时通过updateView方法执行
		 * 综上，一般不应当重写此方法更新视图，而是应当重写updateView方法
		 */
		public function update(command:String, argument:Object):void
		{
			holdingUpdates.push({command:command, argument:argument});
			needUpdate = true;
		}
		
		/**
		 * 该方法在进入渲染阶段时调用，遍历挂起的命令列表以更新所有视图
		 */
		protected function updateView(command:String, argument:Object):void
		{
			
		}
		
		/**
		 * 命令发布器
		 * 视图试图对外界进行的操作均应通过命令方式发布，每个模块应当实现一个CommandListener专用于处理该模块的视图发布的命令
		 */
		public function command(commandText:String, argument:Object):void
		{
			Dispatcher.dispatchEvent(FrameworkConsts.ViewCommandDispatched, new Command(commandText, argument), moduleName);
		}
		
		/**
		 * 显示视图
		 */
		public function show():void
		{
			layer.addChild(this);
		}
		
		/**
		 * 隐藏视图
		 */
		public function hide():void
		{
			if(isShown)
			{
				parent.removeChild(this);
			}
		}
		
		/**
		 * 当前视图是否处于显示状态
		 */
		public function get isShown():Boolean
		{
			return parent != null;
		}
	}
}