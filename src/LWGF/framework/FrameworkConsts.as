package LWGF.framework
{
	public class FrameworkConsts
	{
		public static const ViewCommandDispatched:String = "Command has been dispatched.";
		public static const MessageArrived:String = "Server message has been arrived.";
		
		public function FrameworkConsts()
		{
		}
	}
}