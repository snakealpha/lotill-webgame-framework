package LWGF.content.uiComponent.utils.alert
{
	/**
	 * 警示框上显示按钮的类型
	 */
	public class AlertButtonMode
	{
		public static const OK:uint = 1;
		public static const Cancel:uint = 2;
		public static const Yes:uint = 4;
		public static const No:uint = 8;
		
		public function AlertButtonMode()
		{
		}
	}
}