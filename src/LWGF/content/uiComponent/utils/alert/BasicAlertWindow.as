package LWGF.content.uiComponent.utils.alert
{
	import flash.text.TextField;
	
	import LWGF.framework.uiComponent.Button;

	public class BasicAlertWindow extends AlertWindow
	{
		protected var _titleField:TextField = new TextField();
		protected var _contextField:TextField = new TextField();
		
		public function BasicAlertWindow()
		{
			super();
		}
		
		override protected function initUI():void
		{
			graphics.beginFill(0xffffff);
			graphics.drawRect(0, 0, 300, 200);
			graphics.endFill();
			
			_titleField.x = 5;
			_titleField.y = 5;
			addChild(_titleField);
			
			_contextField.x = 5;
			_contextField.y = 22;
			_contextField.width = 290;
			_contextField.wordWrap = true;
			_contextField.multiline = true;
			addChild(_contextField);
			
			_okButton = new Button();
			_okButton.label = "OK";
			_okButton.x = 5;
			_okButton.y = 160;
			_okButton.width = 75;
			
			_cancelButton = new Button();
			_cancelButton.label = "CANCEL";
			_cancelButton.x = 95;
			_cancelButton.y = 160;
			_cancelButton.width = 75;
			
			_yesButton = new Button();
			_yesButton.label = "YES";
			_yesButton.x = 185;
			_yesButton.y = 160;
			_yesButton.width = 75;
			
			_noButton = new Button();
			_noButton.label = "NO";
			_noButton.x = 275;
			_noButton.y = 160;
			_noButton.width = 75;
			
			_closeButton = new Button();
			
			super.initUI();
		}
		
		override public function set title(value:String):void
		{
			_titleField.text = value;
		}
		
		override public function set content(value:String):void
		{
			_contextField.text = value;
		}
	}
}