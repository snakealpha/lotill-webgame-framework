package LWGF.content.uiComponent.utils.alert
{
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	
	import LWGF.framework.BaseComponent.layer.Layer;
	import LWGF.framework.global.GlobalInfo;
	import LWGF.framework.utils.objectPool.ObjectPool;
	import LWGF.content.global.GameLayers;

	public class AlertManager
	{
		protected static var _instance:AlertManager;
		
		protected var _layer:Layer = GameLayers.instance.notifyLayer;
		
		protected var _currentAlert:AlertWindow = null;
		
		public function AlertManager()
		{
		}
		
		public static function get instance():AlertManager
		{
			if(_instance == null)
			{
				_instance = new AlertManager();
			}
			
			return _instance;
		}
		
		/**
		 * 调用系统警示框功能
		 * content：警示框显示的正文内容
		 * title：警示框的标题内容
		 * buttons：警示框显示按钮的flag,可为AlertButtonMode各值的或值
		 * callback：用户完成警示框选择之后执行的回调，接受一个AlertCallbackParameter作为参数
		 * showClose：是否显示关闭按钮，关闭按钮等效于取消按钮
		 * data：传入警示框实例的对象参数
		 * block：警示框是否为模态
		 * alertClass：使用的警示框类
		 */
		public function alert(content:String, title:String, buttons:uint, callback:Function, alertClass:Class, showClose:Boolean = true, data:Object = null, block:Boolean = true):void
		{
			if(block)
			{
				_layer.graphics.beginFill(0, 0.6);
				_layer.graphics.drawRect(0, 0, GlobalInfo.stage.stageWidth, GlobalInfo.stage.stageHeight);
				_layer.graphics.endFill();
				_layer.mouseEnabled = true;
			}
			
			var window:AlertWindow = ObjectPool.instance.getObject(alertClass) as AlertWindow;
			window.content = content;
			window.title = title;
			window.setButtonMode(buttons);
			window.callback = callback;
			window.displayCloseButton = showClose;
			window.data = data;
			
			window.x = (GlobalInfo.stage.stageWidth - window.width) / 2;
			window.y = (GlobalInfo.stage.stageHeight - window.height) / 2;
			
			_layer.addChild(window);
			_currentAlert = window;
		}
		
		public function closeAlert(window:AlertWindow):void
		{
			if(window == null)
			{
				return;
			}
			
			_layer.clear();
			_layer.graphics.clear();
			_layer.mouseEnabled = false;
			
			if(_layer.contains(window))
			{
				_layer.removeChild(window);
			}
			
			ObjectPool.instance.disposeObject(window, getQualifiedClassName(window) as Class);
			_currentAlert = null;
		}
	}
}