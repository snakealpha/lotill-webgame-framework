package LWGF.content.uiComponent.utils.alert
{
	/**
	 * 供接收警示框返回的回调方法使用的参数类
	 */
	public class AlertCallbackParameter
	{
		protected var _button:uint;
		protected var _data:Object;
		
		public function AlertCallbackParameter()
		{
		}
		
		public function set button(value:uint):void
		{
			_button = value;
		}
		public function get button():uint
		{
			return _button;
		}
		
		public function set data(value:Object):void
		{
			_data = value;
		}
		public function get data():Object
		{
			return _data;
		}
	}
}