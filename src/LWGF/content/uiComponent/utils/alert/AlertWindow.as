package LWGF.content.uiComponent.utils.alert
{
	import flash.events.MouseEvent;
	
	import LWGF.framework.BaseComponent.DisplayComponent;
	import LWGF.framework.uiComponent.Button;
	import LWGF.framework.utils.objectPool.IDisposiable;
	
	/**
	 * 警告窗口的基类
	 */
	public class AlertWindow extends DisplayComponent implements IDisposiable
	{
		protected var _data:Object;
		
		protected var _yesButton:Button;
		protected var _noButton:Button;
		protected var _okButton:Button;
		protected var _cancelButton:Button;
		
		protected var _closeButton:Button;
		
		protected var _titleString:String;
		protected var _contentString:String;
		
		protected var _returnData:AlertCallbackParameter = new AlertCallbackParameter();
		protected var _callback:Function;
		
		public function AlertWindow()
		{
			super();
			
			initUI();
		}
		
		protected function initUI():void
		{
			_yesButton.addEventListener(MouseEvent.CLICK, onYes);
			_noButton.addEventListener(MouseEvent.CLICK, onNo);
			_okButton.addEventListener(MouseEvent.CLICK, onOk);
			_cancelButton.addEventListener(MouseEvent.CLICK, onCancel);
			_closeButton.addEventListener(MouseEvent.CLICK, onClose);
		}
		
		public function set title(value:String):void
		{
			_titleString = value;
		}
		
		public function set content(value:String):void
		{
			_contentString = value
		}
		
		public function setButtonMode(value:uint):void
		{
			if(value & AlertButtonMode.Yes)
			{
				if(_yesButton.parent == null)
				{
					addChild(_yesButton);
				}
			}
			else
			{
				if(_yesButton.parent == this)
				{
					removeChild(_yesButton);
				}
			}
			
			if(value & AlertButtonMode.No)
			{
				if(_noButton.parent == null)
				{
					addChild(_noButton);
				}
			}
			else
			{
				if(_noButton.parent == this)
				{
					removeChild(_noButton);
				}
			}
			
			if(value & AlertButtonMode.OK)
			{
				if(_okButton.parent == null)
				{
					addChild(_okButton);
				}
			}
			else
			{
				if(_okButton.parent == this)
				{
					removeChild(_okButton);
				}
			}
			
			if(value & AlertButtonMode.Cancel)
			{
				if(_cancelButton.parent == null)
				{
					addChild(_cancelButton);
				}
			}
			else
			{
				if(_cancelButton.parent == this)
				{
					removeChild(_cancelButton);
				}
			}
		}
		
		public function set displayCloseButton(value:Boolean):void
		{
			if(value)
			{
				if(_closeButton.parent == null)
				{
					addChild(_closeButton);
				}
			}
			else
			{
				if(_closeButton.parent == this)
				{
					removeChild(_closeButton);
				}
			}
		}
		
		public function set callback(value:Function):void
		{
			_callback = value;
		}
		
		public function set data(value:Object):void
		{
			_data = value;
		}
		
		protected function onYes(event:MouseEvent):void
		{
			_returnData.button = AlertButtonMode.Yes;
			_callback(_returnData);
			AlertManager.instance.closeAlert(this);
		}
		
		protected function onNo(event:MouseEvent):void
		{
			_returnData.button = AlertButtonMode.No;
			_callback(_returnData);
			AlertManager.instance.closeAlert(this);
		}
		
		protected function onOk(event:MouseEvent):void
		{
			_returnData.button = AlertButtonMode.OK;
			_callback(_returnData);
			AlertManager.instance.closeAlert(this);
		}
		
		protected function onCancel(event:MouseEvent):void
		{
			_returnData.button = AlertButtonMode.Cancel;
			_callback(_returnData);
			AlertManager.instance.closeAlert(this);
		}
		
		protected function onClose(event:MouseEvent):void
		{
			onCancel(event);
		}
		
		public function dispose():void
		{
			_callback = null;
			_returnData.button = 0;
			_returnData.data = null;
		}
	}
}