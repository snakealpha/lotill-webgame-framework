package LWGF.content.uiComponent.utils.stateTip
{
	import flash.utils.getQualifiedClassName;
	
	import LWGF.framework.BaseComponent.layer.Layer;
	import LWGF.framework.global.GlobalInfo;
	import LWGF.framework.utils.objectPool.ObjectPool;
	import LWGF.content.global.GameLayers;

	/**
	 * 该类用于弹出状态提示
	 */
	public class StateTipManager
	{
		private static var _instance:StateTipManager;
		
		private var _layer:Layer;
		private var _currentTip:StateTip;
		
		public function StateTipManager()
		{
		}
		
		public static function get instance():StateTipManager
		{
			if(_instance == null)
			{
				_instance = new StateTipManager();
				_instance._layer = GameLayers.instance.tooltipLayer;
			}
			
			return _instance;
		}
		
		/**
		 * 弹出一个状态提示
		 */
		public function popTip(tipClass:Class, data:Object):void
		{
			var tipInstance:StateTip = ObjectPool.instance.getObject(tipClass) as StateTip;
			
			hideTip();
			
			tipInstance.data = data;
			
			tipInstance.x = (GlobalInfo.stage.stageWidth - tipInstance.width) / 2;
			tipInstance.y = (GlobalInfo.stage.stageHeight - tipInstance.height) / 2;
			tipInstance.show(_layer);
			
			_currentTip = tipInstance;
		}
		
		/**
		 * 隐藏当前状态提示
		 */
		public function hideTip():void
		{
			if(_currentTip)
			{
				_currentTip.remove(_layer);
			}
			
			ObjectPool.instance.disposeObject(_currentTip, getQualifiedClassName(_currentTip) as Class);
			_currentTip = null;
		}
		
		/**
		 * 获取当前状态提示
		 */
		public function get currentTip():StateTip
		{
			return _currentTip;
		}
	}
}