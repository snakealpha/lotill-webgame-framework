package LWGF.content.uiComponent.utils.stateTip
{
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.text.TextField;
	
	import LWGF.framework.BaseComponent.layer.Layer;
	import LWGF.framework.utils.assertManager.AssertManager;

	public class BasicStateTip extends StateTip
	{
		private var baseMc:MovieClip;
		private var contentField:TextField;
		
		public function BasicStateTip()
		{
			super();
			
			baseMc = AssertManager.getInstance("BasicStateTipMc") as MovieClip;
			addChild(baseMc);
			
			contentField = baseMc.tipText;
		}
		
		override public function set data(value:Object):void
		{
			if(value is String)
			{
				contentField.text = value as String;
			}
			else
			{
				contentField.text = "";
			}
		}
		
		override public function get flagString():String
		{
			return StateTipType.BasicStateTip;
		}
		
		override public function get width():Number
		{
			return baseMc.width;
		}
		
		override public function get height():Number
		{
			return baseMc.height;
		}
		
		override public function show(layer:Layer):void
		{
			alpha = 0;
			super.show(layer);
			TweenLite.to(this, 0.5, {alpha:1});
		}
	}
}