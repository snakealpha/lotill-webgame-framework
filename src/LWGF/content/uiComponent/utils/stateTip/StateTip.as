package LWGF.content.uiComponent.utils.stateTip
{
	import LWGF.framework.BaseComponent.DisplayComponent;
	import LWGF.framework.BaseComponent.layer.Layer;
	import LWGF.framework.utils.objectPool.IDisposiable;
	
	/**
	 * 弹出式状态提示的共同基类
	 */
	public class StateTip extends DisplayComponent implements IDisposiable
	{
		protected var _data:Object;
		
		public function StateTip()
		{
			super();
		}
		
		public function set data(value:Object):void
		{
			_data = value;
		}
		
		public function dispose():void
		{
			data = null;
		}
		
		/**
		 * 为方便调用现有状态提示的方法识别状态提示提供的标识字符串属性
		 */
		public function get flagString():String
		{
			return "";
		}
		
		public function show(layer:Layer):void
		{
			layer.addChild(this);
		}
		
		public function remove(layer:Layer):void
		{
			if(parent == layer)
			{
				layer.removeChild(this);
			}
		}
	}
}