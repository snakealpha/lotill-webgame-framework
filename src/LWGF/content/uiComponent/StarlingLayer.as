package LWGF.content.uiComponent
{
	import starling.display.Sprite;
	
	/**
	 * 供Starling引擎使用的层次
	 * 
	 * 适用于可能要移出Starling支持的情况：
	 * 该类包含Starling支持代码
	 */
	public class StarlingLayer extends Sprite
	{
		public function StarlingLayer()
		{
			super();
		}
		
		public function clear():void
		{			
			while(numChildren != 0)
			{
				removeChildAt(0);
			}
		}
	}
}