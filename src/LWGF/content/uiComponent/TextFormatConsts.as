package LWGF.content.uiComponent
{
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	/**
	 * 该类用于聚集游戏中使用的各种字符串格式
	 */
	public class TextFormatConsts
	{
		public static var normal_format:TextFormat = new TextFormat("宋体", 12, 0xffffff, false, false, false, null, null, TextFormatAlign.LEFT, 2, 2, 0, 6);
		public static var normal_center_format:TextFormat = new TextFormat("宋体", 12, 0xffffff, false, false, false, null, null, TextFormatAlign.CENTER, 2, 2, 0, 6);
		
		public function TextFormatConsts()
		{
		}
	}
}