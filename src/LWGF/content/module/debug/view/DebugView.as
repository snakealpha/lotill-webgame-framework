package LWGF.content.module.debug.view
{
	import flash.text.TextField;
	import flash.text.TextFieldType;
	
	import LWGF.framework.view.View;
	import LWGF.content.global.GameLayers;
	import LWGF.content.global.data.ModuleConsts;
	import LWGF.content.uiComponent.TextFormatConsts;
	
	public class DebugView extends View
	{
		private var dispTextField:TextField = new TextField();
		
		public function DebugView()
		{
			super(GameLayers.instance.uiLayer);
			
			initDisplay();
		}
		
		private function initDisplay():void
		{
			dispTextField.selectable = false;
			dispTextField.type = TextFieldType.DYNAMIC;
			dispTextField.defaultTextFormat = TextFormatConsts.normal_center_format;
			dispTextField.background = true;
			dispTextField.backgroundColor = 0x00a2e8;
			dispTextField.border = true;
			dispTextField.borderColor = 0x000000;
			dispTextField.width = 14;
			dispTextField.height = 18;
			dispTextField.text = "?";
			addChild(dispTextField);
			
			tooltipClass = DebugTooltip;
		}
		
		override public function get moduleName():String
		{
			return ModuleConsts.debug;
		}
		
		public function set infoData(value:Object):void
		{
			tooltipData = value;
		}
	}
}