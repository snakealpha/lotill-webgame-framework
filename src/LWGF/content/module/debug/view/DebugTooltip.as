package LWGF.content.module.debug.view
{
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	
	import LWGF.framework.uiComponent.tooltip.Tooltip;
	import LWGF.content.uiComponent.TextFormatConsts;
	
	public class DebugTooltip extends Tooltip
	{
		private var _infoField:TextField;
		
		public function DebugTooltip()
		{
			super();
			
			initUI();
		}
		
		private function initUI():void
		{
			_offset = new Point(4, 4);
			
			_infoField = new TextField();
			_infoField.selectable = false;
			_infoField.type = TextFieldType.DYNAMIC;
			_infoField.defaultTextFormat = TextFormatConsts.normal_format;
			_infoField.multiline = true;
			_infoField.autoSize = TextFieldAutoSize.LEFT;
			_infoField.background = true;
			_infoField.backgroundColor = 0x00a2e8;
			_infoField.border = true;
			_infoField.borderColor = 0x000000;
			_infoField.width = 120;
			_infoField.height = 18;
//			_infoField.text = "?";
			addChild(_infoField);
		}
		
		override public function set data(value:Object):void
		{
			var infoStr:String = "FrameRate:" + value.frameRate.toString() + "\n" +
								 "FreeMemory:" + (value.freeMemory / 1000000).toFixed(3) + "MB\n" +
								 "FullMemory:" + (value.fullMemory / 1000000).toFixed(3) + "MB\n";
			_infoField.text = infoStr;
		}
	}
}