package LWGF.content.module.debug
{
	import flash.system.System;
	
	import LWGF.framework.global.GlobalInfo;
	import LWGF.framework.utils.timer.TimerManager;
	import LWGF.content.global.DataManager;
	import LWGF.content.global.data.ModuleConsts;
	import LWGF.content.module.T3Controller;
	import LWGF.content.module.debug.data.DebugEntity;
	import LWGF.content.module.debug.view.DebugView;
	
	public class DebugController extends T3Controller
	{
		private var _entity:DebugEntity = new DebugEntity();
		
		public function DebugController()
		{			
			super();
			
			// test
			updateView("1", 1);
			updateView("2", 2);
			// end test
			DataManager.registerEntity(_entity);
		}
		
		override public function get ModuleName():String
		{
			return ModuleConsts.debug;
		}
		
		override public function get viewClass():Class
		{
			return DebugView;
		}
		
		public function get entity():DebugEntity
		{
			return _entity;
		}
		
		override protected function initBindings():void
		{
			entity.addBinding("_info", _view, "infoData");
		}
		
		override public function onOpenView():void
		{
			super.onOpenView();
			TimerManager.instance.registerTimer(500, 0, onTimer);
		}
		
		public function onTimer():void
		{
			entity.systemInfo = {	frameRate:GlobalInfo.stage.frameRate, 
									freeMemory:System.freeMemory,
									fullMemory:System.totalMemoryNumber};
		}
	}
}