package LWGF.content.module.debug.data
{
	import LWGF.framework.data.Entity;
	import LWGF.content.global.data.ModuleConsts;
	
	public class DebugEntity extends Entity
	{
		public var _info:Object;
		
		private var _sampleRate:uint = 500;
		
		public function DebugEntity()
		{
			super();
		}
		
		public function set systemInfo(value:Object):void
		{
			setFieldValue("_info", value);
		}
		
		public function get sampleRate():uint
		{
			return _sampleRate;
		}
		
		override public function get moduleName():String
		{
			return ModuleConsts.debug;
		}
	}
}