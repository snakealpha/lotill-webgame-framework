package LWGF.content.module
{
	import LWGF.framework.controller.Controller;
	import LWGF.framework.utils.assertManager.AssertManager;
	import LWGF.framework.utils.assertManager.LoadingInfo;
	import LWGF.content.global.DataManager;
	import LWGF.content.global.data.ModuleConsts;
	import LWGF.content.global.data.config.ModuleConfig;
	import LWGF.content.vo.ModuleInformation;
	
	public class T3Controller extends Controller
	{
		public function T3Controller()
		{
			super();
		} 
		
		override protected function loadModuleResource():void
		{
			var module:ModuleInformation = (DataManager.getConfig(ModuleConsts.module) as ModuleConfig).getModuleInfoByName(ModuleName);
			
			if(module.resource != null && module.resource != "")
			{
				AssertManager.load(module.resource, LoadingInfo.MovieClip, onModuleResourceLoaded);
			}
			else
			{
				onModuleResourceLoaded(null);
			}
		}
		
		override protected function onModuleResourceLoaded(info:LoadingInfo):void
		{
			//这一句是为了敦促基本视图进行初始化
			createNewView();
			
			var moduleCofnig:ModuleInformation = (DataManager.getConfig(ModuleConsts.module) as ModuleConfig).getModuleInfoByName(ModuleName);
			
			if(moduleCofnig.isCenter)
			{
				_view.x = (_view.layer.width - _view.width) / 2;
				_view.y = (_view.layer.height - _view.height) / 2;
			}
			else
			{
				_view.x = moduleCofnig.posX;
				_view.y = moduleCofnig.posY;
			}
			
			showView();
		}
	}
}