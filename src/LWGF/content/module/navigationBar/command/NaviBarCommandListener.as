package LWGF.content.module.navigationBar.command
{
	import LWGF.framework.controller.Command;
	import LWGF.framework.controller.CommandListener;
	import LWGF.framework.utils.Log;
	import LWGF.content.global.data.ModuleConsts;
	
	public class NaviBarCommandListener extends CommandListener
	{
		public function NaviBarCommandListener()
		{
			super();
		}
		
		override protected function get moduleName():String
		{
			return ModuleConsts.navigationBar;
		}
		
		override protected function excuteCommand(command:Command):void
		{
			switch(command.command)
			{
				case NaviBarCommandType.FakeClicked:
				{
					Log.traceLog("naviUI fake click get.", "naviBar_command");
					
					break;
				}
			}
		}
	}
}