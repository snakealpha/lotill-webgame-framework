package LWGF.content.module.navigationBar.view
{
	import fl.controls.Button;
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	import LWGF.framework.BaseComponent.layer.Layer;
	import LWGF.framework.uiComponent.utils.ButtonUtils;
	import LWGF.framework.utils.Log;
	import LWGF.framework.utils.assertManager.AssertManager;
	import LWGF.framework.view.View;
	import LWGF.content.global.GameLayers;
	import LWGF.content.global.data.ModuleConsts;
	import LWGF.content.module.navigationBar.command.NaviBarCommandType;
	
	public class NaviBarView extends View
	{
		private var _navUI:MovieClip = AssertManager.getMovieClip("NavUI");
		private var _fakeButton:Button;
		
		public function NaviBarView()
		{
			super(GameLayers.instance.uiLayer);
			
			initUI();
		}
		
		override public function get moduleName():String
		{
			return ModuleConsts.navigationBar;
		}
		
		private function initUI():void
		{
			addChild(_navUI);
			_fakeButton = ButtonUtils.replaceMovieClipWithButton(_navUI.fakeNavButton);
			_fakeButton.addEventListener(MouseEvent.CLICK, onFakeClicked);
		}
		
		private function onFakeClicked(event:MouseEvent):void
		{
			command(NaviBarCommandType.FakeClicked, null);
		}
		
		override public function update(command:String, argument:Object):void
		{
			Log.traceLog(command);
		}
	}
}