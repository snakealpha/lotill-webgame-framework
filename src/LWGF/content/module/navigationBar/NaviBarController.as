package LWGF.content.module.navigationBar
{
	import LWGF.content.global.DataManager;
	import LWGF.content.global.data.ModuleConsts;
	import LWGF.content.module.T3Controller;
	import LWGF.content.module.navigationBar.command.NaviBarCommandListener;
	import LWGF.content.module.navigationBar.view.NaviBarView;
	
	public class NaviBarController extends T3Controller
	{
		private var _command:NaviBarCommandListener = new NaviBarCommandListener();
		
		public function NaviBarController()
		{
			super();
		}
		
		override public function get ModuleName():String
		{
			return ModuleConsts.navigationBar;
		}
		
		override public function get viewClass():Class
		{
			return NaviBarView;
		}
	}
}