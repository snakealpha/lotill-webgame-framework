package LWGF.content.module.port.network
{
	import LWGF.framework.network.MessageEndpoint;
	import LWGF.content.global.data.ModuleConsts;
	
	public class PortMessageEndpoint extends MessageEndpoint
	{
		public function PortMessageEndpoint()
		{
			super();
		}
		
		override public function get moduleName():String
		{
			return ModuleConsts.port;
		}
	}
}