package LWGF.content.module.port.network
{
	import LWGF.framework.network.WrappedSocket;
	import LWGF.content.global.network.T3Proxy;
	
	public class PortProxy extends T3Proxy
	{
		public function PortProxy(socket:WrappedSocket)
		{
			super(socket);
		}
	}
}