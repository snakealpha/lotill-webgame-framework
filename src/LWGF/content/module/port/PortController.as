package LWGF.content.module.port
{
	import LWGF.content.global.DataManager;
	import LWGF.content.global.data.ModuleConsts;
	import LWGF.content.module.T3Controller;
	import LWGF.content.module.port.command.PortCommandListener;
	import LWGF.content.module.port.data.PortConfig;
	import LWGF.content.module.port.data.PortEntity;
	import LWGF.content.module.port.view.PortView;
	
	public class PortController extends T3Controller
	{
		private var _config:PortConfig = new PortConfig();
		private var _entity:PortEntity = new PortEntity();
		private var _command:PortCommandListener = new PortCommandListener();
		
		public function PortController()
		{
			super();
			
			DataManager.registerConfig(_config);
			DataManager.registerEntity(_entity);
		}
		
		override public function get ModuleName():String
		{
			return ModuleConsts.port;
		}
		
		public function get config():PortConfig
		{
			return _config;
		}
		
		public function get entity():PortEntity
		{
			return _entity;
		}
		
		override public function get viewClass():Class
		{
			return PortView;
		}
		
		override public function onOpenView():void
		{
			super.onOpenView();
			_view.layer.clear();
		}
	}
}