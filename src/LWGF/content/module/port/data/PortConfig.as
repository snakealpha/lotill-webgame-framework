package LWGF.content.module.port.data
{
	import flash.utils.Dictionary;
	
	import LWGF.framework.data.Config;
	import LWGF.framework.utils.ObjectUtils;
	import LWGF.content.global.T3ConfigManager;
	import LWGF.content.global.data.ModuleConsts;
	import LWGF.content.vo.LevelInfo;
	import LWGF.content.vo.PortInfo;
	
	public class PortConfig extends Config
	{
		private var _ports:Dictionary = new Dictionary();
		
		private var _microPortLevel:Array = [];
		private var _littlePortLevel:Array = [];
		private var _mediumPortLevel:Array = [];
		private var _largePortLevel:Array = [];
		
		public function PortConfig()
		{
			super();
		}
		
		override public function get configName():String
		{
			return "config.data";
		}
		
		override public function get moduleName():String
		{
			return ModuleConsts.port;
		}
		
		override protected function loadConfig():void
		{
			loadPortInfo();
			loadLevelInfo();
		}
		
		private function loadPortInfo():void
		{
			var info:Array = T3ConfigManager.instance.getConfigTab("PortsInfo");
			for each(var portSrc:Object in info)
			{
				var port:PortInfo = new PortInfo();
				ObjectUtils.putObject(portSrc, port);
				_ports[port.name] = port;
			}
			info.length = 0;
		}
		
		private function loadLevelInfo():void
		{
			var lvConfigs:Array = T3ConfigManager.instance.getConfigTab("MicroExp");
			var i:uint = 0;
			var lvExp:LevelInfo;
			for(i = 0; i != lvConfigs.length; i++)
			{
				lvExp = new LevelInfo();
				ObjectUtils.putObject(lvConfigs[i], lvExp);
				_microPortLevel.push(lvExp);
			}
			lvConfigs.length = 0;
			lvConfigs = T3ConfigManager.instance.getConfigTab("LittleExp");
			for(i = 0; i != lvConfigs.length; i++)
			{
				lvExp = new LevelInfo();
				ObjectUtils.putObject(lvConfigs[i], lvExp);
				_littlePortLevel.push(lvExp);
			}
			lvConfigs.length = 0;
			lvConfigs = T3ConfigManager.instance.getConfigTab("MediumExp");
			for(i = 0; i != lvConfigs.length; i++)
			{
				lvExp = new LevelInfo();
				ObjectUtils.putObject(lvConfigs[i], lvExp);
				_mediumPortLevel.push(lvExp);
			}
			lvConfigs.length = 0;
			lvConfigs = T3ConfigManager.instance.getConfigTab("LargeExp");
			for(i = 0; i != lvConfigs.length; i++)
			{
				lvExp = new LevelInfo();
				ObjectUtils.putObject(lvConfigs[i], lvExp);
				_largePortLevel.push(lvExp);
			}
			lvConfigs.length = 0;
		}
		
		/**
		 * 获取所有港口的字典
		 */
		public function get ports():Dictionary
		{
			return _ports;
		}
	}
}