package LWGF.content.module.port.data
{
	import LWGF.framework.data.Entity;
	import LWGF.content.global.data.ModuleConsts;
	
	public class PortEntity extends Entity
	{
		public function PortEntity()
		{
			super();
		}
		
		override public function get moduleName():String
		{
			return ModuleConsts.port;
		}
	}
}