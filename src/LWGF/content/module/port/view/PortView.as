package LWGF.content.module.port.view
{
	import LWGF.framework.BaseComponent.layer.Layer;
	import LWGF.framework.view.View;
	import LWGF.content.global.data.ModuleConsts;
	
	public class PortView extends View
	{
		public function PortView(layer:Layer)
		{
			super(layer);
		}
		
		override public function get moduleName():String
		{
			return ModuleConsts.port;
		}
	}
}