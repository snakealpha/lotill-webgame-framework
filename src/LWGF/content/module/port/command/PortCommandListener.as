package LWGF.content.module.port.command
{
	import LWGF.framework.controller.CommandListener;
	import LWGF.framework.controller.Controller;
	import LWGF.content.global.GameControllers;
	import LWGF.content.global.data.ModuleConsts;
	
	public class PortCommandListener extends CommandListener
	{
		public function PortCommandListener()
		{
			super();
		}
		
		override protected function get moduleName():String
		{
			return ModuleConsts.port;
		}
		
		override protected function get controller():Controller
		{
			return GameControllers.instance.getControllerByModuleName(moduleName) as Controller;
		}
	}
}