package LWGF.content.module.worldMap.command
{
	import LWGF.framework.controller.CommandListener;
	import LWGF.framework.controller.Controller;
	import LWGF.content.global.GameControllers;
	import LWGF.content.global.data.ModuleConsts;
	
	public class WorldMapCommandListener extends CommandListener
	{
		public function WorldMapCommandListener()
		{
			super();
		}

		override protected function get moduleName():String
		{
			return ModuleConsts.worldMap;
		}
		
		override protected function get controller():Controller
		{
			return GameControllers.instance.getControllerByModuleName(moduleName) as Controller;
		}
	}
}