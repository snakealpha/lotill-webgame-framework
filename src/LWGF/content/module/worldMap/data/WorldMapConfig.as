package LWGF.content.module.worldMap.data
{
	import flash.geom.Point;
	import flash.utils.Dictionary;
	
	import LWGF.framework.animation.Animation;
	import LWGF.framework.animation.defaultDrivers.SimpleAnimationDriver;
	import LWGF.framework.data.Config;
	import LWGF.framework.utils.ObjectUtils;
	import LWGF.framework.utils.assertManager.AssertManager;
	import LWGF.content.global.DataManager;
	import LWGF.content.global.T3ConfigManager;
	import LWGF.content.global.data.ModuleConsts;
	import LWGF.content.module.port.data.PortConfig;
	import LWGF.content.module.worldMap.view.grid.GridInfo;
	import LWGF.content.module.worldMap.view.grid.GridInfoManager;
	import LWGF.content.module.worldMap.view.thing.Port;
	import LWGF.content.module.worldMap.view.thing.ship.Ship;
	import LWGF.content.vo.PortAnimation;
	import LWGF.content.vo.PortInfo;
	import LWGF.content.vo.ShipAnimation;
	
	public class WorldMapConfig extends Config
	{
		protected var _blockWidth:uint = 1200;
		protected var _blockHeight:uint = 600;
		
		protected var _gridWidth:uint = 120;
		protected var _gridHeight:uint = 60;
		
		protected var _maxRow:uint = 3;
		protected var _maxColumn:uint = 3;
		
		protected var _rawPointPixelPoint:Point = new Point(0, 0);
		
		protected var _mapLayerHeight:uint;
		
		protected var _ports:Array = [];
		
		protected var _shipConfigs:Dictionary = new Dictionary();
		protected var _portAniConfigs:Dictionary = new Dictionary();
		
		public function WorldMapConfig()
		{
			super();
		}
		
		override public function get configName():String
		{
			return "config.data";
		}
		
		override public function get moduleName():String
		{
			return ModuleConsts.worldMap;
		}
		
		override protected function loadConfig():void
		{
			super.loadConfig();
			
			// 加载船只动画配置信息
			var configs:Array = T3ConfigManager.instance.getConfigTab("ShipAnimation");
			var config:Object;
			for each(config in configs)
			{
				var factInfo:ShipAnimation = new ShipAnimation();
				ObjectUtils.putObject(config, factInfo);
				
				factInfo.float = (factInfo.rawFloat != 0);
				var floatArray:Array = factInfo.rawFloatX.split(";");
				for each(var x:String in floatArray)
				{
					factInfo.floatX.push(parseInt(x));
				}
				floatArray = factInfo.rawFloatY.split(";");
				for each(var y:String in floatArray)
				{
					factInfo.floatY.push(parseInt(y));
				}
				
				_shipConfigs[factInfo.name] = factInfo;
			}
			configs.length = 0;
			
			// 加载港口动画配置信息
			configs = T3ConfigManager.instance.getConfigTab("PortAnimation");
			for each(config in configs)
			{
				var portInfo:PortAnimation = new PortAnimation();
				ObjectUtils.putObject(config, portInfo);
				
				portInfo.useBasePoint = config.useBasePoint != 0;
				
				_portAniConfigs[portInfo.name] = portInfo;
			}
			configs.length = 0;
			
			initGrids();
		}
		
		public function initPorts():void
		{
			var portConfig:PortConfig = DataManager.getConfig(ModuleConsts.port) as PortConfig;
			for each(var portInfo:PortInfo in portConfig.ports)
			{
				var port:Port = new Port();
				var aniConfig:PortAnimation = _portAniConfigs[portInfo.countary+"_"+portInfo.type+"_"+portInfo.scale] as PortAnimation;
				port.position = new Point(portInfo.posX, portInfo.posY);
				var animation:Animation = new Animation(aniConfig.frameWidth, aniConfig.frameHeight, AssertManager.getBitmapData(aniConfig.res));
				var driver1:SimpleAnimationDriver = animation.driver as SimpleAnimationDriver;
				driver1.maxFrame = aniConfig.frameNum;
				driver1.width = aniConfig.frameWidth;
				driver1.height = aniConfig.frameHeight;
				driver1.stayTime = aniConfig.stay;
				port.animation = animation;
				_ports.push(port);
			}
		}
		
		private function initGrids():void
		{
			//test
			GridInfoManager.addGrid(new GridInfo(new Point(5, 5), false));
			GridInfoManager.addGrid(new GridInfo(new Point(10, 10), true, "entry_city"));
			//end test
		}
		
		/**
		 * 单个地图块的宽度
		 */
		public function get blockWidth():uint
		{
			return _blockWidth;
		}
		
		/**
		 * 单个地图块的高度
		 */
		public function get blockHeight():uint
		{
			return _blockHeight;
		}
		
		/**
		 * 单个格子的宽度
		 */
		public function get gridWidth():uint
		{
			return _gridWidth;
		}
		
		/**
		 * 单个格子的高度
		 */
		public function get gridHeight():uint
		{
			return _gridHeight;
		}
		
		public function get maxRow():uint
		{
			return _maxRow
		}
		
		public function get maxColumn():uint
		{
			return _maxColumn;
		}
		
		public function get rawPointPixelPoint():Point
		{
			return _rawPointPixelPoint;
		}
		
		public function get ports():Array
		{
			return _ports;
		}
		
		/**
		 * 依据设定在配置表里的名称检索，构建和返回船只对象
		 */
		public function getShipWithName(name:String):Ship
		{
			if(!_shipConfigs.hasOwnProperty(name))
			{
				return null;
			}
			
			var config:ShipAnimation = _shipConfigs[name] as ShipAnimation;
			var configShip:Ship = new Ship(config.res, config.frameNum, config.stay, config.float, config.floatX, config.floatY);
			configShip.basePoint = new Point(config.basePointX, config.basePointY);
			return configShip;
		}
	}
}