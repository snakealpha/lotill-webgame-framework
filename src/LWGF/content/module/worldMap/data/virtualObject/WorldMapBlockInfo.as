package LWGF.content.module.worldMap.data.virtualObject
{
	import flash.display.BitmapData;
	
	import LWGF.content.module.worldMap.view.utils.WorldMapTextureManager;

	public class WorldMapBlockInfo
	{
		public static const TextureChanged:String = "texture_changed";
		
		private var _row:int;
		private var _column:int;
		
		private var _texture:BitmapData;
		
		public var _bitmapDataChanged:Function;
		
		public function WorldMapBlockInfo(row:int, column:int, texture:BitmapData = null)
		{
			_row = row;
			_column = column;
			
			_texture = texture;
		} 
		
		public function get row():int
		{
			return _row;
		}
		
		public function get column():int
		{
			return _column;
		}
		
		public function set texture(value:BitmapData):void
		{
			_texture = value;
			
			if(bitmapDataChanged != null)
				bitmapDataChanged();
		}
		public function get texture():BitmapData
		{
			return _texture;
		}
		
		public function get bitmapDataChanged():Function
		{
			return _bitmapDataChanged;
		}
		public function set bitmapDataChanged(value:Function):void
		{
			_bitmapDataChanged = value;
			bitmapDataChanged();
		}
	}
}