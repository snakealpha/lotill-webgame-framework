package LWGF.content.module.worldMap.data.virtualObject
{
	/**
	 * 单个格子的信息
	 * 对于不存在该信息的格子，默认为可通过，无事件，无参数
	 */
	public class WorldMapGridInfo
	{
		protected var _x:uint = 0;
		protected var _y:uint = 0;
		
		protected var _isPassive:Boolean = true;
		
		protected var _gridEvent:String = "";
		protected var _eventArgument:String = "";
		
		public function WorldMapGridInfo()
		{
		}
		
		public function parse():void
		{
			
		}
		
		public function get x():uint
		{
			return _x;
		}
		
		public function get y():uint
		{
			return _y;
		}
		
		public function get isPassive():Boolean
		{
			return _isPassive;
		}
		
		public function get event():String
		{
			return _gridEvent;
		}
		
		public function get argument():String
		{
			return _eventArgument;
		}
	}
}