package LWGF.content.module.worldMap.data
{
	import flash.geom.Point;
	
	import LWGF.framework.data.Entity;
	import LWGF.content.global.data.ModuleConsts;
	
	public class WorldMapEntity extends Entity
	{		
		public var postitonValue:Point;
		
		public function WorldMapEntity()
		{
			super();
		}
		
		override public function get moduleName():String
		{
			return ModuleConsts.worldMap;
		}
		
		public function set position(value:Point):void
		{
			setFieldValue("postitonValue", value);
		}
	}
}