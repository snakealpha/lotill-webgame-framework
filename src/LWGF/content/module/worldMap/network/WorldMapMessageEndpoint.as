package LWGF.content.module.worldMap.network
{
	import LWGF.framework.network.MessageEndpoint;
	import LWGF.content.global.data.ModuleConsts;
	
	public class WorldMapMessageEndpoint extends MessageEndpoint
	{
		public function WorldMapMessageEndpoint()
		{
			super();
		}
		
		override public function get moduleName():String
		{
			return ModuleConsts.worldMap;
		}
	}
}