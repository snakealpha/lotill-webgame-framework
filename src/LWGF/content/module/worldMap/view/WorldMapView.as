package LWGF.content.module.worldMap.view
{
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	
	import LWGF.framework.BaseComponent.layer.Layer;
	import LWGF.framework.global.GlobalInfo;
	import LWGF.framework.view.View;
	import LWGF.content.global.GameLayers;
	import LWGF.content.global.data.ModuleConsts;
	import LWGF.content.module.worldMap.view.map.WorldMapLayer;
	import LWGF.content.module.worldMap.view.thing.ThingLayer;
	import LWGF.content.module.worldMap.view.thing.ship.ShipLayer;
	
	public class WorldMapView extends View
	{
		//地图地表层
		private var _mapLayer:WorldMapLayer = new WorldMapLayer();
				
		public function WorldMapView()
		{
			super(GameLayers.instance.senceLayer);
			
			_mapLayer.currentGrid = new Point(0, 0);
			addChild(_mapLayer);			
			
		}
		
		override public function get moduleName():String
		{
			return ModuleConsts.worldMap;
		}
		
		public function set selfPosition(value:Point):void
		{
			_mapLayer.currentGrid = value;
		}
	}
}