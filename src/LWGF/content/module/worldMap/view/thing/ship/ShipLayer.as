package LWGF.content.module.worldMap.view.thing.ship
{
	import flash.geom.Point;
	import flash.utils.Dictionary;
	
	import LWGF.framework.BaseComponent.DisplayComponent;
	import LWGF.framework.global.GlobalInfo;
	import LWGF.content.global.DataManager;
	import LWGF.content.global.GameControllers;
	import LWGF.content.global.data.ModuleConsts;
	import LWGF.content.module.worldMap.WorldMapController;
	import LWGF.content.module.worldMap.data.WorldMapConfig;
	import LWGF.content.module.worldMap.view.map.WorldMapLayer;
	import LWGF.content.module.worldMap.view.utils.CoordinateTransfer;
	
	public class ShipLayer extends DisplayComponent
	{
		private var _config:WorldMapConfig = DataManager.getConfig(ModuleConsts.worldMap) as WorldMapConfig;;
		
		protected var _rawPoint:Point = new Point();
		
		private var _viewWidth:uint = GlobalInfo.stage.stageWidth;
		private var _viewHeight:uint = GlobalInfo.stage.stageHeight;
		
		private var _pixelPosition:Point;
		private var _basePoint:Point = new Point();
		
		private var _playerShip:Ship;
		/**
		 * 该数组用于存放视野内的所有玩家船只，视野由服务器维护
		 */
		private var _players:Array = [];
		/**
		 * 该数组用于存放已添加到地图上的玩家船只
		 */
		private var _displayedPlayers:Array = [];
		
		protected var _referenceObject:WorldMapLayer;
				
		public function ShipLayer()
		{
			super();
			
			initUI();
		}
		
		private function initUI():void
		{
//			_playerShip = new Ship("test_ship_1", 5, 400);
//			_playerShip.basePoint = new Point(60, 105);
//			_playerShip = _config.getShipWithName("test_ship_1");
			_playerShip = _config.getShipWithName("test_ship_2");
			_playerShip.x = _viewWidth / 2 - _playerShip.width / 2;
			_playerShip.y = _viewHeight / 2 - _playerShip.height / 2;
			addChild(_playerShip);
		}
		
		public function set viewWidth(value:uint):void
		{
			_viewWidth = value;
		}
		
		public function set viewHeight(value:uint):void
		{
			_viewHeight = value;
		}
		
		/**
		 * 设置屏幕中心点的像素位置
		 */
		public function set midPoint(value:Point):void
		{
			_rawPoint.x = value.x - _viewWidth / 2;
			_rawPoint.y = value.y - _viewHeight / 2;
			
			putThings();
		}
		
		/**
		 * 向视野中添加船只
		 */
		public function addShip():void
		{
			
		}
		
		/**
		 * 从视野中移除船只
		 */
		public function removeShip():void
		{
			
		}
		
		public function putThings():void
		{
			var thing:Ship;
			var globalPosition:Point;
			for each(thing in _players)
			{
				thing.x = thing.shipInfo.pixelPosition.x - _rawPoint.x - thing.basePoint.x;
				thing.y = thing.shipInfo.pixelPosition.y - _rawPoint.y - thing.basePoint.y;
				
				if(thing.x >= 0 - thing.width / 2 &&
					thing.y >= 0 - thing.height / 2 &&
					thing.x <= _viewWidth + thing.width / 2 &&
					thing.y <= _viewHeight + thing.height / 2)
				{
					if(thing.parent != this)
					{
						addChild(thing);
						_displayedPlayers.push(thing);
					}
				}
			}
						
			for(var i:uint = 0; i != _displayedPlayers.length; i++)
			{
				thing = _displayedPlayers[i] as Ship;
				
				thing.x = thing.shipInfo.pixelPosition.x - _rawPoint.x - thing.basePoint.x;
				thing.y = thing.shipInfo.pixelPosition.y - _rawPoint.y - thing.basePoint.y;
				
				if(thing.x < 0 - thing.width / 2 ||
					thing.y < 0 - thing.height / 2 ||
					thing.x > _viewWidth + thing.width / 2 ||
					thing.y > _viewHeight + thing.height / 2)
				{
					if(thing.parent == this)
						removeChild(thing);
					
					_displayedPlayers.splice(i, 1);
					i--;
				}
			}
		}
		
		public function get playerShip():Ship
		{
			return _playerShip;
		}
	}
}