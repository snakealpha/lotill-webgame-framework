package LWGF.content.module.worldMap.view.thing.ship
{
	import flash.geom.Point;

	/**
	 * 船只信息...
	 * 仅适用于非当前玩家的船只，而当前玩家船只的信息集中于WorldMapLayer
	 */
	public class ShipInfo
	{
		protected var _position:Point;
		protected var _pixelPosition:Point;
		
		protected var _path:Array = [];
		
		public function ShipInfo()
		{
		}
		
		public function get position():Point
		{
			return _position;
		}
		
		public function set position(value:Point):void
		{
			_position = value;
		}
		
		public function get pixelPosition():Point
		{
			return _pixelPosition;
		}
		
		public function set pixelPosition(value:Point):void
		{
			_pixelPosition = value;
		}
		
		public function get path():Array
		{
			return _path;
		}
	}
	
}