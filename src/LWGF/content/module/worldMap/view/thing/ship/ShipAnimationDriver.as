package LWGF.content.module.worldMap.view.thing.ship
{
	import flash.utils.Dictionary;
	
	import LWGF.framework.animation.AnimationDriver;
	import LWGF.framework.animation.FrameInfomation;
	import LWGF.framework.animation.defaultDrivers.SimpleAnimationDriver;
	
	public class ShipAnimationDriver extends AnimationDriver
	{
		protected var _width:uint = 0;
		protected var _height:uint = 0;
		
		protected var _driverDict:Dictionary = new Dictionary();
		
		protected var _direction:int = 0;
		
		protected var _maxFrame:uint;
		
		public function ShipAnimationDriver()
		{
			super();
			
			initDriver();
		}
		
		protected function initDriver():void
		{
			_driverDict[ShipDirectionType.Up] = new SimpleAnimationDriver();
			_driverDict[ShipDirectionType.UpLeft] = new SimpleAnimationDriver();
			_driverDict[ShipDirectionType.Left] = new SimpleAnimationDriver();
			_driverDict[ShipDirectionType.DownLeft] = new SimpleAnimationDriver();
			_driverDict[ShipDirectionType.Down] = new SimpleAnimationDriver();
			_driverDict[ShipDirectionType.DownRight] = new SimpleAnimationDriver();
			_driverDict[ShipDirectionType.Right] = new SimpleAnimationDriver();
			_driverDict[ShipDirectionType.UpRight] = new SimpleAnimationDriver();
		}
		
		public function getDriverByDirection(direction:int):SimpleAnimationDriver
		{
			return _driverDict[direction] as SimpleAnimationDriver;
		}
		
		public function get direction():int
		{
			return _direction;
		}
		
		public function set direction(value:int):void
		{
			_direction = value;
		}
		
		override public function getFrame(currentFrame:uint, loop:Boolean=true):FrameInfomation
		{
			var frameBaseDirection:uint = Math.abs(_direction);
			return getDriverByDirection(frameBaseDirection).getFrame(currentFrame, loop);
		}
		
		public function set maxFrame(value:uint):void
		{
			_maxFrame = value;
			
			for each(var driver:SimpleAnimationDriver in _driverDict)
			{
				driver.maxFrame = value;
			}
		}
		
		override public function get maxFrame():uint
		{
			return _maxFrame;
		}
		
		public function set stayTime(value:uint):void
		{
			for each(var driver:SimpleAnimationDriver in _driverDict)
			{
				driver.stayTime = value;
			}
		}
		
		public function set width(value:uint):void
		{
			_width = value;
			
			for each(var driver:SimpleAnimationDriver in _driverDict)
			{
				driver.width = value;
			}
		}
		
		public function set height(value:uint):void
		{
			_height = value;
			
			for(var key:Object in _driverDict)
			{
				_driverDict[key].height = _height;
				_driverDict[key].positionOffsetY = Math.abs(int(key)) * _height;
			}
		}
	}
}