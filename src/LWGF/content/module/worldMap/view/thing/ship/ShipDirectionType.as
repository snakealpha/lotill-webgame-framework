package LWGF.content.module.worldMap.view.thing.ship
{
	public class ShipDirectionType
	{
		public static const Up:int = 0;
		public static const UpLeft:int = -1;
		public static const UpRight:int = 1;
		public static const Left:int = -2;
		public static const Right:int = 2;
		public static const DownLeft:int = -3;
		public static const DownRight:int = 3;
		public static const Down:int = 4;
		
		public function ShipDirectionType()
		{
		}
	}
}