package LWGF.content.module.worldMap.view.thing.ship
{
	import BlackCatWorkshop.ASDispatcher.DataEvent;
	
	import flash.display.BitmapData;
	
	import LWGF.framework.animation.Animation;
	import LWGF.framework.animation.AnimationMode;
	import LWGF.framework.utils.assertManager.AssertManager;
	import LWGF.framework.utils.assertManager.LoadingInfo;
	import LWGF.content.module.worldMap.view.thing.MapThing;

	/**
	 * 船只对象
	 */
	public class Ship extends MapThing
	{
		public static const DirectionNum:uint = 5;
		
		protected var _resourceName:String;
		protected var _frameNum:uint;
		protected var _stayTime:uint;
		
		protected var _shipDriver:ShipAnimationDriver = new ShipAnimationDriver();
		protected var _direction:int;
		
		protected var _shipInfo:ShipInfo = new ShipInfo();
		
		protected var _isFloat:Boolean = false;
		protected var _floatX:Array;
		protected var _floatY:Array;
		
		public function Ship(resourceName:String, frameNumber:uint, stayTime:uint, isFloat:Boolean, floatX:Array, floatY:Array)
		{
			super();
			
			_isFloat = isFloat;
			_floatX = floatX;
			_floatY = floatY;
			
			setShipResource(resourceName, frameNumber, stayTime);
		}
		
		protected function onNextFrame(event:DataEvent):void
		{
			if(!_isFloat)
			{
				return;
			}
			
			setRelativePosition();
		}
		
		protected function initShip():void
		{
			AssertManager.load("asserts\\ship\\map\\"+_resourceName+".swf", LoadingInfo.MovieClip, onShipResourceLoaded);
		}
		
		protected function onShipResourceLoaded(info:LoadingInfo):void
		{
			var baseResource:BitmapData = AssertManager.getBitmapData(_resourceName);
			
			animation = new Animation(baseResource.width / _frameNum, baseResource.height / DirectionNum, baseResource, AnimationMode.ExternalDrive);
			_shipDriver.maxFrame = _frameNum;
			_shipDriver.width = baseResource.width / _frameNum;
			_shipDriver.height = baseResource.height / DirectionNum;
			_shipDriver.stayTime = _stayTime;
			animation.driver = _shipDriver;
			
			setRelativePosition();
			
			_animation.addEventListener(Animation.EnterNextFrame, onNextFrame);
			
			if(parent != null)
			{
				_animation.play(1, true);
			}
		}
		
		public function setShipResource(resourceName:String, frameNumber:uint, stayTime:uint):void
		{
			_resourceName = resourceName;
			_frameNum = frameNumber;
			_stayTime = stayTime;
			
			initShip();
		}
		
		public function set direction(value:int):void
		{
			_direction = value;
			(_animation.driver as ShipAnimationDriver).direction = value;
			_animation.currentFrame = 1;
			
			if(value != 0)
			{
				_animation.scaleX = value / Math.abs(value);
			}
			else
			{
				_animation.scaleX = 1;
			}
			
			setRelativePosition();
		}
		
		protected function setRelativePosition():void
		{
			if(_animation.scaleX < 0)
			{
				_animation.x = _floatX[_animation.currentFrame % _floatX.length] + _animation.width - _basePoint.x;
			}
			else
			{
				_animation.x = _floatX[_animation.currentFrame % _floatX.length] - _basePoint.x;
			}

			_animation.y = _floatY[_animation.currentFrame % _floatY.length] - _basePoint.y;
		}
		
		public function get direction():int
		{
			return _direction;
		}
		
		public function get shipInfo():ShipInfo
		{
			return _shipInfo;
		}
	}
}