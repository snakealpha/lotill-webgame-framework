package LWGF.content.module.worldMap.view.thing
{
	import flash.geom.Point;
	
	import LWGF.framework.animation.Animation;

	public class Port extends MapThing
	{
		protected var _customBasePoint:Boolean = false;
		
		public function Port()
		{
			super();
			
			mouseEnabled = false;
			mouseChildren = false;
		}
		
		override public function set basePoint(value:Point):void
		{
			super.basePoint = value;
			_customBasePoint = true;
		}
		
		override public function set animation(value:Animation):void
		{
			super.animation = value;
			if(_customBasePoint == false)
			{
				_basePoint.x = value.width / 2;
				_basePoint.y = value.height / 2;
			}
		}
	}
}