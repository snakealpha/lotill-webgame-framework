package LWGF.content.module.worldMap.view.thing
{
	import com.greensock.TweenLite;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import LWGF.framework.BaseComponent.DisplayComponent;
	import LWGF.framework.global.GlobalInfo;
	import LWGF.content.global.DataManager;
	import LWGF.content.global.GameControllers;
	import LWGF.content.global.data.ModuleConsts;
	import LWGF.content.module.worldMap.WorldMapController;
	import LWGF.content.module.worldMap.data.WorldMapConfig;
	import LWGF.content.module.worldMap.view.grid.MouseGrid;
	import LWGF.content.module.worldMap.view.map.MapBlockType;
	import LWGF.content.module.worldMap.view.map.MapImageBlock;
	import LWGF.content.module.worldMap.view.utils.CoordinateTransfer;
	import LWGF.content.module.worldMap.view.utils.WorldMapTextureManager;
	
	/**
	 * 地表层次
	 */
	public class SurfaceLayer extends DisplayComponent
	{
		private var _config:WorldMapConfig = DataManager.getConfig(ModuleConsts.worldMap) as WorldMapConfig;
		
		private var _centerBlock:MapImageBlock = new MapImageBlock();
		private var _diagonalBlock:MapImageBlock = new MapImageBlock();
		private var _horizontalBlock:MapImageBlock = new MapImageBlock();
		private var _verticalBlock:MapImageBlock = new MapImageBlock();
		
		private var _mouseGrid:MouseGrid = new MouseGrid();
		
		private var _currentPoint:Point;
		
		private var _interactLayer:DisplayComponent = new DisplayComponent();
		
		public function SurfaceLayer()
		{
			super();
			
			initUI();
		}
		
		private function initUI():void
		{			
			addChild(_centerBlock);
			addChild(_interactLayer);
			_interactLayer.addChild(_mouseGrid);
			
			//用于绘制当前鼠标格子的监听
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
			addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
		}
		
		//start - 用于维护当前格子的代码
		private var _needUpdateGrid:Boolean = false;
		private function onAdded(event:Event):void
		{
			addEventListener(Event.ENTER_FRAME, updateCurrentGrid);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
			
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		private function onRemoved(event:Event):void
		{
			removeEventListener(Event.ENTER_FRAME, updateCurrentGrid);
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
			
			removeEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
		}
		private function onMouseMove(event:MouseEvent):void
		{
			_needUpdateGrid = true;
		}
		
		private function updateCurrentGrid(event:Event):void
		{
			if(_needUpdateGrid == false)
			{
				return;
			}
			
			var currentGridPixelPos:Point = CoordinateTransfer.gridToPixel(_currentGridPos);
			
			var mouseGlobalX:Number = mouseX + currentGridPixelPos.x - _layerWidth / 2;
			var mouseGlobalY:Number = mouseY + currentGridPixelPos.y - _layerHeight / 2;
			
			var mouseGridPos:Point = CoordinateTransfer.pixelToGrid(new Point(mouseGlobalX, mouseGlobalY));
			var mouseGridPixelPos:Point = CoordinateTransfer.gridToPixel(mouseGridPos);
			_mouseGrid.x = mouseGridPixelPos.x - currentGridPixelPos.x + _layerWidth / 2 - _config.gridWidth / 2;
			_mouseGrid.y = mouseGridPixelPos.y - currentGridPixelPos.y + _layerHeight / 2 - _config.gridHeight / 2;
			
			_needUpdateGrid = false;
			
			//test
			_mouseGrid.tooltipData = "(" + mouseGridPos.x + "," + mouseGridPos.y + ")";
			//end - 用于维护当前格子的代码
		}
		
		/**
		 * 用于绘制确定中心点时地图视图的支持字段和方法族
		 */
		private var _layerWidth:uint = GlobalInfo.stage.stageWidth;
		private var _layerHeight:uint = GlobalInfo.stage.stageHeight;
		private var _currentBlockPos:Point;
		private var _currentGridPos:Point;
		private var _blockRawPoint:Point;
		public function set layerHeight(value:uint):void
		{
			_layerHeight = value;
		}
		public function get layerHeight():uint
		{
			return _layerHeight;
		}
		
		public function set layerWidth(value:uint):void
		{
			_layerWidth = value;
		}
		public function get layerWidth():uint
		{
			return _layerWidth;
		}
		
		public function get currentGrid():Point
		{
			return _currentGridPos;
		}
		
		public function get pixelRawPoint():Point
		{
			return new Point(_currentPoint.x - _layerWidth / 2, _currentPoint.y - _layerHeight / 2);
		}
		
		/**
		 * 当前位于屏幕中央的格子
		 * 该数据可能不实时
		 */
		public function set currentGrid(value:Point):void
		{
			if(	value.x < 0 ||
				value.y < 0 ||
				value.x > (_config.blockWidth * _config.maxColumn - _config.gridWidth / 2) / (_config.gridWidth / 2) ||
				value.y > (_config.blockHeight * _config.maxRow - _config.gridHeight / 2) / _config.gridHeight)
			{
				return;
			}
			
			var pixelPoint:Point = CoordinateTransfer.gridToPixel(value);		
			
			if(setCurrentPixel(pixelPoint))
			{
				_currentGridPos = value;
			}	
		}
		
		/**
		 * 当前位于屏幕中心的像素点全局坐标
		 * 该数据是实时的
		 */
		public function get currentPixel():Point
		{
			return _currentPoint;
		}
		
		public function setCurrentPixel(value:Point):Boolean
		{			
			_currentPoint = value;
			
			var temp:Point = CoordinateTransfer.getBlockPositionByPixel(value);
			if(_currentBlockPos == null || temp.x != _currentBlockPos.x || temp.y != _currentBlockPos.y)
			{
				WorldMapTextureManager.instance.loadAroundBlock(temp.x, temp.y);
				_currentBlockPos = temp;
				_centerBlock.setMapBlockInfo(WorldMapTextureManager.instance.getBlock(_currentBlockPos.x, _currentBlockPos.y));
			}
			
			_blockRawPoint = CoordinateTransfer.getBlockRawPosition(_currentBlockPos.x, _currentBlockPos.y);
			_centerBlock.x = _layerWidth / 2 - (value.x - _blockRawPoint.x);
			_centerBlock.y = _layerHeight / 2 - (value.y - _blockRawPoint.y);
			
			setAroundBlock();
			
			_needUpdateGrid = true;
			return true;
		}
		
		/**
		 * 这个方法是用于判定中间的地块格子周围有哪些格子需要显示，并且把它们显示出来
		 */
		private function setAroundBlock():void
		{
			var dispL:Boolean = _centerBlock.x > 0;
			var dispU:Boolean = _centerBlock.y > 0;
			var dispR:Boolean = (_centerBlock.x + _config.blockWidth) < _layerWidth;
			var dispD:Boolean = (_centerBlock.y + _config.blockHeight) < _layerHeight;
			
			var dispLU:Boolean = dispL && dispU;
			var dispLD:Boolean = dispL && dispD;
			var dispRU:Boolean = dispR && dispU;
			var dispRD:Boolean = dispR && dispD;
			
			if(dispL)
			{
				_horizontalBlock.y = _centerBlock.y;
				_horizontalBlock.x = _centerBlock.x - _config.blockWidth;
				_horizontalBlock.setMapBlockInfo(WorldMapTextureManager.instance.getDirectedBlock(MapBlockType.Left));
				addChildAt(_horizontalBlock, 0);
			}
			else if(dispR)
			{
				_horizontalBlock.y = _centerBlock.y;
				_horizontalBlock.x = _centerBlock.x + _config.blockWidth;
				_horizontalBlock.setMapBlockInfo(WorldMapTextureManager.instance.getDirectedBlock(MapBlockType.Right));
				addChildAt(_horizontalBlock, 0);
			}
			else
			{
				if(contains(_horizontalBlock))
				{
					removeChild(_horizontalBlock);
				}
			}
			
			if(dispU)
			{
				_verticalBlock.x = _centerBlock.x;
				_verticalBlock.y = _centerBlock.y - _config.blockHeight;
				_verticalBlock.setMapBlockInfo(WorldMapTextureManager.instance.getDirectedBlock(MapBlockType.Up));
				addChildAt(_verticalBlock, 0);
			}
			else if(dispD)
			{
				_verticalBlock.x = _centerBlock.x;
				_verticalBlock.y = _centerBlock.y + _config.blockHeight;
				_verticalBlock.setMapBlockInfo(WorldMapTextureManager.instance.getDirectedBlock(MapBlockType.Down));
				addChildAt(_verticalBlock, 0);
			}
			else
			{
				if(contains(_verticalBlock))
				{
					removeChild(_verticalBlock);
				}
			}
			
			if(dispLU)
			{
				_diagonalBlock.x = _centerBlock.x - _config.blockWidth;
				_diagonalBlock.y = _centerBlock.y - _config.blockHeight;
				_diagonalBlock.setMapBlockInfo(WorldMapTextureManager.instance.getDirectedBlock(MapBlockType.LeftUp));
				addChildAt(_diagonalBlock, 0);
			}
			else if(dispLD)
			{
				_diagonalBlock.x = _centerBlock.x - _config.blockWidth;
				_diagonalBlock.y = _centerBlock.y + _config.blockHeight;
				_diagonalBlock.setMapBlockInfo(WorldMapTextureManager.instance.getDirectedBlock(MapBlockType.LeftDown));
				addChildAt(_diagonalBlock, 0);
			}
			else if(dispRU)
			{
				_diagonalBlock.x = _centerBlock.x + _config.blockWidth;
				_diagonalBlock.y = _centerBlock.y - _config.blockHeight;
				_diagonalBlock.setMapBlockInfo(WorldMapTextureManager.instance.getDirectedBlock(MapBlockType.RightUp));
				addChildAt(_diagonalBlock, 0);
			}
			else if(dispRD)
			{
				_diagonalBlock.x = _centerBlock.x + _config.blockWidth;
				_diagonalBlock.y = _centerBlock.y + _config.blockHeight;
				_diagonalBlock.setMapBlockInfo(WorldMapTextureManager.instance.getDirectedBlock(MapBlockType.RightDown));
				addChildAt(_diagonalBlock, 0);
			}
			else
			{
				if(contains(_diagonalBlock))
				{
					removeChild(_diagonalBlock);
				}
			}
		}
	}
}