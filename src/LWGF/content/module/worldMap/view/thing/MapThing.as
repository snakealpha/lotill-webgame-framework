package LWGF.content.module.worldMap.view.thing
{
	import flash.geom.Point;
	
	import LWGF.framework.BaseComponent.DisplayComponent;
	import LWGF.framework.animation.Animation;
	
	public class MapThing extends DisplayComponent
	{
		protected var _basePoint:Point = new Point();
		
		protected var _animation:Animation;
		
		protected var _position:Point;
		
		public function MapThing()
		{
			super();
		}
		
		public function get animation():Animation
		{
			return _animation;
		}
		public function set animation(value:Animation):void
		{
			_animation = value;
			if(_animation.driver != null)
			{
				_animation.currentFrame = 1;
			}
			
			addChild(_animation);
		}
		
		public function get basePoint():Point
		{
			return _basePoint;
		}
		public function set basePoint(value:Point):void
		{
			_basePoint = value;
		}
		
		public function get position():Point
		{
			return _position;
		}
		
		public function set position(value:Point):void
		{
			_position = value;
		}
	}
}