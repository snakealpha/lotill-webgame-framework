package LWGF.content.module.worldMap.view.thing
{
	import flash.geom.Point;
	
	import LWGF.framework.BaseComponent.DisplayComponent;
	import LWGF.framework.global.GlobalInfo;
	import LWGF.content.global.DataManager;
	import LWGF.content.global.GameControllers;
	import LWGF.content.global.data.ModuleConsts;
	import LWGF.content.module.worldMap.WorldMapController;
	import LWGF.content.module.worldMap.data.WorldMapConfig;
	import LWGF.content.module.worldMap.view.utils.CoordinateTransfer;
	
	public class ThingLayer extends DisplayComponent
	{
		protected var _rawPoint:Point = new Point();
		
		protected var _viewWidth:uint = GlobalInfo.stage.stageWidth;
		protected var _viewHeight:uint = GlobalInfo.stage.stageHeight;
		
		protected var _config:WorldMapConfig = DataManager.getConfig(ModuleConsts.worldMap) as WorldMapConfig;
		protected var _ports:Array = [];
		
		public function ThingLayer()
		{
			super();
		}
		
		/**
		 * 设置屏幕中心点的像素位置
		 */
		public function set midPoint(value:Point):void
		{
			_rawPoint.x = value.x - _viewWidth / 2;
			_rawPoint.y = value.y - _viewHeight / 2;
			
			putThings();
		}
		
		public function set viewHeight(value:uint):void
		{
			_viewHeight = value;
			
			putThings();
		}
		
		public function set viewWidth(value:uint):void
		{
			_viewWidth = value;
			
			putThings();
		}
		
		protected function putThings():void
		{
			var thing:MapThing;
			var globalPosition:Point;
			for each(thing in _config.ports)
			{
				globalPosition = CoordinateTransfer.gridToPixel(thing.position);
				thing.x = globalPosition.x - _rawPoint.x - thing.basePoint.x;
				thing.y = globalPosition.y - _rawPoint.y - thing.basePoint.y;
				
				if(thing.x >= 0 - thing.width / 2 &&
				   thing.y >= 0 - thing.height / 2 &&
				   thing.x <= _viewWidth + thing.width / 2 &&
				   thing.y <= _viewHeight + thing.height / 2)
				{
					if(thing.parent != this)
					{
						addChild(thing);
						_ports.push(thing);
					}
				}
			}
			
			for(var i:uint = 0; i != _ports.length; i++)
			{
				thing = _ports[i] as Port;
				
				globalPosition = CoordinateTransfer.gridToPixel(thing.position);
				thing.x = globalPosition.x - _rawPoint.x - thing.basePoint.x;
				thing.y = globalPosition.y - _rawPoint.y - thing.basePoint.y;
				
				if(thing.x < 0 - thing.width / 2 ||
				   thing.y < 0 - thing.height / 2 ||
				   thing.x > _viewWidth + thing.width / 2 ||
				   thing.y > _viewHeight + thing.height / 2)
				{
					if(thing.parent == this)
						removeChild(thing);
					
					_ports.splice(i, 1);
					i--;
				}
			}
		}
	}
}