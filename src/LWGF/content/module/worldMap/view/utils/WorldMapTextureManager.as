package LWGF.content.module.worldMap.view.utils
{
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	
	import LWGF.framework.utils.assertManager.AssertManager;
	import LWGF.framework.utils.assertManager.LoadingInfo;
	import LWGF.content.global.DataManager;
	import LWGF.content.global.GameControllers;
	import LWGF.content.global.data.ModuleConsts;
	import LWGF.content.module.worldMap.WorldMapController;
	import LWGF.content.module.worldMap.data.WorldMapConfig;
	import LWGF.content.module.worldMap.data.virtualObject.WorldMapBlockInfo;
	import LWGF.content.module.worldMap.view.WorldMapView;
	import LWGF.content.module.worldMap.view.map.MapBlockType;

	/**
	 * 综合管理世界地图使用的各种贴图
	 */
	public class WorldMapTextureManager
	{
		private static var _instance:WorldMapTextureManager;		
		private var _config:WorldMapConfig = DataManager.getConfig(ModuleConsts.worldMap) as WorldMapConfig;
		
		private var _aroundBlocks:Dictionary = new Dictionary();
		
		private var _blankMapBlockTexture:BitmapData;
		
		public function WorldMapTextureManager()
		{
		}
		
		public static function get instance():WorldMapTextureManager
		{
			if(_instance == null)
			{
				_instance = new WorldMapTextureManager();
			}
			
			return _instance;
		}
		
		/**
		 * 获取表示海的图案
		 */
		public function getBlankSea():BitmapData
		{
			if(_blankMapBlockTexture == null)
			{
				_blankMapBlockTexture = AssertManager.getInstance("sea_bg") as BitmapData;
			}
			
			return _blankMapBlockTexture;
		}
		
		/**
		 * 获取指定地块地表
		 */
		public function getBlock(column:int, row:int):WorldMapBlockInfo
		{
			var returnValue:WorldMapBlockInfo = new WorldMapBlockInfo(row, column);
			AssertManager.load(	"asserts\\worldMap\\" + column + "-" + row + ".swf", 
				LoadingInfo.MovieClip, 
				onCurrentBlockLoaded, 
				1, 
				returnValue, true);
			
			return returnValue;
		}
		private function onCurrentBlockLoaded(info:LoadingInfo):void
		{
			var returnValue:WorldMapBlockInfo = info.extend as WorldMapBlockInfo;
			returnValue.texture = AssertManager.getInstance("WorldMap_"+returnValue.column+"-"+returnValue.row) as BitmapData;
		}
		
		public function getDirectedBlock(direction:String):WorldMapBlockInfo
		{
			return _aroundBlocks[direction] as WorldMapBlockInfo;
		}
		
		/**
		 * 获取指定位置地表周围的8格地表
		 */
		public function loadAroundBlock(column:int, row:int):void
		{
			var isLeft:Boolean = false;
			var isRight:Boolean = false;
			var isTop:Boolean = false;
			var isBottom:Boolean = false;
			
			if(column <= 0)
				isLeft = true;
			if(column >= _config.maxColumn - 1)
				isRight = true;
			if(row <= 0)
				isTop = true;
			if(row >= _config.maxRow - 1)
				isBottom = true;
			
			var loadQueue:Array = [ {ignore:isLeft||isTop,     row:row-1, column:column-1},
									{ignore:isLeft,            row:row,   column:column-1},
									{ignore:isLeft||isBottom,  row:row+1, column:column-1},
									{ignore:isTop,             row:row-1, column:column},
									{ignore:isBottom,          row:row+1, column:column},
									{ignore:isRight||isTop,    row:row-1, column:column+1},
									{ignore:isRight,           row:row,   column:column+1},
									{ignore:isRight||isBottom, row:row+1, column:column+1}];

			for each(var loadConfig:Object in loadQueue)
			{
				if(loadConfig.ignore)
				{
					_aroundBlocks[getBlockTypeByPoint(column, row, loadConfig.column, loadConfig.row)] = new WorldMapBlockInfo(-1, -1, null);
					
					continue;
				}
				
				var block:WorldMapBlockInfo = getBlock(loadConfig.column, loadConfig.row);
				_aroundBlocks[getBlockTypeByPoint(column, row, loadConfig.column, loadConfig.row)] = block;
			}
		}
		
		/**
		 * 通过地表行列获取某位置地表相对于另一地表的位置
		 */
		public function getBlockTypeByPoint(sourceColumn:int, sourceRow:int, comparerColumn:int, comparerRow:int):String
		{
			if(sourceColumn == comparerColumn && sourceRow == comparerRow)
			{
				return MapBlockType.Center;
			}
			else if(sourceColumn < comparerColumn && sourceRow == comparerRow)
			{
				return MapBlockType.Right;
			}
			else if(sourceColumn == comparerColumn && sourceRow < comparerRow)
			{
				return MapBlockType.Down;
			}
			else if(sourceColumn == comparerColumn && sourceRow > comparerRow)
			{
				return MapBlockType.Up;
			}
			else if(sourceColumn > comparerColumn && sourceRow == comparerRow)
			{
				return MapBlockType.Left;
			}
			else if(sourceColumn < comparerColumn && sourceRow < comparerRow)
			{
				return MapBlockType.RightDown;
			}
			else if(sourceColumn > comparerColumn && sourceRow > comparerRow)
			{
				return MapBlockType.LeftUp;
			}
			else if(sourceColumn < comparerColumn && sourceRow > comparerRow)
			{
				return MapBlockType.RightUp;
			}
			else if(sourceColumn > comparerColumn && sourceRow < comparerRow)
			{
				return MapBlockType.LeftDown;
			}
			return "";
		}
	}
}