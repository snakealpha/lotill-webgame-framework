package LWGF.content.module.worldMap.view.utils
{
	import flash.geom.Point;
	import flash.sampler.NewObjectSample;
	
	import LWGF.framework.utils.Log;
	import LWGF.content.global.DataManager;
	import LWGF.content.global.GameControllers;
	import LWGF.content.global.data.ModuleConsts;
	import LWGF.content.module.worldMap.WorldMapController;
	import LWGF.content.module.worldMap.data.WorldMapConfig;
	import LWGF.content.module.worldMap.view.grid.GridInfoManager;

	/**
	 * 该类用于提供路由以及寻路功能
	 */
	public final class Route
	{
		private static var _config:WorldMapConfig = DataManager.getConfig(ModuleConsts.worldMap) as WorldMapConfig;
		
		public function Route()
		{
		}
		
		/**
		 * 该方法用于缩短路径，针对连续沿x轴移动的情况，缩短路径之后将三个x坐标连续的情况中中间一个坐标去掉，以保证移动的连续性
		 */
		private static function reducePath(rawPath:Array):Array
		{
			return [];
		}
		
		/**
		 * 从周围格子当中取出评分最小的格子所需要启用的评分方法
		 * 但是路径中以前出现过的格子评分超高
		 */
		private static function pathFindingSocrer(fromGrid:Point, toGrid:Point, path:Array):int
		{
			if(!GridInfoManager.getGrid(fromGrid).isPassive)
			{
				return 2000000000;
			}
			
			for(var i:uint = 0; i != path.length; i++)
			{
				if(fromGrid.x == path[i].x && fromGrid.y == path[i])
				{
					return 999999;
				}
			}
			
			var fromPixel:Point = CoordinateTransfer.gridToPixel(fromGrid);
			var toPixel:Point = CoordinateTransfer.gridToPixel(toGrid);
			
			return (toPixel.x - fromPixel.x)*(toPixel.x - fromPixel.x) + (toPixel.y - fromPixel.y)*(toPixel.y - fromPixel.y);
		}
		
		/**
		 * 返回grid和comparerGrid当中评分较低的格子
		 */
		private static function judgeGrid(grid:Point, comparerGrid:Point, toGrid:Point, path:Array):Point
		{
			var gridSocre:int = pathFindingSocrer(grid, toGrid, path);
			var comparerSocre:int = pathFindingSocrer(comparerGrid, toGrid, path);
			
			if(gridSocre >= comparerSocre)
			{
				return comparerGrid;
			}
			else
			{
				return grid;
			}
		}
		
		/**
		 * 寻路
		 * 返回的是移动方向矢量
		 * 暂无复杂的障碍规避算法
		 */
		public static function findPath(fromGrid:Point, toGrid:Point):Array
		{
			var path:Array = [];
			
			if(!GridInfoManager.getGrid(toGrid).isPassive)
			{
				Log.traceLog("Path can not generate. Target Point is not passive.", "worldMap_pathFinding");
				return path;
			}
			
			var currentGrid:Point = new Point(fromGrid.x, fromGrid.y);
			var tempGrid:Point;
			var nextGrid:Point;
			
			while(currentGrid.x != toGrid.x || currentGrid.y != toGrid.y)
			{
				nextGrid = new Point(currentGrid.x - 2, currentGrid.y);
				
				tempGrid = new Point(currentGrid.x + 2, currentGrid.y);
				nextGrid = judgeGrid(tempGrid, nextGrid, toGrid, path);
				
				tempGrid = new Point(currentGrid.x, currentGrid.y - 1);
				nextGrid = judgeGrid(tempGrid, nextGrid, toGrid, path);
				
				tempGrid = new Point(currentGrid.x, currentGrid.y + 1);
				nextGrid = judgeGrid(tempGrid, nextGrid, toGrid, path);
				
				tempGrid = new Point(currentGrid.x - 1, currentGrid.y + (currentGrid.x % 2 == 0?-1:0));
				nextGrid = judgeGrid(tempGrid, nextGrid, toGrid, path);
				
				tempGrid = new Point(currentGrid.x - 1, currentGrid.y + currentGrid.x % 2);
				nextGrid = judgeGrid(tempGrid, nextGrid, toGrid, path);
				
				tempGrid = new Point(currentGrid.x + 1, currentGrid.y + (currentGrid.x % 2 == 0?-1:0));
				nextGrid = judgeGrid(tempGrid, nextGrid, toGrid, path);
				
				tempGrid = new Point(currentGrid.x + 1, currentGrid.y + currentGrid.x % 2);
				nextGrid = judgeGrid(tempGrid, nextGrid, toGrid, path);
				
				path.push(nextGrid);
				currentGrid = nextGrid;
			}
			
			return path;
		}
		
		/**
		 * 预留路由算法
		 * 用于选择航线
		 */
		public static function route(fromGrid:Point, toGrid:Point):Array
		{
			return [];
		}
	}
}