package LWGF.content.module.worldMap.view.utils
{
	import flash.geom.Point;
	
	import LWGF.content.global.DataManager;
	import LWGF.content.global.GameControllers;
	import LWGF.content.global.data.ModuleConsts;
	import LWGF.content.module.worldMap.WorldMapController;
	import LWGF.content.module.worldMap.data.WorldMapConfig;
	import LWGF.content.module.worldMap.view.thing.ship.ShipDirectionType;

	/**
	 * 用于坐标转换的工具类
	 */
	public class CoordinateTransfer
	{		
		private static var _config:WorldMapConfig = DataManager.getConfig(ModuleConsts.worldMap) as WorldMapConfig;
		
		public function CoordinateTransfer()
		{
		}
		
		public static function gridToPixel(gridPoint:Point):Point
		{
			var rectX:int = gridPoint.x * _config.gridWidth / 2;
			var rectY:int = gridPoint.y * _config.gridHeight;
			if(gridPoint.x % 2 != 0)
			{
				rectY += _config.gridHeight / 2;
			}
			
			return new Point(rectX + _config.gridWidth / 2, rectY + _config.gridHeight / 2);
		}
		
		public static function pixelToGrid(pixelPoint:Point):Point
		{
			var boxX:int = pixelPoint.x / _config.gridWidth * 2;
			var boxY:int = pixelPoint.y / _config.gridHeight * 2;
			
			var slopeIsPositive:Boolean = (Math.abs(boxX - boxY) % 2) == 1;
			
			var x:int;
			var y:int;
			
			if(slopeIsPositive)
			{
				if(pixelPoint.x - boxX * _config.gridWidth / 2 >= pixelPoint.y * 2 - boxY * _config.gridHeight)
				{
					x = boxX;
				}
				else
				{
					x = boxX - 1;
				}
			}
			else
			{
				if(0.5 * pixelPoint.x + pixelPoint.y <= 0.5 * (boxX * _config.gridWidth / 2) + (boxY + 1) * _config.gridHeight / 2)
				{
					x = boxX - 1;
				}
				else
				{
					x = boxX;
				}
			}
			
			if(x % 2 == 0)
			{
				y = boxY / 2;
			}
			else
			{
				y = (boxY - 1) / 2;
			}
			return new Point(x, y);
		}
		
		public static function getBlockPositionByPixel(pixelPoint:Point):Point
		{
			var row:uint = Math.floor(pixelPoint.y/_config.blockHeight);
			var column:uint = Math.floor(pixelPoint.x/_config.blockWidth);
			return new Point(column, row);
		}
		
		public static function getGlobalPixelFromBlockPixel(column:int, row:int, pixelPoint:Point):Point
		{
			var blockRaw:Point = getBlockRawPosition(column, row);
			return new Point(pixelPoint.x + blockRaw.x, pixelPoint.y + blockRaw.y);
		}
		
		public static function getBlockPixelFromGlobalPixel(column:int, row:int, pixelPoint:Point):Point
		{
			var blockRaw:Point = getBlockRawPosition(column, row);
			return new Point(pixelPoint.x - blockRaw.x, pixelPoint.y - blockRaw.y);
		}
		
		public static function getBlockRawPosition(column:int, row:int):Point
		{
			return new Point(column * _config.blockWidth, row * _config.blockHeight);
		}
		
		/**
		 * 该方法用于通过移动时起点和终点的坐标获得移动面对的方向
		 */
		public static function getDirectionWithGridMoving(from:Point, to:Point):int
		{
			var fromPixel:Point = gridToPixel(from);
			var toPixel:Point = gridToPixel(to);
			
			if(fromPixel.x == toPixel.x)
			{
				if(fromPixel.y > toPixel.y)
				{
					return ShipDirectionType.Up;
				}
				else
				{
					return ShipDirectionType.Down;
				}
			}
			else if(fromPixel.y == toPixel.y)
			{
				if(fromPixel.x > toPixel.x)
				{
					return ShipDirectionType.Left;
				}
				else
				{
					return ShipDirectionType.Right;
				}
			}
			else if(fromPixel.x < toPixel.x)
			{
				if(fromPixel.y > toPixel.y)
				{
					return ShipDirectionType.UpRight;
				}
				else
				{
					return ShipDirectionType.DownRight;
				}
			}
			else
			{
				if(fromPixel.y > toPixel.y)
				{
					return ShipDirectionType.UpLeft;
				}
				else
				{
					return ShipDirectionType.DownLeft;
				}
			}
		}
	}
}