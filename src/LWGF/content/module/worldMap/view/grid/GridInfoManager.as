package LWGF.content.module.worldMap.view.grid
{
	import flash.geom.Point;

	/**
	 * 方便取格子信息用的辅助类
	 */
	public final class GridInfoManager
	{
		protected static var _gridInfos:Array = [];
		protected static var _defaultGridInfo:GridInfo = new GridInfo(new Point(0, 0));
		
		public function GridInfoManager()
		{
		}
		
		public static function addGrid(grid:GridInfo):void
		{
			if(!_gridInfos[grid.position.x])
			{
				_gridInfos[grid.position.x] = [];
			}
			_gridInfos[grid.position.x][grid.position.y] = grid;
		}
		
		public static function getGrid(position:Point):GridInfo
		{
			if(_gridInfos.hasOwnProperty(position.x) && _gridInfos[position.x].hasOwnProperty(position.y))
			{
				return _gridInfos[position.x][position.y] as GridInfo;
			}
			return _defaultGridInfo;
		}
	}
}