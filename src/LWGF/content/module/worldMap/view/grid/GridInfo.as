package LWGF.content.module.worldMap.view.grid
{
	import flash.geom.Point;

	/**
	 * 用于保存某个格子的相关信息
	 */
	public class GridInfo
	{
		protected var _position:Point;
		protected var _isPassive:Boolean;
		protected var _event:String = "";
		
		public function GridInfo(position:Point, isPassive:Boolean = true, event:String = "")
		{
			_position = position;
			_isPassive = isPassive;
			_event = event;
		}
		
		/**
		 * 格子的坐标位置
		 */
		public function get position():Point
		{
			return _position;
		}
		
		/**
		 * 格子是否允许通过
		 */
		public function get isPassive():Boolean
		{
			return _isPassive;
		}
		
		/**
		 * 在格子上触发的事件
		 */
		public function get event():String
		{
			return _event;
		}
	}
}