package LWGF.content.module.worldMap.view.grid
{
	import flash.display.GraphicsPathCommand;
	
	import LWGF.framework.BaseComponent.DisplayComponent;
	import LWGF.content.global.DataManager;
	import LWGF.content.global.GameControllers;
	import LWGF.content.global.data.ModuleConsts;
	import LWGF.content.module.worldMap.WorldMapController;
	import LWGF.content.module.worldMap.data.WorldMapConfig;
	
	public class MouseGrid extends DisplayComponent
	{
		private static var _config:WorldMapConfig = DataManager.getConfig(ModuleConsts.worldMap) as WorldMapConfig;
		
		public function MouseGrid()
		{
			super();
			
			initUI();
		}
		
		protected function initUI():void
		{
			var gridCommands:Vector.<int> = new Vector.<int>();
			var gridParams:Vector.<Number> = new Vector.<Number>();
			
			gridCommands.push(GraphicsPathCommand.MOVE_TO);
			gridParams.push(0, _config.gridHeight / 2);
			gridCommands.push(GraphicsPathCommand.LINE_TO);
			gridParams.push(_config.gridWidth / 2, 0);
			gridCommands.push(GraphicsPathCommand.LINE_TO);
			gridParams.push(_config.gridWidth, _config.gridHeight / 2);
			gridCommands.push(GraphicsPathCommand.LINE_TO);
			gridParams.push(_config.gridWidth / 2, _config.gridHeight);
			
			graphics.beginFill(0xffc90e, 0.35);
			graphics.drawPath(gridCommands, gridParams);
			graphics.endFill();
			
			//test
			tooltipClass = GridTooltip;
		}
	}
}