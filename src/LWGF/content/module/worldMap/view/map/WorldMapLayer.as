package LWGF.content.module.worldMap.view.map
{
	import BlackCatWorkshop.ASDispatcher.DataEvent;
	import BlackCatWorkshop.ASDispatcher.Dispatcher;
	
	import com.greensock.TweenLite;
	import com.greensock.easing.Linear;
	
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import LWGF.framework.global.GlobalInfo;
	import LWGF.content.global.DataManager;
	import LWGF.content.global.GameControllers;
	import LWGF.content.global.data.ModuleConsts;
	import LWGF.content.module.worldMap.WorldMapController;
	import LWGF.content.module.worldMap.data.WorldMapConfig;
	import LWGF.content.module.worldMap.view.grid.GridInfo;
	import LWGF.content.module.worldMap.view.grid.GridInfoManager;
	import LWGF.content.module.worldMap.view.thing.SurfaceLayer;
	import LWGF.content.module.worldMap.view.thing.ThingLayer;
	import LWGF.content.module.worldMap.view.thing.ship.ShipDirectionType;
	import LWGF.content.module.worldMap.view.thing.ship.ShipLayer;
	import LWGF.content.module.worldMap.view.utils.CoordinateTransfer;
	import LWGF.content.module.worldMap.view.utils.Route;
	
	public class WorldMapLayer extends Sprite
	{
		private var _config:WorldMapConfig = DataManager.getConfig(ModuleConsts.worldMap) as WorldMapConfig;
		
		private var _currentPoint:Point;
		
		//会在地图移动之后调用的回调列表
		private var _movedCallback:Array = [];

		private var _surfaceLayer:SurfaceLayer = new SurfaceLayer();
		private var _thingLayer:ThingLayer = new ThingLayer();
		private var _shipLayer:ShipLayer = new ShipLayer();
		
		private var _selfPath:Array;
		private var _blockMoving:Boolean = false;
		
		public function WorldMapLayer()
		{
			super();
			
			initUI();
		}
		
		private function initUI():void
		{
			addChild(_surfaceLayer);
			
			addChild(_shipLayer);
			
			addChild(_thingLayer);
			
			//测试寻路使用
			addEventListener(MouseEvent.CLICK, onClick);
			//接续沿路径移动之用
			Dispatcher.addEventListener("arriveGrid", function (event:DataEvent):void {runPath();}, ModuleConsts.worldMap);
		}
		
		/**
		 * 在当前版本中，当点击某点时，会导致玩家船只移动到点击点处的菱形格位置
		 */
		private function onClick(event:MouseEvent):void
		{
			var mouseGrid:Point = CoordinateTransfer.pixelToGrid(new Point(_surfaceLayer.currentPixel.x - _surfaceLayer.layerWidth/2 + _surfaceLayer.mouseX, 
																		   _surfaceLayer.currentPixel.y - _surfaceLayer.layerHeight/2 + _surfaceLayer.mouseY));
			
			if(_selfPath != null && 
				_selfPath.length != 0 && 
				_selfPath[_selfPath.length-1].x == mouseGrid.x && 
				_selfPath[_selfPath.length-1].y == mouseGrid.y)
			{
				return;
			}
			
			_selfPath = Route.findPath(_surfaceLayer.currentGrid, mouseGrid);
			runPath();
		}
		
		/**
		 * 按照路径移动玩家的船...其实移动的是地图层，因此该方法没有置于船层 
		 */
		public function runPath():void
		{
			if(_blockMoving)
			{
				_blockMoving = false;
				return;
			}
			
			if(_selfPath.length != 0)
			{
				moveToGrid(_selfPath.shift() as Point);
			}
		}
		
		/**
		 * 阻断玩家船只的移动
		 */
		public function blockRunning():void
		{
			_blockMoving = false;
		}
		
		private var _curPoint:Point = new Point();
		
		/**
		 * 当前位于屏幕中央的格子
		 * 该数据可能不实时
		 */
		public function set currentGrid(value:Point):void
		{
			_surfaceLayer.currentGrid = value;
			_shipLayer.midPoint = CoordinateTransfer.gridToPixel(value);
			_thingLayer.midPoint = CoordinateTransfer.gridToPixel(value);
		}
		
		public function get currentGrid():Point
		{
			return _surfaceLayer.currentGrid;
		}
		
		public function moveToGrid(targetGridPoint:Point):void
		{
			var targetPixelPoint:Point = CoordinateTransfer.gridToPixel(targetGridPoint);
			
			if(	targetGridPoint.x < 0 ||
				targetGridPoint.y < 0 ||
				targetGridPoint.x > (_config.blockWidth * _config.maxColumn - _config.gridWidth / 2) / (_config.gridWidth / 2) ||
				targetGridPoint.y > (_config.blockHeight * _config.maxRow - _config.gridHeight / 2) / _config.gridHeight)
			{
				return;
			}
			
			var currentPixelPoint:Point = _surfaceLayer.currentPixel;
			_shipLayer.playerShip.direction = CoordinateTransfer.getDirectionWithGridMoving(_surfaceLayer.currentGrid, targetGridPoint);
			
			TweenLite.to(currentPixelPoint, 1, 
					{x:targetPixelPoint.x, 
					y:targetPixelPoint.y,
					overwrite:1,
					ease:Linear.easeNone,
					onUpdate:
					function ():void 
					{
						setCurrentPosition(currentPixelPoint);
					}, 
					onComplete:
					function ():void
					{
						currentGrid = targetGridPoint;
						Dispatcher.dispatchEvent("arriveGrid", currentGrid, ModuleConsts.worldMap);
						var gridInfo:GridInfo = GridInfoManager.getGrid(currentGrid);
						if(gridInfo.event != null && gridInfo.event != "")
						{
							Dispatcher.dispatchEvent(gridInfo.event, new Point(currentGrid.x, currentGrid.y), ModuleConsts.worldMap);
						}
					}});
		}
		
		public function setCurrentPosition(value:Point):Boolean
		{
			if(value.x >= _config.blockWidth * _config.maxColumn || value.y >= _config.blockHeight * _config.maxColumn)
			{
				return false;
			}
			
			_shipLayer.midPoint = value;
			_thingLayer.midPoint = value;
			
			_surfaceLayer.setCurrentPixel(value);
			return true;
		}
		
		public function get selfPath():Array
		{
			return _selfPath;
		}
		
		/**
		 * 用于维护其他玩家船只的方法族
		 */
		public function addShip():void
		{
			_shipLayer.addShip();
		}
		
		public function removeShip():void
		{
			_shipLayer.removeShip();
		}
	}
}