package LWGF.content.module.worldMap.view.map
{
	/**
	 * 该类用于标注某一地图块相对于中央地图块的位置
	 */
	public class MapBlockType
	{
		public static const Left:String = "Left";
		public static const Center:String = "Center";
		public static const Right:String = "Right";
		public static const Up:String = "Up";
		public static const Down:String = "Down";
		public static const LeftUp:String = "LeftUp";
		public static const RightUp:String = "RightUp";
		public static const LeftDown:String = "LeftDown";
		public static const RightDown:String = "RightDown";
		
		public function MapBlockType()
		{
		}
	}
}