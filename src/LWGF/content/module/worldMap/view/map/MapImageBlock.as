package LWGF.content.module.worldMap.view.map
{
	import flash.display.Bitmap;
	import flash.display.CapsStyle;
	import flash.display.Shape;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	
	import LWGF.framework.BaseComponent.DisplayComponent;
	import LWGF.content.global.DataManager;
	import LWGF.content.global.GameControllers;
	import LWGF.content.global.data.ModuleConsts;
	import LWGF.content.module.worldMap.WorldMapController;
	import LWGF.content.module.worldMap.data.WorldMapConfig;
	import LWGF.content.module.worldMap.data.virtualObject.WorldMapBlockInfo;
	import LWGF.content.module.worldMap.view.utils.CoordinateTransfer;
	import LWGF.content.module.worldMap.view.utils.WorldMapTextureManager;
	import LWGF.content.uiComponent.TextFormatConsts;
	
	public class MapImageBlock extends DisplayComponent
	{
		private var _seaBg:Bitmap = new Bitmap();
		private var _land:Bitmap = new Bitmap();
		
		private var _config:WorldMapConfig = DataManager.getConfig(ModuleConsts.worldMap) as WorldMapConfig;
				
		private var _blockInfo:WorldMapBlockInfo;
		
		private var _gridLayer:Shape = new Shape();
		
		//供列出格子使用的字段
		private var _tfs:Array = [];
		
		public function MapImageBlock()
		{
			super();
			
			initUI();
		}
		
		private function initUI():void
		{
			_seaBg.bitmapData = WorldMapTextureManager.instance.getBlankSea();
			addChild(_seaBg);
			addChild(_land);
			addChild(_gridLayer);
		}
		
		/**
		 * 当前地图块在整个地图中的行坐标
		 */
		public function get row():int
		{
			return _blockInfo.row;
		}
		
		/**
		 * 当前地图块在整个地图中的列坐标
		 */
		public function get column():int
		{
			return _blockInfo.column;
		}
		
		/**
		 * 获得当前地图块左上角在整个世界地图上的像素坐标
		 */
		public function get blockRawPoint():Point
		{
			var x:uint = column * _config.blockWidth;
			var y:uint = row * _config.blockWidth;
			
			return new Point(x, y);
		}
		
		public function drawGrid():void
		{
			_gridLayer.graphics.clear();
			if(column == -1 || row == -1)
			{
				return;
			}
			
			//绘制菱形网格线
			var b:int = _config.blockHeight - _config.gridHeight / 2;
			_gridLayer.graphics.lineStyle(1, 0xffffff, 0.1);
			for(b; b >= 0; b -= _config.gridHeight)
			{
				_gridLayer.graphics.moveTo(0, b);
				_gridLayer.graphics.lineTo((_config.blockHeight - b) * 2, _config.blockHeight);
			}
			for(b; b >= 0-_config.blockHeight; b -= _config.gridHeight)
			{
				_gridLayer.graphics.moveTo(-2 * b, 0);
				_gridLayer.graphics.lineTo(_config.blockWidth, 0.5 * _config.blockWidth + b);
			}
			
			b = _config.gridHeight / 2;
			for(b; b <= _config.blockHeight; b += _config.gridHeight)
			{
				_gridLayer.graphics.moveTo(0, b);
				_gridLayer.graphics.lineTo(2 * b, 0);
			}
			for(b; b <= _config.blockHeight * 2; b += _config.gridHeight)
			{
				_gridLayer.graphics.moveTo(2 * (b - _config.blockHeight), _config.blockHeight);
				_gridLayer.graphics.lineTo(_config.blockWidth, -0.5 * _config.blockWidth + b);
			}
			
			//绘制边缘图块的边线
			_gridLayer.graphics.lineStyle(16, 0xff0000, 0.3, false, "normal", CapsStyle.NONE);
			if(column == 0)
			{
				if(row == 0)
				{
					_gridLayer.graphics.moveTo(_config.rawPointPixelPoint.x, _config.blockHeight);
					_gridLayer.graphics.lineTo(_config.rawPointPixelPoint.x, _config.rawPointPixelPoint.y);
					_gridLayer.graphics.lineTo(_config.blockWidth, _config.rawPointPixelPoint.y);
				}
				else if(row == _config.maxRow - 1)
				{
					_gridLayer.graphics.moveTo(_config.rawPointPixelPoint.x, 0);
					_gridLayer.graphics.lineTo(_config.rawPointPixelPoint.x, _config.blockHeight - _config.rawPointPixelPoint.y);
					_gridLayer.graphics.lineTo(_config.blockWidth, _config.blockHeight - _config.rawPointPixelPoint.y);
				}
				else
				{
					_gridLayer.graphics.moveTo(_config.rawPointPixelPoint.x, 0);
					_gridLayer.graphics.lineTo(_config.rawPointPixelPoint.x, _config.blockHeight);
				}
			}
			else if(column == _config.maxColumn - 1)
			{
				if(row == 0)
				{
					_gridLayer.graphics.moveTo(0, _config.rawPointPixelPoint.y);
					_gridLayer.graphics.lineTo(_config.blockWidth - _config.rawPointPixelPoint.x, _config.rawPointPixelPoint.y);
					_gridLayer.graphics.lineTo(_config.blockWidth - _config.rawPointPixelPoint.x, _config.blockHeight);
				}
				else if(row == _config.maxRow - 1)
				{
					_gridLayer.graphics.moveTo(_config.blockWidth - _config.rawPointPixelPoint.x, 0);
					_gridLayer.graphics.lineTo(_config.blockWidth - _config.rawPointPixelPoint.x, _config.blockHeight - _config.rawPointPixelPoint.y);
					_gridLayer.graphics.lineTo(0, _config.blockHeight - _config.rawPointPixelPoint.y);
				}
				else
				{
					_gridLayer.graphics.moveTo(_config.blockWidth - _config.rawPointPixelPoint.x, 0);
					_gridLayer.graphics.lineTo(_config.blockWidth - _config.rawPointPixelPoint.x, _config.blockHeight);
				}
			}
			else if(row == 0)
			{
				_gridLayer.graphics.moveTo(0, _config.rawPointPixelPoint.y);
				_gridLayer.graphics.lineTo(_config.blockWidth, _config.rawPointPixelPoint.y);
			}
			else if(row == _config.maxRow - 1)
			{
				_gridLayer.graphics.moveTo(0, _config.blockHeight - _config.rawPointPixelPoint.y);
				_gridLayer.graphics.lineTo(_config.blockWidth, _config.blockHeight - _config.rawPointPixelPoint.y);
			}
		}
		
		public function setMapBlockInfo(blockInfo:WorldMapBlockInfo):void
		{
			_blockInfo = blockInfo;
			_blockInfo.bitmapDataChanged = onBitmapChanged;
			
			//对于处于边缘的格子，需要在格子边上绘制边线
			drawGrid();
		}
		
		private function onBitmapChanged():void
		{
			_land.bitmapData = _blockInfo.texture;
		}
	}
}