package LWGF.content.module.worldMap
{
	import LWGF.framework.BaseComponent.layer.Layer;
	import LWGF.framework.utils.assertManager.LoadingInfo;
	import LWGF.content.global.DataManager;
	import LWGF.content.global.data.ModuleConsts;
	import LWGF.content.module.T3Controller;
	import LWGF.content.module.worldMap.command.WorldMapCommandListener;
	import LWGF.content.module.worldMap.data.WorldMapConfig;
	import LWGF.content.module.worldMap.data.WorldMapEntity;
	import LWGF.content.module.worldMap.view.WorldMapView;
	
	public class WorldMapController extends T3Controller
	{
		private var _command:WorldMapCommandListener = new WorldMapCommandListener();
		private var _entity:WorldMapEntity = new WorldMapEntity();
		private var _config:WorldMapConfig = new WorldMapConfig();
		
		public function WorldMapController()
		{
			super();
			
			DataManager.registerConfig(_config);
			DataManager.registerEntity(_entity);
		}
		
		override public function get ModuleName():String
		{
			return ModuleConsts.worldMap;
		}
		
		override public function get viewClass():Class
		{
			return WorldMapView;
		}
		
		public function get config():WorldMapConfig
		{
			return _config;
		}
		
		public function get entity():WorldMapEntity
		{
			return _entity;
		}
		
		override protected function initBindings():void
		{
			super.initBindings();
			
			_entity.addBinding("postitonValue", _view, "selfPosition");
		}
		
		override public function onCreatedView():void
		{
			_config.initPorts();
		}
		
		override public function onOpenView():void
		{
			super.onOpenView();
			_view.layer.clear();
		}
	}
}