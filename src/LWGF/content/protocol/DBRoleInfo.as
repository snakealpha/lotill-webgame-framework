package LWGF.content.protocol {
	import com.protobuf.*;
	use namespace com.protobuf.used_by_generated_code;
	import com.protobuf.fieldDescriptors.*;
	import flash.utils.Endian;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;
	import flash.errors.IOError;
	import LWGF.content.protocol.DBRoleInfoExtend;
	// @@protoc_insertion_point(imports)

	// @@protoc_insertion_point(class_metadata)
	public dynamic final class DBRoleInfo extends com.protobuf.Message {
		/**
		 *  @private
		 */
		public static const FUIN:FieldDescriptor$TYPE_UINT32 = new FieldDescriptor$TYPE_UINT32("lotill.t3.protocol.DBRoleInfo.FUin", "fUin", (1 << 3) | com.protobuf.WireType.VARINT);

		private var FUin$field:uint;

		private var hasField$0:uint = 0;

		public function clearFUin():void {
			hasField$0 &= 0xfffffffe;
			FUin$field = new uint();
		}

		public function get hasFUin():Boolean {
			return (hasField$0 & 0x1) != 0;
		}

		public function set fUin(value:uint):void {
			hasField$0 |= 0x1;
			FUin$field = value;
		}

		public function get fUin():uint {
			return FUin$field;
		}

		/**
		 *  @private
		 */
		public static const FPLATFORM:FieldDescriptor$TYPE_INT32 = new FieldDescriptor$TYPE_INT32("lotill.t3.protocol.DBRoleInfo.FPlatform", "fPlatform", (2 << 3) | com.protobuf.WireType.VARINT);

		private var FPlatform$field:int;

		public function clearFPlatform():void {
			hasField$0 &= 0xfffffffd;
			FPlatform$field = new int();
		}

		public function get hasFPlatform():Boolean {
			return (hasField$0 & 0x2) != 0;
		}

		public function set fPlatform(value:int):void {
			hasField$0 |= 0x2;
			FPlatform$field = value;
		}

		public function get fPlatform():int {
			return FPlatform$field;
		}

		/**
		 *  @private
		 */
		public static const FACCOUNT:FieldDescriptor$TYPE_STRING = new FieldDescriptor$TYPE_STRING("lotill.t3.protocol.DBRoleInfo.FAccount", "fAccount", (3 << 3) | com.protobuf.WireType.LENGTH_DELIMITED);

		private var FAccount$field:String;

		public function clearFAccount():void {
			FAccount$field = null;
		}

		public function get hasFAccount():Boolean {
			return FAccount$field != null;
		}

		public function set fAccount(value:String):void {
			FAccount$field = value;
		}

		public function get fAccount():String {
			return FAccount$field;
		}

		/**
		 *  @private
		 */
		public static const FNAME:FieldDescriptor$TYPE_STRING = new FieldDescriptor$TYPE_STRING("lotill.t3.protocol.DBRoleInfo.FName", "fName", (4 << 3) | com.protobuf.WireType.LENGTH_DELIMITED);

		private var FName$field:String;

		public function clearFName():void {
			FName$field = null;
		}

		public function get hasFName():Boolean {
			return FName$field != null;
		}

		public function set fName(value:String):void {
			FName$field = value;
		}

		public function get fName():String {
			return FName$field;
		}

		/**
		 *  @private
		 */
		public static const FGENDER:FieldDescriptor$TYPE_INT32 = new FieldDescriptor$TYPE_INT32("lotill.t3.protocol.DBRoleInfo.FGender", "fGender", (5 << 3) | com.protobuf.WireType.VARINT);

		private var FGender$field:int;

		public function clearFGender():void {
			hasField$0 &= 0xfffffffb;
			FGender$field = new int();
		}

		public function get hasFGender():Boolean {
			return (hasField$0 & 0x4) != 0;
		}

		public function set fGender(value:int):void {
			hasField$0 |= 0x4;
			FGender$field = value;
		}

		public function get fGender():int {
			return FGender$field;
		}

		/**
		 *  @private
		 */
		public static const FEXP:FieldDescriptor$TYPE_INT32 = new FieldDescriptor$TYPE_INT32("lotill.t3.protocol.DBRoleInfo.FExp", "fExp", (6 << 3) | com.protobuf.WireType.VARINT);

		private var FExp$field:int;

		public function clearFExp():void {
			hasField$0 &= 0xfffffff7;
			FExp$field = new int();
		}

		public function get hasFExp():Boolean {
			return (hasField$0 & 0x8) != 0;
		}

		public function set fExp(value:int):void {
			hasField$0 |= 0x8;
			FExp$field = value;
		}

		public function get fExp():int {
			return FExp$field;
		}

		/**
		 *  @private
		 */
		public static const FLEVEL:FieldDescriptor$TYPE_INT32 = new FieldDescriptor$TYPE_INT32("lotill.t3.protocol.DBRoleInfo.FLevel", "fLevel", (7 << 3) | com.protobuf.WireType.VARINT);

		private var FLevel$field:int;

		public function clearFLevel():void {
			hasField$0 &= 0xffffffef;
			FLevel$field = new int();
		}

		public function get hasFLevel():Boolean {
			return (hasField$0 & 0x10) != 0;
		}

		public function set fLevel(value:int):void {
			hasField$0 |= 0x10;
			FLevel$field = value;
		}

		public function get fLevel():int {
			return FLevel$field;
		}

		/**
		 *  @private
		 */
		public static const FAVATAR:FieldDescriptor$TYPE_INT32 = new FieldDescriptor$TYPE_INT32("lotill.t3.protocol.DBRoleInfo.FAvatar", "fAvatar", (8 << 3) | com.protobuf.WireType.VARINT);

		private var FAvatar$field:int;

		public function clearFAvatar():void {
			hasField$0 &= 0xffffffdf;
			FAvatar$field = new int();
		}

		public function get hasFAvatar():Boolean {
			return (hasField$0 & 0x20) != 0;
		}

		public function set fAvatar(value:int):void {
			hasField$0 |= 0x20;
			FAvatar$field = value;
		}

		public function get fAvatar():int {
			return FAvatar$field;
		}

		/**
		 *  @private
		 */
		public static const FCREATETIME:FieldDescriptor$TYPE_UINT32 = new FieldDescriptor$TYPE_UINT32("lotill.t3.protocol.DBRoleInfo.FCreateTime", "fCreateTime", (9 << 3) | com.protobuf.WireType.VARINT);

		private var FCreateTime$field:uint;

		public function clearFCreateTime():void {
			hasField$0 &= 0xffffffbf;
			FCreateTime$field = new uint();
		}

		public function get hasFCreateTime():Boolean {
			return (hasField$0 & 0x40) != 0;
		}

		public function set fCreateTime(value:uint):void {
			hasField$0 |= 0x40;
			FCreateTime$field = value;
		}

		public function get fCreateTime():uint {
			return FCreateTime$field;
		}

		/**
		 *  @private
		 */
		public static const FLASTLOGINTIME:FieldDescriptor$TYPE_UINT32 = new FieldDescriptor$TYPE_UINT32("lotill.t3.protocol.DBRoleInfo.FLastLoginTime", "fLastLoginTime", (10 << 3) | com.protobuf.WireType.VARINT);

		private var FLastLoginTime$field:uint;

		public function clearFLastLoginTime():void {
			hasField$0 &= 0xffffff7f;
			FLastLoginTime$field = new uint();
		}

		public function get hasFLastLoginTime():Boolean {
			return (hasField$0 & 0x80) != 0;
		}

		public function set fLastLoginTime(value:uint):void {
			hasField$0 |= 0x80;
			FLastLoginTime$field = value;
		}

		public function get fLastLoginTime():uint {
			return FLastLoginTime$field;
		}

		/**
		 *  @private
		 */
		public static const FLASTLOGOUTTIME:FieldDescriptor$TYPE_UINT32 = new FieldDescriptor$TYPE_UINT32("lotill.t3.protocol.DBRoleInfo.FLastLogoutTime", "fLastLogoutTime", (11 << 3) | com.protobuf.WireType.VARINT);

		private var FLastLogoutTime$field:uint;

		public function clearFLastLogoutTime():void {
			hasField$0 &= 0xfffffeff;
			FLastLogoutTime$field = new uint();
		}

		public function get hasFLastLogoutTime():Boolean {
			return (hasField$0 & 0x100) != 0;
		}

		public function set fLastLogoutTime(value:uint):void {
			hasField$0 |= 0x100;
			FLastLogoutTime$field = value;
		}

		public function get fLastLogoutTime():uint {
			return FLastLogoutTime$field;
		}

		/**
		 *  @private
		 */
		public static const FAVAILABLE:FieldDescriptor$TYPE_UINT32 = new FieldDescriptor$TYPE_UINT32("lotill.t3.protocol.DBRoleInfo.FAvailable", "fAvailable", (12 << 3) | com.protobuf.WireType.VARINT);

		private var FAvailable$field:uint;

		public function clearFAvailable():void {
			hasField$0 &= 0xfffffdff;
			FAvailable$field = new uint();
		}

		public function get hasFAvailable():Boolean {
			return (hasField$0 & 0x200) != 0;
		}

		public function set fAvailable(value:uint):void {
			hasField$0 |= 0x200;
			FAvailable$field = value;
		}

		public function get fAvailable():uint {
			return FAvailable$field;
		}

		/**
		 *  @private
		 */
		public static const FSHIPEXP:FieldDescriptor$TYPE_UINT32 = new FieldDescriptor$TYPE_UINT32("lotill.t3.protocol.DBRoleInfo.FShipExp", "fShipExp", (13 << 3) | com.protobuf.WireType.VARINT);

		private var FShipExp$field:uint;

		public function clearFShipExp():void {
			hasField$0 &= 0xfffffbff;
			FShipExp$field = new uint();
		}

		public function get hasFShipExp():Boolean {
			return (hasField$0 & 0x400) != 0;
		}

		public function set fShipExp(value:uint):void {
			hasField$0 |= 0x400;
			FShipExp$field = value;
		}

		public function get fShipExp():uint {
			return FShipExp$field;
		}

		/**
		 *  @private
		 */
		public static const FEXTEND:FieldDescriptor$TYPE_MESSAGE = new FieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.DBRoleInfo.FExtend", "fExtend", (14 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.DBRoleInfoExtend; });

		private var FExtend$field:LWGF.content.protocol.DBRoleInfoExtend;

		public function clearFExtend():void {
			FExtend$field = null;
		}

		public function get hasFExtend():Boolean {
			return FExtend$field != null;
		}

		public function set fExtend(value:LWGF.content.protocol.DBRoleInfoExtend):void {
			FExtend$field = value;
		}

		public function get fExtend():LWGF.content.protocol.DBRoleInfoExtend {
			return FExtend$field;
		}

		/**
		 *  @private
		 */
		override com.protobuf.used_by_generated_code final function writeToBuffer(output:com.protobuf.WritingBuffer):void {
			if (hasFUin) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 1);
				com.protobuf.WriteUtils.write$TYPE_UINT32(output, FUin$field);
			}
			if (hasFPlatform) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 2);
				com.protobuf.WriteUtils.write$TYPE_INT32(output, FPlatform$field);
			}
			if (hasFAccount) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 3);
				com.protobuf.WriteUtils.write$TYPE_STRING(output, FAccount$field);
			}
			if (hasFName) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 4);
				com.protobuf.WriteUtils.write$TYPE_STRING(output, FName$field);
			}
			if (hasFGender) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 5);
				com.protobuf.WriteUtils.write$TYPE_INT32(output, FGender$field);
			}
			if (hasFExp) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 6);
				com.protobuf.WriteUtils.write$TYPE_INT32(output, FExp$field);
			}
			if (hasFLevel) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 7);
				com.protobuf.WriteUtils.write$TYPE_INT32(output, FLevel$field);
			}
			if (hasFAvatar) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 8);
				com.protobuf.WriteUtils.write$TYPE_INT32(output, FAvatar$field);
			}
			if (hasFCreateTime) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 9);
				com.protobuf.WriteUtils.write$TYPE_UINT32(output, FCreateTime$field);
			}
			if (hasFLastLoginTime) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 10);
				com.protobuf.WriteUtils.write$TYPE_UINT32(output, FLastLoginTime$field);
			}
			if (hasFLastLogoutTime) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 11);
				com.protobuf.WriteUtils.write$TYPE_UINT32(output, FLastLogoutTime$field);
			}
			if (hasFAvailable) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 12);
				com.protobuf.WriteUtils.write$TYPE_UINT32(output, FAvailable$field);
			}
			if (hasFShipExp) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 13);
				com.protobuf.WriteUtils.write$TYPE_UINT32(output, FShipExp$field);
			}
			if (hasFExtend) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 14);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, FExtend$field);
			}
			for (var fieldKey:* in this) {
				super.writeUnknown(output, fieldKey);
			}
		}

		/**
		 *  @private
		 */
		override com.protobuf.used_by_generated_code final function readFromSlice(input:flash.utils.IDataInput, bytesAfterSlice:uint):void {
			var FUin$count:uint = 0;
			var FPlatform$count:uint = 0;
			var FAccount$count:uint = 0;
			var FName$count:uint = 0;
			var FGender$count:uint = 0;
			var FExp$count:uint = 0;
			var FLevel$count:uint = 0;
			var FAvatar$count:uint = 0;
			var FCreateTime$count:uint = 0;
			var FLastLoginTime$count:uint = 0;
			var FLastLogoutTime$count:uint = 0;
			var FAvailable$count:uint = 0;
			var FShipExp$count:uint = 0;
			var FExtend$count:uint = 0;
			while (input.bytesAvailable > bytesAfterSlice) {
				var tag:uint = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
				switch (tag >> 3) {
				case 1:
					if (FUin$count != 0) {
						throw new flash.errors.IOError('Bad data format: DBRoleInfo.fUin cannot be set twice.');
					}
					++FUin$count;
					this.fUin = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
					break;
				case 2:
					if (FPlatform$count != 0) {
						throw new flash.errors.IOError('Bad data format: DBRoleInfo.fPlatform cannot be set twice.');
					}
					++FPlatform$count;
					this.fPlatform = com.protobuf.ReadUtils.read$TYPE_INT32(input);
					break;
				case 3:
					if (FAccount$count != 0) {
						throw new flash.errors.IOError('Bad data format: DBRoleInfo.fAccount cannot be set twice.');
					}
					++FAccount$count;
					this.fAccount = com.protobuf.ReadUtils.read$TYPE_STRING(input);
					break;
				case 4:
					if (FName$count != 0) {
						throw new flash.errors.IOError('Bad data format: DBRoleInfo.fName cannot be set twice.');
					}
					++FName$count;
					this.fName = com.protobuf.ReadUtils.read$TYPE_STRING(input);
					break;
				case 5:
					if (FGender$count != 0) {
						throw new flash.errors.IOError('Bad data format: DBRoleInfo.fGender cannot be set twice.');
					}
					++FGender$count;
					this.fGender = com.protobuf.ReadUtils.read$TYPE_INT32(input);
					break;
				case 6:
					if (FExp$count != 0) {
						throw new flash.errors.IOError('Bad data format: DBRoleInfo.fExp cannot be set twice.');
					}
					++FExp$count;
					this.fExp = com.protobuf.ReadUtils.read$TYPE_INT32(input);
					break;
				case 7:
					if (FLevel$count != 0) {
						throw new flash.errors.IOError('Bad data format: DBRoleInfo.fLevel cannot be set twice.');
					}
					++FLevel$count;
					this.fLevel = com.protobuf.ReadUtils.read$TYPE_INT32(input);
					break;
				case 8:
					if (FAvatar$count != 0) {
						throw new flash.errors.IOError('Bad data format: DBRoleInfo.fAvatar cannot be set twice.');
					}
					++FAvatar$count;
					this.fAvatar = com.protobuf.ReadUtils.read$TYPE_INT32(input);
					break;
				case 9:
					if (FCreateTime$count != 0) {
						throw new flash.errors.IOError('Bad data format: DBRoleInfo.fCreateTime cannot be set twice.');
					}
					++FCreateTime$count;
					this.fCreateTime = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
					break;
				case 10:
					if (FLastLoginTime$count != 0) {
						throw new flash.errors.IOError('Bad data format: DBRoleInfo.fLastLoginTime cannot be set twice.');
					}
					++FLastLoginTime$count;
					this.fLastLoginTime = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
					break;
				case 11:
					if (FLastLogoutTime$count != 0) {
						throw new flash.errors.IOError('Bad data format: DBRoleInfo.fLastLogoutTime cannot be set twice.');
					}
					++FLastLogoutTime$count;
					this.fLastLogoutTime = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
					break;
				case 12:
					if (FAvailable$count != 0) {
						throw new flash.errors.IOError('Bad data format: DBRoleInfo.fAvailable cannot be set twice.');
					}
					++FAvailable$count;
					this.fAvailable = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
					break;
				case 13:
					if (FShipExp$count != 0) {
						throw new flash.errors.IOError('Bad data format: DBRoleInfo.fShipExp cannot be set twice.');
					}
					++FShipExp$count;
					this.fShipExp = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
					break;
				case 14:
					if (FExtend$count != 0) {
						throw new flash.errors.IOError('Bad data format: DBRoleInfo.fExtend cannot be set twice.');
					}
					++FExtend$count;
					this.fExtend = new LWGF.content.protocol.DBRoleInfoExtend();
					com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, this.fExtend);
					break;
				default:
					super.readUnknown(input, tag);
					break;
				}
			}
		}

	}
}
