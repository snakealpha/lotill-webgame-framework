package LWGF.content.protocol {
	import com.protobuf.*;
	use namespace com.protobuf.used_by_generated_code;
	import com.protobuf.fieldDescriptors.*;
	import flash.utils.Endian;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;
	import flash.errors.IOError;
	import LWGF.content.protocol.LogoutResean;
	// @@protoc_insertion_point(imports)

	// @@protoc_insertion_point(class_metadata)
	public dynamic final class LogoutRequest extends com.protobuf.Message {
		/**
		 *  @private
		 */
		public static const UIN:FieldDescriptor$TYPE_UINT32 = new FieldDescriptor$TYPE_UINT32("lotill.t3.protocol.LogoutRequest.uin", "uin", (1 << 3) | com.protobuf.WireType.VARINT);

		private var uin$field:uint;

		private var hasField$0:uint = 0;

		public function clearUin():void {
			hasField$0 &= 0xfffffffe;
			uin$field = new uint();
		}

		public function get hasUin():Boolean {
			return (hasField$0 & 0x1) != 0;
		}

		public function set uin(value:uint):void {
			hasField$0 |= 0x1;
			uin$field = value;
		}

		public function get uin():uint {
			return uin$field;
		}

		/**
		 *  @private
		 */
		public static const REASON:FieldDescriptor$TYPE_ENUM = new FieldDescriptor$TYPE_ENUM("lotill.t3.protocol.LogoutRequest.reason", "reason", (2 << 3) | com.protobuf.WireType.VARINT, LWGF.content.protocol.LogoutResean);

		private var reason$field:int;

		public function clearReason():void {
			hasField$0 &= 0xfffffffd;
			reason$field = new int();
		}

		public function get hasReason():Boolean {
			return (hasField$0 & 0x2) != 0;
		}

		public function set reason(value:int):void {
			hasField$0 |= 0x2;
			reason$field = value;
		}

		public function get reason():int {
			return reason$field;
		}

		/**
		 *  @private
		 */
		override com.protobuf.used_by_generated_code final function writeToBuffer(output:com.protobuf.WritingBuffer):void {
			if (hasUin) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 1);
				com.protobuf.WriteUtils.write$TYPE_UINT32(output, uin$field);
			}
			if (hasReason) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 2);
				com.protobuf.WriteUtils.write$TYPE_ENUM(output, reason$field);
			}
			for (var fieldKey:* in this) {
				super.writeUnknown(output, fieldKey);
			}
		}

		/**
		 *  @private
		 */
		override com.protobuf.used_by_generated_code final function readFromSlice(input:flash.utils.IDataInput, bytesAfterSlice:uint):void {
			var uin$count:uint = 0;
			var reason$count:uint = 0;
			while (input.bytesAvailable > bytesAfterSlice) {
				var tag:uint = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
				switch (tag >> 3) {
				case 1:
					if (uin$count != 0) {
						throw new flash.errors.IOError('Bad data format: LogoutRequest.uin cannot be set twice.');
					}
					++uin$count;
					this.uin = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
					break;
				case 2:
					if (reason$count != 0) {
						throw new flash.errors.IOError('Bad data format: LogoutRequest.reason cannot be set twice.');
					}
					++reason$count;
					this.reason = com.protobuf.ReadUtils.read$TYPE_ENUM(input);
					break;
				default:
					super.readUnknown(input, tag);
					break;
				}
			}
		}

	}
}
