package LWGF.content.protocol {
	import com.protobuf.*;
	use namespace com.protobuf.used_by_generated_code;
	import com.protobuf.fieldDescriptors.*;
	import flash.utils.Endian;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;
	import flash.errors.IOError;
	import LWGF.content.protocol.RoleInfo;
	// @@protoc_insertion_point(imports)

	// @@protoc_insertion_point(class_metadata)
	public dynamic final class LoginResponse extends com.protobuf.Message {
		/**
		 *  @private
		 */
		public static const RESULT:FieldDescriptor$TYPE_INT32 = new FieldDescriptor$TYPE_INT32("lotill.t3.protocol.LoginResponse.result", "result", (1 << 3) | com.protobuf.WireType.VARINT);

		private var result$field:int;

		private var hasField$0:uint = 0;

		public function clearResult():void {
			hasField$0 &= 0xfffffffe;
			result$field = new int();
		}

		public function get hasResult():Boolean {
			return (hasField$0 & 0x1) != 0;
		}

		public function set result(value:int):void {
			hasField$0 |= 0x1;
			result$field = value;
		}

		public function get result():int {
			return result$field;
		}

		/**
		 *  @private
		 */
		public static const ROLEINFO:FieldDescriptor$TYPE_MESSAGE = new FieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.LoginResponse.roleinfo", "roleinfo", (2 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.RoleInfo; });

		private var roleinfo$field:LWGF.content.protocol.RoleInfo;

		public function clearRoleinfo():void {
			roleinfo$field = null;
		}

		public function get hasRoleinfo():Boolean {
			return roleinfo$field != null;
		}

		public function set roleinfo(value:LWGF.content.protocol.RoleInfo):void {
			roleinfo$field = value;
		}

		public function get roleinfo():LWGF.content.protocol.RoleInfo {
			return roleinfo$field;
		}

		/**
		 *  @private
		 */
		override com.protobuf.used_by_generated_code final function writeToBuffer(output:com.protobuf.WritingBuffer):void {
			if (hasResult) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 1);
				com.protobuf.WriteUtils.write$TYPE_INT32(output, result$field);
			}
			if (hasRoleinfo) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 2);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, roleinfo$field);
			}
			for (var fieldKey:* in this) {
				super.writeUnknown(output, fieldKey);
			}
		}

		/**
		 *  @private
		 */
		override com.protobuf.used_by_generated_code final function readFromSlice(input:flash.utils.IDataInput, bytesAfterSlice:uint):void {
			var result$count:uint = 0;
			var roleinfo$count:uint = 0;
			while (input.bytesAvailable > bytesAfterSlice) {
				var tag:uint = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
				switch (tag >> 3) {
				case 1:
					if (result$count != 0) {
						throw new flash.errors.IOError('Bad data format: LoginResponse.result cannot be set twice.');
					}
					++result$count;
					this.result = com.protobuf.ReadUtils.read$TYPE_INT32(input);
					break;
				case 2:
					if (roleinfo$count != 0) {
						throw new flash.errors.IOError('Bad data format: LoginResponse.roleinfo cannot be set twice.');
					}
					++roleinfo$count;
					this.roleinfo = new LWGF.content.protocol.RoleInfo();
					com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, this.roleinfo);
					break;
				default:
					super.readUnknown(input, tag);
					break;
				}
			}
		}

	}
}
