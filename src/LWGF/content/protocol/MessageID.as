package LWGF.content.protocol {
	public final class MessageID {
		public static const MSGID_LISTROLE_REQUEST:int = 100;
		public static const MSGID_LISTROLE_RESPONSE:int = 101;
		public static const MSGID_DELETEROLE_REQUEST:int = 102;
		public static const MSGID_DELETEROLE_RESPONSE:int = 103;
		public static const MSGID_CREATEROLE_REQUEST:int = 104;
		public static const MSGID_CREATEROLE_RESPONSE:int = 105;
		public static const MSGID_LOGIN_REQUEST:int = 106;
		public static const MSGID_LOGIN_RESPONSE:int = 107;
		public static const MSGID_LOGIN_NOTIFY:int = 108;
		public static const MSGID_LOGOUTSERVER_REQUEST:int = 109;
		public static const MSGID_LOGOUTSERVER_RESPONSE:int = 110;
		public static const MSGID_LOGOUTSERVER_NOTIFY:int = 111;
		public static const MSGID_ROLEDB_FETCHROLE_REQUEST:int = 112;
		public static const MSGID_ROLEDB_FETCHROLE_RESPONSE:int = 113;
		public static const MSGID_ROLEDB_UPDATEROLE_REQUEST:int = 114;
		public static const MSGID_ROLEDB_UPDATEROLE_NOTIFY:int = 115;
		public static const MSGID_REALNAMECHECK_REQUEST:int = 116;
		public static const MSGID_REALNAMECHECK_RESPONSE:int = 117;
		public static const MSGID_AASLOGIN_REQUEST:int = 118;
		public static const MSGID_AASLOGIN_RESPONSE:int = 119;
		public static const MSGID_AASADDICT_NOTIFY:int = 120;
		public static const MSGID_AASACTIVE_NOTIFY:int = 121;
		public static const MSGID_KICKROLE_WORLD_REQUEST:int = 122;
		public static const MSGID_KICKROLE_WORLD_RESPONSE:int = 123;
		public static const MSGID_NAME_FINDNAME_REQUEST:int = 124;
		public static const MSGID_NAME_FINDNAME_RESPONSE:int = 125;
		public static const MSGID_NAME_UPDATENAME_REQUEST:int = 126;
		public static const MSGID_NAME_UPDATENAME_RESPONSE:int = 127;
		public static const MSGID_LOGIN_SHIP_NOTIFY:int = 128;
	}
}
