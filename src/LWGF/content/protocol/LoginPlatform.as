package LWGF.content.protocol {
	public final class LoginPlatform {
		public static const LOGIN_PLATFORM_LOTILL:int = 0;
		public static const LOGIN_PLATFORM_QQ:int = 1;
		public static const LOGIN_PLATFORM_SINA:int = 2;
		public static const LOGIN_PLATFORM_360:int = 3;
	}
}
