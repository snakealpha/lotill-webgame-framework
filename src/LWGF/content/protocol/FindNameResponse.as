package LWGF.content.protocol {
	import com.protobuf.*;
	use namespace com.protobuf.used_by_generated_code;
	import com.protobuf.fieldDescriptors.*;
	import flash.utils.Endian;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;
	import flash.errors.IOError;
	// @@protoc_insertion_point(imports)

	// @@protoc_insertion_point(class_metadata)
	public dynamic final class FindNameResponse extends com.protobuf.Message {
		/**
		 *  @private
		 */
		public static const RESULT:FieldDescriptor$TYPE_INT32 = new FieldDescriptor$TYPE_INT32("lotill.t3.protocol.FindNameResponse.result", "result", (1 << 3) | com.protobuf.WireType.VARINT);

		private var result$field:int;

		private var hasField$0:uint = 0;

		public function clearResult():void {
			hasField$0 &= 0xfffffffe;
			result$field = new int();
		}

		public function get hasResult():Boolean {
			return (hasField$0 & 0x1) != 0;
		}

		public function set result(value:int):void {
			hasField$0 |= 0x1;
			result$field = value;
		}

		public function get result():int {
			return result$field;
		}

		/**
		 *  @private
		 */
		public static const NAME:FieldDescriptor$TYPE_STRING = new FieldDescriptor$TYPE_STRING("lotill.t3.protocol.FindNameResponse.name", "name", (2 << 3) | com.protobuf.WireType.LENGTH_DELIMITED);

		private var name$field:String;

		public function clearName():void {
			name$field = null;
		}

		public function get hasName():Boolean {
			return name$field != null;
		}

		public function set name(value:String):void {
			name$field = value;
		}

		public function get name():String {
			return name$field;
		}

		/**
		 *  @private
		 */
		public static const NAME_TYPE:FieldDescriptor$TYPE_INT32 = new FieldDescriptor$TYPE_INT32("lotill.t3.protocol.FindNameResponse.name_type", "nameType", (3 << 3) | com.protobuf.WireType.VARINT);

		private var name_type$field:int;

		public function clearNameType():void {
			hasField$0 &= 0xfffffffd;
			name_type$field = new int();
		}

		public function get hasNameType():Boolean {
			return (hasField$0 & 0x2) != 0;
		}

		public function set nameType(value:int):void {
			hasField$0 |= 0x2;
			name_type$field = value;
		}

		public function get nameType():int {
			return name_type$field;
		}

		/**
		 *  @private
		 */
		public static const ACCOUNT:FieldDescriptor$TYPE_STRING = new FieldDescriptor$TYPE_STRING("lotill.t3.protocol.FindNameResponse.account", "account", (4 << 3) | com.protobuf.WireType.LENGTH_DELIMITED);

		private var account$field:String;

		public function clearAccount():void {
			account$field = null;
		}

		public function get hasAccount():Boolean {
			return account$field != null;
		}

		public function set account(value:String):void {
			account$field = value;
		}

		public function get account():String {
			return account$field;
		}

		/**
		 *  @private
		 */
		override com.protobuf.used_by_generated_code final function writeToBuffer(output:com.protobuf.WritingBuffer):void {
			if (hasResult) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 1);
				com.protobuf.WriteUtils.write$TYPE_INT32(output, result$field);
			}
			if (hasName) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 2);
				com.protobuf.WriteUtils.write$TYPE_STRING(output, name$field);
			}
			if (hasNameType) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 3);
				com.protobuf.WriteUtils.write$TYPE_INT32(output, name_type$field);
			}
			if (hasAccount) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 4);
				com.protobuf.WriteUtils.write$TYPE_STRING(output, account$field);
			}
			for (var fieldKey:* in this) {
				super.writeUnknown(output, fieldKey);
			}
		}

		/**
		 *  @private
		 */
		override com.protobuf.used_by_generated_code final function readFromSlice(input:flash.utils.IDataInput, bytesAfterSlice:uint):void {
			var result$count:uint = 0;
			var name$count:uint = 0;
			var name_type$count:uint = 0;
			var account$count:uint = 0;
			while (input.bytesAvailable > bytesAfterSlice) {
				var tag:uint = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
				switch (tag >> 3) {
				case 1:
					if (result$count != 0) {
						throw new flash.errors.IOError('Bad data format: FindNameResponse.result cannot be set twice.');
					}
					++result$count;
					this.result = com.protobuf.ReadUtils.read$TYPE_INT32(input);
					break;
				case 2:
					if (name$count != 0) {
						throw new flash.errors.IOError('Bad data format: FindNameResponse.name cannot be set twice.');
					}
					++name$count;
					this.name = com.protobuf.ReadUtils.read$TYPE_STRING(input);
					break;
				case 3:
					if (name_type$count != 0) {
						throw new flash.errors.IOError('Bad data format: FindNameResponse.nameType cannot be set twice.');
					}
					++name_type$count;
					this.nameType = com.protobuf.ReadUtils.read$TYPE_INT32(input);
					break;
				case 4:
					if (account$count != 0) {
						throw new flash.errors.IOError('Bad data format: FindNameResponse.account cannot be set twice.');
					}
					++account$count;
					this.account = com.protobuf.ReadUtils.read$TYPE_STRING(input);
					break;
				default:
					super.readUnknown(input, tag);
					break;
				}
			}
		}

	}
}
