package LWGF.content.protocol {
	import com.protobuf.*;
	use namespace com.protobuf.used_by_generated_code;
	import com.protobuf.fieldDescriptors.*;
	import flash.utils.Endian;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;
	import flash.errors.IOError;
	// @@protoc_insertion_point(imports)

	// @@protoc_insertion_point(class_metadata)
	public dynamic final class Ship extends com.protobuf.Message {
		/**
		 *  @private
		 */
		public static const ID:FieldDescriptor$TYPE_UINT32 = new FieldDescriptor$TYPE_UINT32("lotill.t3.protocol.Ship.id", "id", (1 << 3) | com.protobuf.WireType.VARINT);

		private var id$field:uint;

		private var hasField$0:uint = 0;

		public function clearId():void {
			hasField$0 &= 0xfffffffe;
			id$field = new uint();
		}

		public function get hasId():Boolean {
			return (hasField$0 & 0x1) != 0;
		}

		public function set id(value:uint):void {
			hasField$0 |= 0x1;
			id$field = value;
		}

		public function get id():uint {
			return id$field;
		}

		/**
		 *  @private
		 */
		public static const HULL:FieldDescriptor$TYPE_UINT32 = new FieldDescriptor$TYPE_UINT32("lotill.t3.protocol.Ship.hull", "hull", (2 << 3) | com.protobuf.WireType.VARINT);

		private var hull$field:uint;

		public function clearHull():void {
			hasField$0 &= 0xfffffffd;
			hull$field = new uint();
		}

		public function get hasHull():Boolean {
			return (hasField$0 & 0x2) != 0;
		}

		public function set hull(value:uint):void {
			hasField$0 |= 0x2;
			hull$field = value;
		}

		public function get hull():uint {
			return hull$field;
		}

		/**
		 *  @private
		 */
		public static const ARTILLERY:FieldDescriptor$TYPE_UINT32 = new FieldDescriptor$TYPE_UINT32("lotill.t3.protocol.Ship.artillery", "artillery", (3 << 3) | com.protobuf.WireType.VARINT);

		private var artillery$field:uint;

		public function clearArtillery():void {
			hasField$0 &= 0xfffffffb;
			artillery$field = new uint();
		}

		public function get hasArtillery():Boolean {
			return (hasField$0 & 0x4) != 0;
		}

		public function set artillery(value:uint):void {
			hasField$0 |= 0x4;
			artillery$field = value;
		}

		public function get artillery():uint {
			return artillery$field;
		}

		/**
		 *  @private
		 */
		public static const SAIL:FieldDescriptor$TYPE_UINT32 = new FieldDescriptor$TYPE_UINT32("lotill.t3.protocol.Ship.sail", "sail", (4 << 3) | com.protobuf.WireType.VARINT);

		private var sail$field:uint;

		public function clearSail():void {
			hasField$0 &= 0xfffffff7;
			sail$field = new uint();
		}

		public function get hasSail():Boolean {
			return (hasField$0 & 0x8) != 0;
		}

		public function set sail(value:uint):void {
			hasField$0 |= 0x8;
			sail$field = value;
		}

		public function get sail():uint {
			return sail$field;
		}

		/**
		 *  @private
		 */
		public static const MAST:FieldDescriptor$TYPE_UINT32 = new FieldDescriptor$TYPE_UINT32("lotill.t3.protocol.Ship.mast", "mast", (5 << 3) | com.protobuf.WireType.VARINT);

		private var mast$field:uint;

		public function clearMast():void {
			hasField$0 &= 0xffffffef;
			mast$field = new uint();
		}

		public function get hasMast():Boolean {
			return (hasField$0 & 0x10) != 0;
		}

		public function set mast(value:uint):void {
			hasField$0 |= 0x10;
			mast$field = value;
		}

		public function get mast():uint {
			return mast$field;
		}

		/**
		 *  @private
		 */
		public static const CABIN:FieldDescriptor$TYPE_UINT32 = new FieldDescriptor$TYPE_UINT32("lotill.t3.protocol.Ship.cabin", "cabin", (6 << 3) | com.protobuf.WireType.VARINT);

		private var cabin$field:uint;

		public function clearCabin():void {
			hasField$0 &= 0xffffffdf;
			cabin$field = new uint();
		}

		public function get hasCabin():Boolean {
			return (hasField$0 & 0x20) != 0;
		}

		public function set cabin(value:uint):void {
			hasField$0 |= 0x20;
			cabin$field = value;
		}

		public function get cabin():uint {
			return cabin$field;
		}

		/**
		 *  @private
		 */
		override com.protobuf.used_by_generated_code final function writeToBuffer(output:com.protobuf.WritingBuffer):void {
			if (hasId) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 1);
				com.protobuf.WriteUtils.write$TYPE_UINT32(output, id$field);
			}
			if (hasHull) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 2);
				com.protobuf.WriteUtils.write$TYPE_UINT32(output, hull$field);
			}
			if (hasArtillery) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 3);
				com.protobuf.WriteUtils.write$TYPE_UINT32(output, artillery$field);
			}
			if (hasSail) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 4);
				com.protobuf.WriteUtils.write$TYPE_UINT32(output, sail$field);
			}
			if (hasMast) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 5);
				com.protobuf.WriteUtils.write$TYPE_UINT32(output, mast$field);
			}
			if (hasCabin) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 6);
				com.protobuf.WriteUtils.write$TYPE_UINT32(output, cabin$field);
			}
			for (var fieldKey:* in this) {
				super.writeUnknown(output, fieldKey);
			}
		}

		/**
		 *  @private
		 */
		override com.protobuf.used_by_generated_code final function readFromSlice(input:flash.utils.IDataInput, bytesAfterSlice:uint):void {
			var id$count:uint = 0;
			var hull$count:uint = 0;
			var artillery$count:uint = 0;
			var sail$count:uint = 0;
			var mast$count:uint = 0;
			var cabin$count:uint = 0;
			while (input.bytesAvailable > bytesAfterSlice) {
				var tag:uint = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
				switch (tag >> 3) {
				case 1:
					if (id$count != 0) {
						throw new flash.errors.IOError('Bad data format: Ship.id cannot be set twice.');
					}
					++id$count;
					this.id = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
					break;
				case 2:
					if (hull$count != 0) {
						throw new flash.errors.IOError('Bad data format: Ship.hull cannot be set twice.');
					}
					++hull$count;
					this.hull = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
					break;
				case 3:
					if (artillery$count != 0) {
						throw new flash.errors.IOError('Bad data format: Ship.artillery cannot be set twice.');
					}
					++artillery$count;
					this.artillery = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
					break;
				case 4:
					if (sail$count != 0) {
						throw new flash.errors.IOError('Bad data format: Ship.sail cannot be set twice.');
					}
					++sail$count;
					this.sail = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
					break;
				case 5:
					if (mast$count != 0) {
						throw new flash.errors.IOError('Bad data format: Ship.mast cannot be set twice.');
					}
					++mast$count;
					this.mast = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
					break;
				case 6:
					if (cabin$count != 0) {
						throw new flash.errors.IOError('Bad data format: Ship.cabin cannot be set twice.');
					}
					++cabin$count;
					this.cabin = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
					break;
				default:
					super.readUnknown(input, tag);
					break;
				}
			}
		}

	}
}
