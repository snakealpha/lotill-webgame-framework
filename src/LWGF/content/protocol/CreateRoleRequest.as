package LWGF.content.protocol {
	import com.protobuf.*;
	use namespace com.protobuf.used_by_generated_code;
	import com.protobuf.fieldDescriptors.*;
	import flash.utils.Endian;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;
	import flash.errors.IOError;
	// @@protoc_insertion_point(imports)

	// @@protoc_insertion_point(class_metadata)
	public dynamic final class CreateRoleRequest extends com.protobuf.Message {
		/**
		 *  @private
		 */
		public static const ACCOUNT:FieldDescriptor$TYPE_STRING = new FieldDescriptor$TYPE_STRING("lotill.t3.protocol.CreateRoleRequest.account", "account", (1 << 3) | com.protobuf.WireType.LENGTH_DELIMITED);

		private var account$field:String;

		public function clearAccount():void {
			account$field = null;
		}

		public function get hasAccount():Boolean {
			return account$field != null;
		}

		public function set account(value:String):void {
			account$field = value;
		}

		public function get account():String {
			return account$field;
		}

		/**
		 *  @private
		 */
		public static const GENDER:FieldDescriptor$TYPE_INT32 = new FieldDescriptor$TYPE_INT32("lotill.t3.protocol.CreateRoleRequest.gender", "gender", (2 << 3) | com.protobuf.WireType.VARINT);

		private var gender$field:int;

		private var hasField$0:uint = 0;

		public function clearGender():void {
			hasField$0 &= 0xfffffffe;
			gender$field = new int();
		}

		public function get hasGender():Boolean {
			return (hasField$0 & 0x1) != 0;
		}

		public function set gender(value:int):void {
			hasField$0 |= 0x1;
			gender$field = value;
		}

		public function get gender():int {
			return gender$field;
		}

		/**
		 *  @private
		 */
		public static const AVATAR:FieldDescriptor$TYPE_INT32 = new FieldDescriptor$TYPE_INT32("lotill.t3.protocol.CreateRoleRequest.avatar", "avatar", (3 << 3) | com.protobuf.WireType.VARINT);

		private var avatar$field:int;

		public function clearAvatar():void {
			hasField$0 &= 0xfffffffd;
			avatar$field = new int();
		}

		public function get hasAvatar():Boolean {
			return (hasField$0 & 0x2) != 0;
		}

		public function set avatar(value:int):void {
			hasField$0 |= 0x2;
			avatar$field = value;
		}

		public function get avatar():int {
			return avatar$field;
		}

		/**
		 *  @private
		 */
		public static const NAME:FieldDescriptor$TYPE_STRING = new FieldDescriptor$TYPE_STRING("lotill.t3.protocol.CreateRoleRequest.name", "name", (4 << 3) | com.protobuf.WireType.LENGTH_DELIMITED);

		private var name$field:String;

		public function clearName():void {
			name$field = null;
		}

		public function get hasName():Boolean {
			return name$field != null;
		}

		public function set name(value:String):void {
			name$field = value;
		}

		public function get name():String {
			return name$field;
		}

		/**
		 *  @private
		 */
		public static const WORLD:FieldDescriptor$TYPE_INT32 = new FieldDescriptor$TYPE_INT32("lotill.t3.protocol.CreateRoleRequest.world", "world", (5 << 3) | com.protobuf.WireType.VARINT);

		private var world$field:int;

		public function clearWorld():void {
			hasField$0 &= 0xfffffffb;
			world$field = new int();
		}

		public function get hasWorld():Boolean {
			return (hasField$0 & 0x4) != 0;
		}

		public function set world(value:int):void {
			hasField$0 |= 0x4;
			world$field = value;
		}

		public function get world():int {
			return world$field;
		}

		/**
		 *  @private
		 */
		override com.protobuf.used_by_generated_code final function writeToBuffer(output:com.protobuf.WritingBuffer):void {
			if (hasAccount) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 1);
				com.protobuf.WriteUtils.write$TYPE_STRING(output, account$field);
			}
			if (hasGender) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 2);
				com.protobuf.WriteUtils.write$TYPE_INT32(output, gender$field);
			}
			if (hasAvatar) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 3);
				com.protobuf.WriteUtils.write$TYPE_INT32(output, avatar$field);
			}
			if (hasName) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 4);
				com.protobuf.WriteUtils.write$TYPE_STRING(output, name$field);
			}
			if (hasWorld) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 5);
				com.protobuf.WriteUtils.write$TYPE_INT32(output, world$field);
			}
			for (var fieldKey:* in this) {
				super.writeUnknown(output, fieldKey);
			}
		}

		/**
		 *  @private
		 */
		override com.protobuf.used_by_generated_code final function readFromSlice(input:flash.utils.IDataInput, bytesAfterSlice:uint):void {
			var account$count:uint = 0;
			var gender$count:uint = 0;
			var avatar$count:uint = 0;
			var name$count:uint = 0;
			var world$count:uint = 0;
			while (input.bytesAvailable > bytesAfterSlice) {
				var tag:uint = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
				switch (tag >> 3) {
				case 1:
					if (account$count != 0) {
						throw new flash.errors.IOError('Bad data format: CreateRoleRequest.account cannot be set twice.');
					}
					++account$count;
					this.account = com.protobuf.ReadUtils.read$TYPE_STRING(input);
					break;
				case 2:
					if (gender$count != 0) {
						throw new flash.errors.IOError('Bad data format: CreateRoleRequest.gender cannot be set twice.');
					}
					++gender$count;
					this.gender = com.protobuf.ReadUtils.read$TYPE_INT32(input);
					break;
				case 3:
					if (avatar$count != 0) {
						throw new flash.errors.IOError('Bad data format: CreateRoleRequest.avatar cannot be set twice.');
					}
					++avatar$count;
					this.avatar = com.protobuf.ReadUtils.read$TYPE_INT32(input);
					break;
				case 4:
					if (name$count != 0) {
						throw new flash.errors.IOError('Bad data format: CreateRoleRequest.name cannot be set twice.');
					}
					++name$count;
					this.name = com.protobuf.ReadUtils.read$TYPE_STRING(input);
					break;
				case 5:
					if (world$count != 0) {
						throw new flash.errors.IOError('Bad data format: CreateRoleRequest.world cannot be set twice.');
					}
					++world$count;
					this.world = com.protobuf.ReadUtils.read$TYPE_INT32(input);
					break;
				default:
					super.readUnknown(input, tag);
					break;
				}
			}
		}

	}
}
