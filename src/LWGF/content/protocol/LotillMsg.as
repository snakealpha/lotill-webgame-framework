package LWGF.content.protocol {
	import com.protobuf.*;
	use namespace com.protobuf.used_by_generated_code;
	import com.protobuf.fieldDescriptors.*;
	import flash.utils.Endian;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;
	import flash.errors.IOError;
	import LWGF.content.protocol.LoginNotify;
	import LWGF.content.protocol.FindNameResponse;
	import LWGF.content.protocol.FetchRoleRequest;
	import LWGF.content.protocol.UpdateNameResponse;
	import LWGF.content.protocol.LoginResponse;
	import LWGF.content.protocol.ListRoleRequest;
	import LWGF.content.protocol.LoginRequest;
	import LWGF.content.protocol.CreateRoleRequest;
	import LWGF.content.protocol.LogoutResponse;
	import LWGF.content.protocol.LogoutNotify;
	import LWGF.content.protocol.FindNameRequest;
	import LWGF.content.protocol.UpdateNameRequest;
	import LWGF.content.protocol.UpdateRoleNotify;
	import LWGF.content.protocol.LoginShipNotify;
	import LWGF.content.protocol.LogoutRequest;
	import LWGF.content.protocol.KickRoleResponse;
	import LWGF.content.protocol.LoginPlatform;
	import LWGF.content.protocol.CreateRoleResponse;
	import LWGF.content.protocol.ListRoleResponse;
	import LWGF.content.protocol.FetchRoleResponse;
	import LWGF.content.protocol.KickRoleRequest;
	// @@protoc_insertion_point(imports)

	// @@protoc_insertion_point(class_metadata)
	public dynamic final class LotillMsg extends com.protobuf.Message {
		/**
		 *  @private
		 */
		public static const MSGID:FieldDescriptor$TYPE_INT32 = new FieldDescriptor$TYPE_INT32("lotill.t3.protocol.LotillMsg.msgid", "msgid", (1 << 3) | com.protobuf.WireType.VARINT);

		public var msgid:int;

		/**
		 *  @private
		 */
		public static const UIN:FieldDescriptor$TYPE_INT32 = new FieldDescriptor$TYPE_INT32("lotill.t3.protocol.LotillMsg.uin", "uin", (2 << 3) | com.protobuf.WireType.VARINT);

		private var uin$field:int;

		private var hasField$0:uint = 0;

		public function clearUin():void {
			hasField$0 &= 0xfffffffe;
			uin$field = new int();
		}

		public function get hasUin():Boolean {
			return (hasField$0 & 0x1) != 0;
		}

		public function set uin(value:int):void {
			hasField$0 |= 0x1;
			uin$field = value;
		}

		public function get uin():int {
			return uin$field;
		}

		/**
		 *  @private
		 */
		public static const ACCOUNT:FieldDescriptor$TYPE_STRING = new FieldDescriptor$TYPE_STRING("lotill.t3.protocol.LotillMsg.account", "account", (3 << 3) | com.protobuf.WireType.LENGTH_DELIMITED);

		private var account$field:String;

		public function clearAccount():void {
			account$field = null;
		}

		public function get hasAccount():Boolean {
			return account$field != null;
		}

		public function set account(value:String):void {
			account$field = value;
		}

		public function get account():String {
			return account$field;
		}

		/**
		 *  @private
		 */
		public static const SESSION_KEY:FieldDescriptor$TYPE_STRING = new FieldDescriptor$TYPE_STRING("lotill.t3.protocol.LotillMsg.session_key", "sessionKey", (4 << 3) | com.protobuf.WireType.LENGTH_DELIMITED);

		private var session_key$field:String;

		public function clearSessionKey():void {
			session_key$field = null;
		}

		public function get hasSessionKey():Boolean {
			return session_key$field != null;
		}

		public function set sessionKey(value:String):void {
			session_key$field = value;
		}

		public function get sessionKey():String {
			return session_key$field;
		}

		/**
		 *  @private
		 */
		public static const FEED_BACK:FieldDescriptor$TYPE_INT32 = new FieldDescriptor$TYPE_INT32("lotill.t3.protocol.LotillMsg.feed_back", "feedBack", (5 << 3) | com.protobuf.WireType.VARINT);

		private var feed_back$field:int;

		public function clearFeedBack():void {
			hasField$0 &= 0xfffffffd;
			feed_back$field = new int();
		}

		public function get hasFeedBack():Boolean {
			return (hasField$0 & 0x2) != 0;
		}

		public function set feedBack(value:int):void {
			hasField$0 |= 0x2;
			feed_back$field = value;
		}

		public function get feedBack():int {
			return feed_back$field;
		}

		/**
		 *  @private
		 */
		public static const TIME_STAMP:FieldDescriptor$TYPE_INT64 = new FieldDescriptor$TYPE_INT64("lotill.t3.protocol.LotillMsg.time_stamp", "timeStamp", (6 << 3) | com.protobuf.WireType.VARINT);

		private var time_stamp$field:Int64;

		public function clearTimeStamp():void {
			time_stamp$field = null;
		}

		public function get hasTimeStamp():Boolean {
			return time_stamp$field != null;
		}

		public function set timeStamp(value:Int64):void {
			time_stamp$field = value;
		}

		public function get timeStamp():Int64 {
			return time_stamp$field;
		}

		/**
		 *  @private
		 */
		public static const PLATFORM:FieldDescriptor$TYPE_ENUM = new FieldDescriptor$TYPE_ENUM("lotill.t3.protocol.LotillMsg.platform", "platform", (7 << 3) | com.protobuf.WireType.VARINT, LWGF.content.protocol.LoginPlatform);

		private var platform$field:int;

		public function clearPlatform():void {
			hasField$0 &= 0xfffffffb;
			platform$field = new int();
		}

		public function get hasPlatform():Boolean {
			return (hasField$0 & 0x4) != 0;
		}

		public function set platform(value:int):void {
			hasField$0 |= 0x4;
			platform$field = value;
		}

		public function get platform():int {
			return platform$field;
		}

		/**
		 *  @private
		 */
		public static const CLIENT_DEBUG_INFO:FieldDescriptor$TYPE_STRING = new FieldDescriptor$TYPE_STRING("lotill.t3.protocol.LotillMsg.client_debug_info", "clientDebugInfo", (8 << 3) | com.protobuf.WireType.LENGTH_DELIMITED);

		private var client_debug_info$field:String;

		public function clearClientDebugInfo():void {
			client_debug_info$field = null;
		}

		public function get hasClientDebugInfo():Boolean {
			return client_debug_info$field != null;
		}

		public function set clientDebugInfo(value:String):void {
			client_debug_info$field = value;
		}

		public function get clientDebugInfo():String {
			return client_debug_info$field;
		}

		/**
		 *  @private
		 */
		public static const LIST_ROLE_REQUEST:FieldDescriptor$TYPE_MESSAGE = new FieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.LotillMsg.list_role_request", "listRoleRequest", (11 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.ListRoleRequest; });

		private var list_role_request$field:LWGF.content.protocol.ListRoleRequest;

		public function clearListRoleRequest():void {
			list_role_request$field = null;
		}

		public function get hasListRoleRequest():Boolean {
			return list_role_request$field != null;
		}

		public function set listRoleRequest(value:LWGF.content.protocol.ListRoleRequest):void {
			list_role_request$field = value;
		}

		public function get listRoleRequest():LWGF.content.protocol.ListRoleRequest {
			return list_role_request$field;
		}

		/**
		 *  @private
		 */
		public static const LIST_ROLE_RESPONSE:FieldDescriptor$TYPE_MESSAGE = new FieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.LotillMsg.list_role_response", "listRoleResponse", (12 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.ListRoleResponse; });

		private var list_role_response$field:LWGF.content.protocol.ListRoleResponse;

		public function clearListRoleResponse():void {
			list_role_response$field = null;
		}

		public function get hasListRoleResponse():Boolean {
			return list_role_response$field != null;
		}

		public function set listRoleResponse(value:LWGF.content.protocol.ListRoleResponse):void {
			list_role_response$field = value;
		}

		public function get listRoleResponse():LWGF.content.protocol.ListRoleResponse {
			return list_role_response$field;
		}

		/**
		 *  @private
		 */
		public static const CREATE_ROLE_REQUEST:FieldDescriptor$TYPE_MESSAGE = new FieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.LotillMsg.create_role_request", "createRoleRequest", (13 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.CreateRoleRequest; });

		private var create_role_request$field:LWGF.content.protocol.CreateRoleRequest;

		public function clearCreateRoleRequest():void {
			create_role_request$field = null;
		}

		public function get hasCreateRoleRequest():Boolean {
			return create_role_request$field != null;
		}

		public function set createRoleRequest(value:LWGF.content.protocol.CreateRoleRequest):void {
			create_role_request$field = value;
		}

		public function get createRoleRequest():LWGF.content.protocol.CreateRoleRequest {
			return create_role_request$field;
		}

		/**
		 *  @private
		 */
		public static const CREATE_ROLE_RESPONSE:FieldDescriptor$TYPE_MESSAGE = new FieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.LotillMsg.create_role_response", "createRoleResponse", (14 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.CreateRoleResponse; });

		private var create_role_response$field:LWGF.content.protocol.CreateRoleResponse;

		public function clearCreateRoleResponse():void {
			create_role_response$field = null;
		}

		public function get hasCreateRoleResponse():Boolean {
			return create_role_response$field != null;
		}

		public function set createRoleResponse(value:LWGF.content.protocol.CreateRoleResponse):void {
			create_role_response$field = value;
		}

		public function get createRoleResponse():LWGF.content.protocol.CreateRoleResponse {
			return create_role_response$field;
		}

		/**
		 *  @private
		 */
		public static const LOGIN_REQUEST:FieldDescriptor$TYPE_MESSAGE = new FieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.LotillMsg.login_request", "loginRequest", (15 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.LoginRequest; });

		private var login_request$field:LWGF.content.protocol.LoginRequest;

		public function clearLoginRequest():void {
			login_request$field = null;
		}

		public function get hasLoginRequest():Boolean {
			return login_request$field != null;
		}

		public function set loginRequest(value:LWGF.content.protocol.LoginRequest):void {
			login_request$field = value;
		}

		public function get loginRequest():LWGF.content.protocol.LoginRequest {
			return login_request$field;
		}

		/**
		 *  @private
		 */
		public static const LOGIN_RESPONSE:FieldDescriptor$TYPE_MESSAGE = new FieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.LotillMsg.login_response", "loginResponse", (16 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.LoginResponse; });

		private var login_response$field:LWGF.content.protocol.LoginResponse;

		public function clearLoginResponse():void {
			login_response$field = null;
		}

		public function get hasLoginResponse():Boolean {
			return login_response$field != null;
		}

		public function set loginResponse(value:LWGF.content.protocol.LoginResponse):void {
			login_response$field = value;
		}

		public function get loginResponse():LWGF.content.protocol.LoginResponse {
			return login_response$field;
		}

		/**
		 *  @private
		 */
		public static const LOGIN_NOTIFY:FieldDescriptor$TYPE_MESSAGE = new FieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.LotillMsg.login_notify", "loginNotify", (17 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.LoginNotify; });

		private var login_notify$field:LWGF.content.protocol.LoginNotify;

		public function clearLoginNotify():void {
			login_notify$field = null;
		}

		public function get hasLoginNotify():Boolean {
			return login_notify$field != null;
		}

		public function set loginNotify(value:LWGF.content.protocol.LoginNotify):void {
			login_notify$field = value;
		}

		public function get loginNotify():LWGF.content.protocol.LoginNotify {
			return login_notify$field;
		}

		/**
		 *  @private
		 */
		public static const FETCH_ROLE_REQUEST:FieldDescriptor$TYPE_MESSAGE = new FieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.LotillMsg.fetch_role_request", "fetchRoleRequest", (18 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.FetchRoleRequest; });

		private var fetch_role_request$field:LWGF.content.protocol.FetchRoleRequest;

		public function clearFetchRoleRequest():void {
			fetch_role_request$field = null;
		}

		public function get hasFetchRoleRequest():Boolean {
			return fetch_role_request$field != null;
		}

		public function set fetchRoleRequest(value:LWGF.content.protocol.FetchRoleRequest):void {
			fetch_role_request$field = value;
		}

		public function get fetchRoleRequest():LWGF.content.protocol.FetchRoleRequest {
			return fetch_role_request$field;
		}

		/**
		 *  @private
		 */
		public static const FETCH_ROLE_RESPONSE:FieldDescriptor$TYPE_MESSAGE = new FieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.LotillMsg.fetch_role_response", "fetchRoleResponse", (19 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.FetchRoleResponse; });

		private var fetch_role_response$field:LWGF.content.protocol.FetchRoleResponse;

		public function clearFetchRoleResponse():void {
			fetch_role_response$field = null;
		}

		public function get hasFetchRoleResponse():Boolean {
			return fetch_role_response$field != null;
		}

		public function set fetchRoleResponse(value:LWGF.content.protocol.FetchRoleResponse):void {
			fetch_role_response$field = value;
		}

		public function get fetchRoleResponse():LWGF.content.protocol.FetchRoleResponse {
			return fetch_role_response$field;
		}

		/**
		 *  @private
		 */
		public static const UPDATE_ROLE_NOTIFY:FieldDescriptor$TYPE_MESSAGE = new FieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.LotillMsg.update_role_notify", "updateRoleNotify", (20 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.UpdateRoleNotify; });

		private var update_role_notify$field:LWGF.content.protocol.UpdateRoleNotify;

		public function clearUpdateRoleNotify():void {
			update_role_notify$field = null;
		}

		public function get hasUpdateRoleNotify():Boolean {
			return update_role_notify$field != null;
		}

		public function set updateRoleNotify(value:LWGF.content.protocol.UpdateRoleNotify):void {
			update_role_notify$field = value;
		}

		public function get updateRoleNotify():LWGF.content.protocol.UpdateRoleNotify {
			return update_role_notify$field;
		}

		/**
		 *  @private
		 */
		public static const KICK_ROLE_REQUEST:FieldDescriptor$TYPE_MESSAGE = new FieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.LotillMsg.kick_role_request", "kickRoleRequest", (21 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.KickRoleRequest; });

		private var kick_role_request$field:LWGF.content.protocol.KickRoleRequest;

		public function clearKickRoleRequest():void {
			kick_role_request$field = null;
		}

		public function get hasKickRoleRequest():Boolean {
			return kick_role_request$field != null;
		}

		public function set kickRoleRequest(value:LWGF.content.protocol.KickRoleRequest):void {
			kick_role_request$field = value;
		}

		public function get kickRoleRequest():LWGF.content.protocol.KickRoleRequest {
			return kick_role_request$field;
		}

		/**
		 *  @private
		 */
		public static const KICK_ROLE_RESPONSE:FieldDescriptor$TYPE_MESSAGE = new FieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.LotillMsg.kick_role_response", "kickRoleResponse", (22 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.KickRoleResponse; });

		private var kick_role_response$field:LWGF.content.protocol.KickRoleResponse;

		public function clearKickRoleResponse():void {
			kick_role_response$field = null;
		}

		public function get hasKickRoleResponse():Boolean {
			return kick_role_response$field != null;
		}

		public function set kickRoleResponse(value:LWGF.content.protocol.KickRoleResponse):void {
			kick_role_response$field = value;
		}

		public function get kickRoleResponse():LWGF.content.protocol.KickRoleResponse {
			return kick_role_response$field;
		}

		/**
		 *  @private
		 */
		public static const LOGOUT_REQUEST:FieldDescriptor$TYPE_MESSAGE = new FieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.LotillMsg.logout_request", "logoutRequest", (23 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.LogoutRequest; });

		private var logout_request$field:LWGF.content.protocol.LogoutRequest;

		public function clearLogoutRequest():void {
			logout_request$field = null;
		}

		public function get hasLogoutRequest():Boolean {
			return logout_request$field != null;
		}

		public function set logoutRequest(value:LWGF.content.protocol.LogoutRequest):void {
			logout_request$field = value;
		}

		public function get logoutRequest():LWGF.content.protocol.LogoutRequest {
			return logout_request$field;
		}

		/**
		 *  @private
		 */
		public static const LOGOUT_RESPONSE:FieldDescriptor$TYPE_MESSAGE = new FieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.LotillMsg.logout_response", "logoutResponse", (24 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.LogoutResponse; });

		private var logout_response$field:LWGF.content.protocol.LogoutResponse;

		public function clearLogoutResponse():void {
			logout_response$field = null;
		}

		public function get hasLogoutResponse():Boolean {
			return logout_response$field != null;
		}

		public function set logoutResponse(value:LWGF.content.protocol.LogoutResponse):void {
			logout_response$field = value;
		}

		public function get logoutResponse():LWGF.content.protocol.LogoutResponse {
			return logout_response$field;
		}

		/**
		 *  @private
		 */
		public static const LOGOUT_NOTIFY:FieldDescriptor$TYPE_MESSAGE = new FieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.LotillMsg.logout_notify", "logoutNotify", (25 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.LogoutNotify; });

		private var logout_notify$field:LWGF.content.protocol.LogoutNotify;

		public function clearLogoutNotify():void {
			logout_notify$field = null;
		}

		public function get hasLogoutNotify():Boolean {
			return logout_notify$field != null;
		}

		public function set logoutNotify(value:LWGF.content.protocol.LogoutNotify):void {
			logout_notify$field = value;
		}

		public function get logoutNotify():LWGF.content.protocol.LogoutNotify {
			return logout_notify$field;
		}

		/**
		 *  @private
		 */
		public static const FIND_NAME_REQUEST:FieldDescriptor$TYPE_MESSAGE = new FieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.LotillMsg.find_name_request", "findNameRequest", (26 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.FindNameRequest; });

		private var find_name_request$field:LWGF.content.protocol.FindNameRequest;

		public function clearFindNameRequest():void {
			find_name_request$field = null;
		}

		public function get hasFindNameRequest():Boolean {
			return find_name_request$field != null;
		}

		public function set findNameRequest(value:LWGF.content.protocol.FindNameRequest):void {
			find_name_request$field = value;
		}

		public function get findNameRequest():LWGF.content.protocol.FindNameRequest {
			return find_name_request$field;
		}

		/**
		 *  @private
		 */
		public static const FIND_NAME_RESPONSE:FieldDescriptor$TYPE_MESSAGE = new FieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.LotillMsg.find_name_response", "findNameResponse", (27 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.FindNameResponse; });

		private var find_name_response$field:LWGF.content.protocol.FindNameResponse;

		public function clearFindNameResponse():void {
			find_name_response$field = null;
		}

		public function get hasFindNameResponse():Boolean {
			return find_name_response$field != null;
		}

		public function set findNameResponse(value:LWGF.content.protocol.FindNameResponse):void {
			find_name_response$field = value;
		}

		public function get findNameResponse():LWGF.content.protocol.FindNameResponse {
			return find_name_response$field;
		}

		/**
		 *  @private
		 */
		public static const UPDATE_NAME_REQUEST:FieldDescriptor$TYPE_MESSAGE = new FieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.LotillMsg.update_name_request", "updateNameRequest", (28 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.UpdateNameRequest; });

		private var update_name_request$field:LWGF.content.protocol.UpdateNameRequest;

		public function clearUpdateNameRequest():void {
			update_name_request$field = null;
		}

		public function get hasUpdateNameRequest():Boolean {
			return update_name_request$field != null;
		}

		public function set updateNameRequest(value:LWGF.content.protocol.UpdateNameRequest):void {
			update_name_request$field = value;
		}

		public function get updateNameRequest():LWGF.content.protocol.UpdateNameRequest {
			return update_name_request$field;
		}

		/**
		 *  @private
		 */
		public static const UPDATE_NAME_RESPONSE:FieldDescriptor$TYPE_MESSAGE = new FieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.LotillMsg.update_name_response", "updateNameResponse", (29 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.UpdateNameResponse; });

		private var update_name_response$field:LWGF.content.protocol.UpdateNameResponse;

		public function clearUpdateNameResponse():void {
			update_name_response$field = null;
		}

		public function get hasUpdateNameResponse():Boolean {
			return update_name_response$field != null;
		}

		public function set updateNameResponse(value:LWGF.content.protocol.UpdateNameResponse):void {
			update_name_response$field = value;
		}

		public function get updateNameResponse():LWGF.content.protocol.UpdateNameResponse {
			return update_name_response$field;
		}

		/**
		 *  @private
		 */
		public static const LOGIN_SHIP_NOTIFY:FieldDescriptor$TYPE_MESSAGE = new FieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.LotillMsg.login_ship_notify", "loginShipNotify", (30 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.LoginShipNotify; });

		private var login_ship_notify$field:LWGF.content.protocol.LoginShipNotify;

		public function clearLoginShipNotify():void {
			login_ship_notify$field = null;
		}

		public function get hasLoginShipNotify():Boolean {
			return login_ship_notify$field != null;
		}

		public function set loginShipNotify(value:LWGF.content.protocol.LoginShipNotify):void {
			login_ship_notify$field = value;
		}

		public function get loginShipNotify():LWGF.content.protocol.LoginShipNotify {
			return login_ship_notify$field;
		}

		/**
		 *  @private
		 */
		override com.protobuf.used_by_generated_code final function writeToBuffer(output:com.protobuf.WritingBuffer):void {
			com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 1);
			com.protobuf.WriteUtils.write$TYPE_INT32(output, this.msgid);
			if (hasUin) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 2);
				com.protobuf.WriteUtils.write$TYPE_INT32(output, uin$field);
			}
			if (hasAccount) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 3);
				com.protobuf.WriteUtils.write$TYPE_STRING(output, account$field);
			}
			if (hasSessionKey) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 4);
				com.protobuf.WriteUtils.write$TYPE_STRING(output, session_key$field);
			}
			if (hasFeedBack) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 5);
				com.protobuf.WriteUtils.write$TYPE_INT32(output, feed_back$field);
			}
			if (hasTimeStamp) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 6);
				com.protobuf.WriteUtils.write$TYPE_INT64(output, time_stamp$field);
			}
			if (hasPlatform) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 7);
				com.protobuf.WriteUtils.write$TYPE_ENUM(output, platform$field);
			}
			if (hasClientDebugInfo) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 8);
				com.protobuf.WriteUtils.write$TYPE_STRING(output, client_debug_info$field);
			}
			if (hasListRoleRequest) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 11);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, list_role_request$field);
			}
			if (hasListRoleResponse) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 12);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, list_role_response$field);
			}
			if (hasCreateRoleRequest) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 13);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, create_role_request$field);
			}
			if (hasCreateRoleResponse) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 14);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, create_role_response$field);
			}
			if (hasLoginRequest) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 15);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, login_request$field);
			}
			if (hasLoginResponse) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 16);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, login_response$field);
			}
			if (hasLoginNotify) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 17);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, login_notify$field);
			}
			if (hasFetchRoleRequest) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 18);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, fetch_role_request$field);
			}
			if (hasFetchRoleResponse) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 19);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, fetch_role_response$field);
			}
			if (hasUpdateRoleNotify) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 20);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, update_role_notify$field);
			}
			if (hasKickRoleRequest) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 21);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, kick_role_request$field);
			}
			if (hasKickRoleResponse) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 22);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, kick_role_response$field);
			}
			if (hasLogoutRequest) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 23);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, logout_request$field);
			}
			if (hasLogoutResponse) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 24);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, logout_response$field);
			}
			if (hasLogoutNotify) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 25);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, logout_notify$field);
			}
			if (hasFindNameRequest) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 26);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, find_name_request$field);
			}
			if (hasFindNameResponse) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 27);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, find_name_response$field);
			}
			if (hasUpdateNameRequest) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 28);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, update_name_request$field);
			}
			if (hasUpdateNameResponse) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 29);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, update_name_response$field);
			}
			if (hasLoginShipNotify) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 30);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, login_ship_notify$field);
			}
			for (var fieldKey:* in this) {
				super.writeUnknown(output, fieldKey);
			}
		}

		/**
		 *  @private
		 */
		override com.protobuf.used_by_generated_code final function readFromSlice(input:flash.utils.IDataInput, bytesAfterSlice:uint):void {
			var msgid$count:uint = 0;
			var uin$count:uint = 0;
			var account$count:uint = 0;
			var session_key$count:uint = 0;
			var feed_back$count:uint = 0;
			var time_stamp$count:uint = 0;
			var platform$count:uint = 0;
			var client_debug_info$count:uint = 0;
			var list_role_request$count:uint = 0;
			var list_role_response$count:uint = 0;
			var create_role_request$count:uint = 0;
			var create_role_response$count:uint = 0;
			var login_request$count:uint = 0;
			var login_response$count:uint = 0;
			var login_notify$count:uint = 0;
			var fetch_role_request$count:uint = 0;
			var fetch_role_response$count:uint = 0;
			var update_role_notify$count:uint = 0;
			var kick_role_request$count:uint = 0;
			var kick_role_response$count:uint = 0;
			var logout_request$count:uint = 0;
			var logout_response$count:uint = 0;
			var logout_notify$count:uint = 0;
			var find_name_request$count:uint = 0;
			var find_name_response$count:uint = 0;
			var update_name_request$count:uint = 0;
			var update_name_response$count:uint = 0;
			var login_ship_notify$count:uint = 0;
			while (input.bytesAvailable > bytesAfterSlice) {
				var tag:uint = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
				switch (tag >> 3) {
				case 1:
					if (msgid$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.msgid cannot be set twice.');
					}
					++msgid$count;
					this.msgid = com.protobuf.ReadUtils.read$TYPE_INT32(input);
					break;
				case 2:
					if (uin$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.uin cannot be set twice.');
					}
					++uin$count;
					this.uin = com.protobuf.ReadUtils.read$TYPE_INT32(input);
					break;
				case 3:
					if (account$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.account cannot be set twice.');
					}
					++account$count;
					this.account = com.protobuf.ReadUtils.read$TYPE_STRING(input);
					break;
				case 4:
					if (session_key$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.sessionKey cannot be set twice.');
					}
					++session_key$count;
					this.sessionKey = com.protobuf.ReadUtils.read$TYPE_STRING(input);
					break;
				case 5:
					if (feed_back$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.feedBack cannot be set twice.');
					}
					++feed_back$count;
					this.feedBack = com.protobuf.ReadUtils.read$TYPE_INT32(input);
					break;
				case 6:
					if (time_stamp$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.timeStamp cannot be set twice.');
					}
					++time_stamp$count;
					this.timeStamp = com.protobuf.ReadUtils.read$TYPE_INT64(input);
					break;
				case 7:
					if (platform$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.platform cannot be set twice.');
					}
					++platform$count;
					this.platform = com.protobuf.ReadUtils.read$TYPE_ENUM(input);
					break;
				case 8:
					if (client_debug_info$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.clientDebugInfo cannot be set twice.');
					}
					++client_debug_info$count;
					this.clientDebugInfo = com.protobuf.ReadUtils.read$TYPE_STRING(input);
					break;
				case 11:
					if (list_role_request$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.listRoleRequest cannot be set twice.');
					}
					++list_role_request$count;
					this.listRoleRequest = new LWGF.content.protocol.ListRoleRequest();
					com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, this.listRoleRequest);
					break;
				case 12:
					if (list_role_response$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.listRoleResponse cannot be set twice.');
					}
					++list_role_response$count;
					this.listRoleResponse = new LWGF.content.protocol.ListRoleResponse();
					com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, this.listRoleResponse);
					break;
				case 13:
					if (create_role_request$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.createRoleRequest cannot be set twice.');
					}
					++create_role_request$count;
					this.createRoleRequest = new LWGF.content.protocol.CreateRoleRequest();
					com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, this.createRoleRequest);
					break;
				case 14:
					if (create_role_response$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.createRoleResponse cannot be set twice.');
					}
					++create_role_response$count;
					this.createRoleResponse = new LWGF.content.protocol.CreateRoleResponse();
					com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, this.createRoleResponse);
					break;
				case 15:
					if (login_request$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.loginRequest cannot be set twice.');
					}
					++login_request$count;
					this.loginRequest = new LWGF.content.protocol.LoginRequest();
					com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, this.loginRequest);
					break;
				case 16:
					if (login_response$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.loginResponse cannot be set twice.');
					}
					++login_response$count;
					this.loginResponse = new LWGF.content.protocol.LoginResponse();
					com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, this.loginResponse);
					break;
				case 17:
					if (login_notify$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.loginNotify cannot be set twice.');
					}
					++login_notify$count;
					this.loginNotify = new LWGF.content.protocol.LoginNotify();
					com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, this.loginNotify);
					break;
				case 18:
					if (fetch_role_request$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.fetchRoleRequest cannot be set twice.');
					}
					++fetch_role_request$count;
					this.fetchRoleRequest = new LWGF.content.protocol.FetchRoleRequest();
					com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, this.fetchRoleRequest);
					break;
				case 19:
					if (fetch_role_response$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.fetchRoleResponse cannot be set twice.');
					}
					++fetch_role_response$count;
					this.fetchRoleResponse = new LWGF.content.protocol.FetchRoleResponse();
					com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, this.fetchRoleResponse);
					break;
				case 20:
					if (update_role_notify$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.updateRoleNotify cannot be set twice.');
					}
					++update_role_notify$count;
					this.updateRoleNotify = new LWGF.content.protocol.UpdateRoleNotify();
					com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, this.updateRoleNotify);
					break;
				case 21:
					if (kick_role_request$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.kickRoleRequest cannot be set twice.');
					}
					++kick_role_request$count;
					this.kickRoleRequest = new LWGF.content.protocol.KickRoleRequest();
					com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, this.kickRoleRequest);
					break;
				case 22:
					if (kick_role_response$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.kickRoleResponse cannot be set twice.');
					}
					++kick_role_response$count;
					this.kickRoleResponse = new LWGF.content.protocol.KickRoleResponse();
					com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, this.kickRoleResponse);
					break;
				case 23:
					if (logout_request$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.logoutRequest cannot be set twice.');
					}
					++logout_request$count;
					this.logoutRequest = new LWGF.content.protocol.LogoutRequest();
					com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, this.logoutRequest);
					break;
				case 24:
					if (logout_response$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.logoutResponse cannot be set twice.');
					}
					++logout_response$count;
					this.logoutResponse = new LWGF.content.protocol.LogoutResponse();
					com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, this.logoutResponse);
					break;
				case 25:
					if (logout_notify$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.logoutNotify cannot be set twice.');
					}
					++logout_notify$count;
					this.logoutNotify = new LWGF.content.protocol.LogoutNotify();
					com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, this.logoutNotify);
					break;
				case 26:
					if (find_name_request$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.findNameRequest cannot be set twice.');
					}
					++find_name_request$count;
					this.findNameRequest = new LWGF.content.protocol.FindNameRequest();
					com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, this.findNameRequest);
					break;
				case 27:
					if (find_name_response$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.findNameResponse cannot be set twice.');
					}
					++find_name_response$count;
					this.findNameResponse = new LWGF.content.protocol.FindNameResponse();
					com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, this.findNameResponse);
					break;
				case 28:
					if (update_name_request$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.updateNameRequest cannot be set twice.');
					}
					++update_name_request$count;
					this.updateNameRequest = new LWGF.content.protocol.UpdateNameRequest();
					com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, this.updateNameRequest);
					break;
				case 29:
					if (update_name_response$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.updateNameResponse cannot be set twice.');
					}
					++update_name_response$count;
					this.updateNameResponse = new LWGF.content.protocol.UpdateNameResponse();
					com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, this.updateNameResponse);
					break;
				case 30:
					if (login_ship_notify$count != 0) {
						throw new flash.errors.IOError('Bad data format: LotillMsg.loginShipNotify cannot be set twice.');
					}
					++login_ship_notify$count;
					this.loginShipNotify = new LWGF.content.protocol.LoginShipNotify();
					com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, this.loginShipNotify);
					break;
				default:
					super.readUnknown(input, tag);
					break;
				}
			}
		}

	}
}
