package LWGF.content.protocol {
	import com.protobuf.*;
	use namespace com.protobuf.used_by_generated_code;
	import com.protobuf.fieldDescriptors.*;
	import flash.utils.Endian;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;
	import flash.errors.IOError;
	// @@protoc_insertion_point(imports)

	// @@protoc_insertion_point(class_metadata)
	public dynamic final class RoleInfo extends com.protobuf.Message {
		/**
		 *  @private
		 */
		public static const UIN:FieldDescriptor$TYPE_UINT32 = new FieldDescriptor$TYPE_UINT32("lotill.t3.protocol.RoleInfo.uin", "uin", (1 << 3) | com.protobuf.WireType.VARINT);

		private var uin$field:uint;

		private var hasField$0:uint = 0;

		public function clearUin():void {
			hasField$0 &= 0xfffffffe;
			uin$field = new uint();
		}

		public function get hasUin():Boolean {
			return (hasField$0 & 0x1) != 0;
		}

		public function set uin(value:uint):void {
			hasField$0 |= 0x1;
			uin$field = value;
		}

		public function get uin():uint {
			return uin$field;
		}

		/**
		 *  @private
		 */
		public static const NAME:FieldDescriptor$TYPE_STRING = new FieldDescriptor$TYPE_STRING("lotill.t3.protocol.RoleInfo.name", "name", (2 << 3) | com.protobuf.WireType.LENGTH_DELIMITED);

		private var name$field:String;

		public function clearName():void {
			name$field = null;
		}

		public function get hasName():Boolean {
			return name$field != null;
		}

		public function set name(value:String):void {
			name$field = value;
		}

		public function get name():String {
			return name$field;
		}

		/**
		 *  @private
		 */
		public static const GENDER:FieldDescriptor$TYPE_INT32 = new FieldDescriptor$TYPE_INT32("lotill.t3.protocol.RoleInfo.gender", "gender", (3 << 3) | com.protobuf.WireType.VARINT);

		private var gender$field:int;

		public function clearGender():void {
			hasField$0 &= 0xfffffffd;
			gender$field = new int();
		}

		public function get hasGender():Boolean {
			return (hasField$0 & 0x2) != 0;
		}

		public function set gender(value:int):void {
			hasField$0 |= 0x2;
			gender$field = value;
		}

		public function get gender():int {
			return gender$field;
		}

		/**
		 *  @private
		 */
		public static const EXP:FieldDescriptor$TYPE_INT32 = new FieldDescriptor$TYPE_INT32("lotill.t3.protocol.RoleInfo.exp", "exp", (4 << 3) | com.protobuf.WireType.VARINT);

		private var exp$field:int;

		public function clearExp():void {
			hasField$0 &= 0xfffffffb;
			exp$field = new int();
		}

		public function get hasExp():Boolean {
			return (hasField$0 & 0x4) != 0;
		}

		public function set exp(value:int):void {
			hasField$0 |= 0x4;
			exp$field = value;
		}

		public function get exp():int {
			return exp$field;
		}

		/**
		 *  @private
		 */
		public static const LEVEL:FieldDescriptor$TYPE_INT32 = new FieldDescriptor$TYPE_INT32("lotill.t3.protocol.RoleInfo.level", "level", (5 << 3) | com.protobuf.WireType.VARINT);

		private var level$field:int;

		public function clearLevel():void {
			hasField$0 &= 0xfffffff7;
			level$field = new int();
		}

		public function get hasLevel():Boolean {
			return (hasField$0 & 0x8) != 0;
		}

		public function set level(value:int):void {
			hasField$0 |= 0x8;
			level$field = value;
		}

		public function get level():int {
			return level$field;
		}

		/**
		 *  @private
		 */
		public static const AVATAR:FieldDescriptor$TYPE_INT32 = new FieldDescriptor$TYPE_INT32("lotill.t3.protocol.RoleInfo.avatar", "avatar", (6 << 3) | com.protobuf.WireType.VARINT);

		private var avatar$field:int;

		public function clearAvatar():void {
			hasField$0 &= 0xffffffef;
			avatar$field = new int();
		}

		public function get hasAvatar():Boolean {
			return (hasField$0 & 0x10) != 0;
		}

		public function set avatar(value:int):void {
			hasField$0 |= 0x10;
			avatar$field = value;
		}

		public function get avatar():int {
			return avatar$field;
		}

		/**
		 *  @private
		 */
		public static const CREATE_TIME:FieldDescriptor$TYPE_UINT32 = new FieldDescriptor$TYPE_UINT32("lotill.t3.protocol.RoleInfo.create_time", "createTime", (7 << 3) | com.protobuf.WireType.VARINT);

		private var create_time$field:uint;

		public function clearCreateTime():void {
			hasField$0 &= 0xffffffdf;
			create_time$field = new uint();
		}

		public function get hasCreateTime():Boolean {
			return (hasField$0 & 0x20) != 0;
		}

		public function set createTime(value:uint):void {
			hasField$0 |= 0x20;
			create_time$field = value;
		}

		public function get createTime():uint {
			return create_time$field;
		}

		/**
		 *  @private
		 */
		public static const LAST_LOGIN_TIME:FieldDescriptor$TYPE_UINT32 = new FieldDescriptor$TYPE_UINT32("lotill.t3.protocol.RoleInfo.last_login_time", "lastLoginTime", (8 << 3) | com.protobuf.WireType.VARINT);

		private var last_login_time$field:uint;

		public function clearLastLoginTime():void {
			hasField$0 &= 0xffffffbf;
			last_login_time$field = new uint();
		}

		public function get hasLastLoginTime():Boolean {
			return (hasField$0 & 0x40) != 0;
		}

		public function set lastLoginTime(value:uint):void {
			hasField$0 |= 0x40;
			last_login_time$field = value;
		}

		public function get lastLoginTime():uint {
			return last_login_time$field;
		}

		/**
		 *  @private
		 */
		public static const LAST_LOGOUT_TIME:FieldDescriptor$TYPE_UINT32 = new FieldDescriptor$TYPE_UINT32("lotill.t3.protocol.RoleInfo.last_Logout_time", "last_LogoutTime", (9 << 3) | com.protobuf.WireType.VARINT);

		private var last_Logout_time$field:uint;

		public function clearLast_LogoutTime():void {
			hasField$0 &= 0xffffff7f;
			last_Logout_time$field = new uint();
		}

		public function get hasLast_LogoutTime():Boolean {
			return (hasField$0 & 0x80) != 0;
		}

		public function set last_LogoutTime(value:uint):void {
			hasField$0 |= 0x80;
			last_Logout_time$field = value;
		}

		public function get last_LogoutTime():uint {
			return last_Logout_time$field;
		}

		/**
		 *  @private
		 */
		public static const SHIP_EXP:FieldDescriptor$TYPE_UINT32 = new FieldDescriptor$TYPE_UINT32("lotill.t3.protocol.RoleInfo.ship_exp", "shipExp", (10 << 3) | com.protobuf.WireType.VARINT);

		private var ship_exp$field:uint;

		public function clearShipExp():void {
			hasField$0 &= 0xfffffeff;
			ship_exp$field = new uint();
		}

		public function get hasShipExp():Boolean {
			return (hasField$0 & 0x100) != 0;
		}

		public function set shipExp(value:uint):void {
			hasField$0 |= 0x100;
			ship_exp$field = value;
		}

		public function get shipExp():uint {
			return ship_exp$field;
		}

		/**
		 *  @private
		 */
		override com.protobuf.used_by_generated_code final function writeToBuffer(output:com.protobuf.WritingBuffer):void {
			if (hasUin) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 1);
				com.protobuf.WriteUtils.write$TYPE_UINT32(output, uin$field);
			}
			if (hasName) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 2);
				com.protobuf.WriteUtils.write$TYPE_STRING(output, name$field);
			}
			if (hasGender) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 3);
				com.protobuf.WriteUtils.write$TYPE_INT32(output, gender$field);
			}
			if (hasExp) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 4);
				com.protobuf.WriteUtils.write$TYPE_INT32(output, exp$field);
			}
			if (hasLevel) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 5);
				com.protobuf.WriteUtils.write$TYPE_INT32(output, level$field);
			}
			if (hasAvatar) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 6);
				com.protobuf.WriteUtils.write$TYPE_INT32(output, avatar$field);
			}
			if (hasCreateTime) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 7);
				com.protobuf.WriteUtils.write$TYPE_UINT32(output, create_time$field);
			}
			if (hasLastLoginTime) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 8);
				com.protobuf.WriteUtils.write$TYPE_UINT32(output, last_login_time$field);
			}
			if (hasLast_LogoutTime) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 9);
				com.protobuf.WriteUtils.write$TYPE_UINT32(output, last_Logout_time$field);
			}
			if (hasShipExp) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 10);
				com.protobuf.WriteUtils.write$TYPE_UINT32(output, ship_exp$field);
			}
			for (var fieldKey:* in this) {
				super.writeUnknown(output, fieldKey);
			}
		}

		/**
		 *  @private
		 */
		override com.protobuf.used_by_generated_code final function readFromSlice(input:flash.utils.IDataInput, bytesAfterSlice:uint):void {
			var uin$count:uint = 0;
			var name$count:uint = 0;
			var gender$count:uint = 0;
			var exp$count:uint = 0;
			var level$count:uint = 0;
			var avatar$count:uint = 0;
			var create_time$count:uint = 0;
			var last_login_time$count:uint = 0;
			var last_Logout_time$count:uint = 0;
			var ship_exp$count:uint = 0;
			while (input.bytesAvailable > bytesAfterSlice) {
				var tag:uint = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
				switch (tag >> 3) {
				case 1:
					if (uin$count != 0) {
						throw new flash.errors.IOError('Bad data format: RoleInfo.uin cannot be set twice.');
					}
					++uin$count;
					this.uin = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
					break;
				case 2:
					if (name$count != 0) {
						throw new flash.errors.IOError('Bad data format: RoleInfo.name cannot be set twice.');
					}
					++name$count;
					this.name = com.protobuf.ReadUtils.read$TYPE_STRING(input);
					break;
				case 3:
					if (gender$count != 0) {
						throw new flash.errors.IOError('Bad data format: RoleInfo.gender cannot be set twice.');
					}
					++gender$count;
					this.gender = com.protobuf.ReadUtils.read$TYPE_INT32(input);
					break;
				case 4:
					if (exp$count != 0) {
						throw new flash.errors.IOError('Bad data format: RoleInfo.exp cannot be set twice.');
					}
					++exp$count;
					this.exp = com.protobuf.ReadUtils.read$TYPE_INT32(input);
					break;
				case 5:
					if (level$count != 0) {
						throw new flash.errors.IOError('Bad data format: RoleInfo.level cannot be set twice.');
					}
					++level$count;
					this.level = com.protobuf.ReadUtils.read$TYPE_INT32(input);
					break;
				case 6:
					if (avatar$count != 0) {
						throw new flash.errors.IOError('Bad data format: RoleInfo.avatar cannot be set twice.');
					}
					++avatar$count;
					this.avatar = com.protobuf.ReadUtils.read$TYPE_INT32(input);
					break;
				case 7:
					if (create_time$count != 0) {
						throw new flash.errors.IOError('Bad data format: RoleInfo.createTime cannot be set twice.');
					}
					++create_time$count;
					this.createTime = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
					break;
				case 8:
					if (last_login_time$count != 0) {
						throw new flash.errors.IOError('Bad data format: RoleInfo.lastLoginTime cannot be set twice.');
					}
					++last_login_time$count;
					this.lastLoginTime = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
					break;
				case 9:
					if (last_Logout_time$count != 0) {
						throw new flash.errors.IOError('Bad data format: RoleInfo.last_LogoutTime cannot be set twice.');
					}
					++last_Logout_time$count;
					this.last_LogoutTime = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
					break;
				case 10:
					if (ship_exp$count != 0) {
						throw new flash.errors.IOError('Bad data format: RoleInfo.shipExp cannot be set twice.');
					}
					++ship_exp$count;
					this.shipExp = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
					break;
				default:
					super.readUnknown(input, tag);
					break;
				}
			}
		}

	}
}
