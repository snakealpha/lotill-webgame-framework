package LWGF.content.protocol {
	import com.protobuf.*;
	use namespace com.protobuf.used_by_generated_code;
	import com.protobuf.fieldDescriptors.*;
	import flash.utils.Endian;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;
	import flash.errors.IOError;
	import LWGF.content.protocol.FetchRoleResean;
	import LWGF.content.protocol.DBRoleInfo;
	// @@protoc_insertion_point(imports)

	// @@protoc_insertion_point(class_metadata)
	public dynamic final class FetchRoleResponse extends com.protobuf.Message {
		/**
		 *  @private
		 */
		public static const RESULT:FieldDescriptor$TYPE_INT32 = new FieldDescriptor$TYPE_INT32("lotill.t3.protocol.FetchRoleResponse.result", "result", (1 << 3) | com.protobuf.WireType.VARINT);

		private var result$field:int;

		private var hasField$0:uint = 0;

		public function clearResult():void {
			hasField$0 &= 0xfffffffe;
			result$field = new int();
		}

		public function get hasResult():Boolean {
			return (hasField$0 & 0x1) != 0;
		}

		public function set result(value:int):void {
			hasField$0 |= 0x1;
			result$field = value;
		}

		public function get result():int {
			return result$field;
		}

		/**
		 *  @private
		 */
		public static const WORLD:FieldDescriptor$TYPE_INT32 = new FieldDescriptor$TYPE_INT32("lotill.t3.protocol.FetchRoleResponse.world", "world", (2 << 3) | com.protobuf.WireType.VARINT);

		private var world$field:int;

		public function clearWorld():void {
			hasField$0 &= 0xfffffffd;
			world$field = new int();
		}

		public function get hasWorld():Boolean {
			return (hasField$0 & 0x2) != 0;
		}

		public function set world(value:int):void {
			hasField$0 |= 0x2;
			world$field = value;
		}

		public function get world():int {
			return world$field;
		}

		/**
		 *  @private
		 */
		public static const REASON:FieldDescriptor$TYPE_ENUM = new FieldDescriptor$TYPE_ENUM("lotill.t3.protocol.FetchRoleResponse.reason", "reason", (3 << 3) | com.protobuf.WireType.VARINT, LWGF.content.protocol.FetchRoleResean);

		private var reason$field:int;

		public function clearReason():void {
			hasField$0 &= 0xfffffffb;
			reason$field = new int();
		}

		public function get hasReason():Boolean {
			return (hasField$0 & 0x4) != 0;
		}

		public function set reason(value:int):void {
			hasField$0 |= 0x4;
			reason$field = value;
		}

		public function get reason():int {
			return reason$field;
		}

		/**
		 *  @private
		 */
		public static const ROLEINFO:FieldDescriptor$TYPE_MESSAGE = new FieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.FetchRoleResponse.roleinfo", "roleinfo", (4 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.DBRoleInfo; });

		private var roleinfo$field:LWGF.content.protocol.DBRoleInfo;

		public function clearRoleinfo():void {
			roleinfo$field = null;
		}

		public function get hasRoleinfo():Boolean {
			return roleinfo$field != null;
		}

		public function set roleinfo(value:LWGF.content.protocol.DBRoleInfo):void {
			roleinfo$field = value;
		}

		public function get roleinfo():LWGF.content.protocol.DBRoleInfo {
			return roleinfo$field;
		}

		/**
		 *  @private
		 */
		override com.protobuf.used_by_generated_code final function writeToBuffer(output:com.protobuf.WritingBuffer):void {
			if (hasResult) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 1);
				com.protobuf.WriteUtils.write$TYPE_INT32(output, result$field);
			}
			if (hasWorld) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 2);
				com.protobuf.WriteUtils.write$TYPE_INT32(output, world$field);
			}
			if (hasReason) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 3);
				com.protobuf.WriteUtils.write$TYPE_ENUM(output, reason$field);
			}
			if (hasRoleinfo) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 4);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, roleinfo$field);
			}
			for (var fieldKey:* in this) {
				super.writeUnknown(output, fieldKey);
			}
		}

		/**
		 *  @private
		 */
		override com.protobuf.used_by_generated_code final function readFromSlice(input:flash.utils.IDataInput, bytesAfterSlice:uint):void {
			var result$count:uint = 0;
			var world$count:uint = 0;
			var reason$count:uint = 0;
			var roleinfo$count:uint = 0;
			while (input.bytesAvailable > bytesAfterSlice) {
				var tag:uint = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
				switch (tag >> 3) {
				case 1:
					if (result$count != 0) {
						throw new flash.errors.IOError('Bad data format: FetchRoleResponse.result cannot be set twice.');
					}
					++result$count;
					this.result = com.protobuf.ReadUtils.read$TYPE_INT32(input);
					break;
				case 2:
					if (world$count != 0) {
						throw new flash.errors.IOError('Bad data format: FetchRoleResponse.world cannot be set twice.');
					}
					++world$count;
					this.world = com.protobuf.ReadUtils.read$TYPE_INT32(input);
					break;
				case 3:
					if (reason$count != 0) {
						throw new flash.errors.IOError('Bad data format: FetchRoleResponse.reason cannot be set twice.');
					}
					++reason$count;
					this.reason = com.protobuf.ReadUtils.read$TYPE_ENUM(input);
					break;
				case 4:
					if (roleinfo$count != 0) {
						throw new flash.errors.IOError('Bad data format: FetchRoleResponse.roleinfo cannot be set twice.');
					}
					++roleinfo$count;
					this.roleinfo = new LWGF.content.protocol.DBRoleInfo();
					com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, this.roleinfo);
					break;
				default:
					super.readUnknown(input, tag);
					break;
				}
			}
		}

	}
}
