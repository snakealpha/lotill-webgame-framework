package LWGF.content.protocol {
	import com.protobuf.*;
	use namespace com.protobuf.used_by_generated_code;
	import com.protobuf.fieldDescriptors.*;
	import flash.utils.Endian;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;
	import flash.errors.IOError;
	import LWGF.content.protocol.RoleSummary;
	// @@protoc_insertion_point(imports)

	// @@protoc_insertion_point(class_metadata)
	public dynamic final class ListRoleResponse extends com.protobuf.Message {
		/**
		 *  @private
		 */
		public static const RESULT:FieldDescriptor$TYPE_INT32 = new FieldDescriptor$TYPE_INT32("lotill.t3.protocol.ListRoleResponse.result", "result", (1 << 3) | com.protobuf.WireType.VARINT);

		private var result$field:int;

		private var hasField$0:uint = 0;

		public function clearResult():void {
			hasField$0 &= 0xfffffffe;
			result$field = new int();
		}

		public function get hasResult():Boolean {
			return (hasField$0 & 0x1) != 0;
		}

		public function set result(value:int):void {
			hasField$0 |= 0x1;
			result$field = value;
		}

		public function get result():int {
			return result$field;
		}

		/**
		 *  @private
		 */
		public static const WORLD:FieldDescriptor$TYPE_INT32 = new FieldDescriptor$TYPE_INT32("lotill.t3.protocol.ListRoleResponse.world", "world", (2 << 3) | com.protobuf.WireType.VARINT);

		private var world$field:int;

		public function clearWorld():void {
			hasField$0 &= 0xfffffffd;
			world$field = new int();
		}

		public function get hasWorld():Boolean {
			return (hasField$0 & 0x2) != 0;
		}

		public function set world(value:int):void {
			hasField$0 |= 0x2;
			world$field = value;
		}

		public function get world():int {
			return world$field;
		}

		/**
		 *  @private
		 */
		public static const ROLE_SUMMARY:RepeatedFieldDescriptor$TYPE_MESSAGE = new RepeatedFieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.ListRoleResponse.role_summary", "roleSummary", (3 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.RoleSummary; });

		[ArrayElementType("LWGF.content.protocol.RoleSummary")]
		public var roleSummary:Array = [];

		/**
		 *  @private
		 */
		override com.protobuf.used_by_generated_code final function writeToBuffer(output:com.protobuf.WritingBuffer):void {
			if (hasResult) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 1);
				com.protobuf.WriteUtils.write$TYPE_INT32(output, result$field);
			}
			if (hasWorld) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 2);
				com.protobuf.WriteUtils.write$TYPE_INT32(output, world$field);
			}
			for (var roleSummary$index:uint = 0; roleSummary$index < this.roleSummary.length; ++roleSummary$index) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 3);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, this.roleSummary[roleSummary$index]);
			}
			for (var fieldKey:* in this) {
				super.writeUnknown(output, fieldKey);
			}
		}

		/**
		 *  @private
		 */
		override com.protobuf.used_by_generated_code final function readFromSlice(input:flash.utils.IDataInput, bytesAfterSlice:uint):void {
			var result$count:uint = 0;
			var world$count:uint = 0;
			while (input.bytesAvailable > bytesAfterSlice) {
				var tag:uint = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
				switch (tag >> 3) {
				case 1:
					if (result$count != 0) {
						throw new flash.errors.IOError('Bad data format: ListRoleResponse.result cannot be set twice.');
					}
					++result$count;
					this.result = com.protobuf.ReadUtils.read$TYPE_INT32(input);
					break;
				case 2:
					if (world$count != 0) {
						throw new flash.errors.IOError('Bad data format: ListRoleResponse.world cannot be set twice.');
					}
					++world$count;
					this.world = com.protobuf.ReadUtils.read$TYPE_INT32(input);
					break;
				case 3:
					this.roleSummary.push(com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, new LWGF.content.protocol.RoleSummary()));
					break;
				default:
					super.readUnknown(input, tag);
					break;
				}
			}
		}

	}
}
