package LWGF.content.protocol {
	public final class Macros {
		public static const MAX_NAME_LEN:int = 64;
		public static const MAX_ACCOUNT_LEN:int = 32;
	}
}
