package LWGF.content.protocol {
	import com.protobuf.*;
	use namespace com.protobuf.used_by_generated_code;
	import com.protobuf.fieldDescriptors.*;
	import flash.utils.Endian;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;
	import flash.errors.IOError;
	import LWGF.content.protocol.Capitain;
	import LWGF.content.protocol.Ship;
	// @@protoc_insertion_point(imports)

	// @@protoc_insertion_point(class_metadata)
	public dynamic final class LoginShipNotify extends com.protobuf.Message {
		/**
		 *  @private
		 */
		public static const CAPITAIN_LIST:RepeatedFieldDescriptor$TYPE_MESSAGE = new RepeatedFieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.LoginShipNotify.capitain_list", "capitainList", (1 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.Capitain; });

		[ArrayElementType("LWGF.content.protocol.Capitain")]
		public var capitainList:Array = [];

		/**
		 *  @private
		 */
		public static const SHIP_LIST:RepeatedFieldDescriptor$TYPE_MESSAGE = new RepeatedFieldDescriptor$TYPE_MESSAGE("lotill.t3.protocol.LoginShipNotify.ship_list", "shipList", (2 << 3) | com.protobuf.WireType.LENGTH_DELIMITED, function():Class { return LWGF.content.protocol.Ship; });

		[ArrayElementType("LWGF.content.protocol.Ship")]
		public var shipList:Array = [];

		/**
		 *  @private
		 */
		public static const SHIP_TEAM_LIST1:RepeatedFieldDescriptor$TYPE_UINT32 = new RepeatedFieldDescriptor$TYPE_UINT32("lotill.t3.protocol.LoginShipNotify.ship_team_list1", "shipTeamList1", (3 << 3) | com.protobuf.WireType.VARINT);

		[ArrayElementType("uint")]
		public var shipTeamList1:Array = [];

		/**
		 *  @private
		 */
		public static const SHIP_TEAM_LIST2:RepeatedFieldDescriptor$TYPE_UINT32 = new RepeatedFieldDescriptor$TYPE_UINT32("lotill.t3.protocol.LoginShipNotify.ship_team_list2", "shipTeamList2", (4 << 3) | com.protobuf.WireType.VARINT);

		[ArrayElementType("uint")]
		public var shipTeamList2:Array = [];

		/**
		 *  @private
		 */
		public static const SHIP_TEAM_LIST3:RepeatedFieldDescriptor$TYPE_UINT32 = new RepeatedFieldDescriptor$TYPE_UINT32("lotill.t3.protocol.LoginShipNotify.ship_team_list3", "shipTeamList3", (5 << 3) | com.protobuf.WireType.VARINT);

		[ArrayElementType("uint")]
		public var shipTeamList3:Array = [];

		/**
		 *  @private
		 */
		override com.protobuf.used_by_generated_code final function writeToBuffer(output:com.protobuf.WritingBuffer):void {
			for (var capitainList$index:uint = 0; capitainList$index < this.capitainList.length; ++capitainList$index) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 1);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, this.capitainList[capitainList$index]);
			}
			for (var shipList$index:uint = 0; shipList$index < this.shipList.length; ++shipList$index) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.LENGTH_DELIMITED, 2);
				com.protobuf.WriteUtils.write$TYPE_MESSAGE(output, this.shipList[shipList$index]);
			}
			for (var shipTeamList1$index:uint = 0; shipTeamList1$index < this.shipTeamList1.length; ++shipTeamList1$index) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 3);
				com.protobuf.WriteUtils.write$TYPE_UINT32(output, this.shipTeamList1[shipTeamList1$index]);
			}
			for (var shipTeamList2$index:uint = 0; shipTeamList2$index < this.shipTeamList2.length; ++shipTeamList2$index) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 4);
				com.protobuf.WriteUtils.write$TYPE_UINT32(output, this.shipTeamList2[shipTeamList2$index]);
			}
			for (var shipTeamList3$index:uint = 0; shipTeamList3$index < this.shipTeamList3.length; ++shipTeamList3$index) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 5);
				com.protobuf.WriteUtils.write$TYPE_UINT32(output, this.shipTeamList3[shipTeamList3$index]);
			}
			for (var fieldKey:* in this) {
				super.writeUnknown(output, fieldKey);
			}
		}

		/**
		 *  @private
		 */
		override com.protobuf.used_by_generated_code final function readFromSlice(input:flash.utils.IDataInput, bytesAfterSlice:uint):void {
			while (input.bytesAvailable > bytesAfterSlice) {
				var tag:uint = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
				switch (tag >> 3) {
				case 1:
					this.capitainList.push(com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, new LWGF.content.protocol.Capitain()));
					break;
				case 2:
					this.shipList.push(com.protobuf.ReadUtils.read$TYPE_MESSAGE(input, new LWGF.content.protocol.Ship()));
					break;
				case 3:
					if ((tag & 7) == com.protobuf.WireType.LENGTH_DELIMITED) {
						com.protobuf.ReadUtils.readPackedRepeated(input, com.protobuf.ReadUtils.read$TYPE_UINT32, this.shipTeamList1);
						break;
					}
					this.shipTeamList1.push(com.protobuf.ReadUtils.read$TYPE_UINT32(input));
					break;
				case 4:
					if ((tag & 7) == com.protobuf.WireType.LENGTH_DELIMITED) {
						com.protobuf.ReadUtils.readPackedRepeated(input, com.protobuf.ReadUtils.read$TYPE_UINT32, this.shipTeamList2);
						break;
					}
					this.shipTeamList2.push(com.protobuf.ReadUtils.read$TYPE_UINT32(input));
					break;
				case 5:
					if ((tag & 7) == com.protobuf.WireType.LENGTH_DELIMITED) {
						com.protobuf.ReadUtils.readPackedRepeated(input, com.protobuf.ReadUtils.read$TYPE_UINT32, this.shipTeamList3);
						break;
					}
					this.shipTeamList3.push(com.protobuf.ReadUtils.read$TYPE_UINT32(input));
					break;
				default:
					super.readUnknown(input, tag);
					break;
				}
			}
		}

	}
}
