package LWGF.content.protocol {
	public final class UpdateNameTtype {
		public static const NAMEUPDATETYPE_ADD:int = 1;
		public static const NAMEUPDATETYPE_REMOVE:int = 2;
	}
}
