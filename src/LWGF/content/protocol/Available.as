package LWGF.content.protocol {
	public final class Available {
		public static const AVAILABLE_NORMAL:int = 1;
		public static const AVAILABLE_BAN:int = 2;
		public static const AVAILABLE_CLOSE:int = 3;
		public static const AVAILABLE_DELETE:int = 4;
	}
}
