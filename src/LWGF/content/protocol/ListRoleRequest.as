package LWGF.content.protocol {
	import com.protobuf.*;
	use namespace com.protobuf.used_by_generated_code;
	import com.protobuf.fieldDescriptors.*;
	import flash.utils.Endian;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;
	import flash.errors.IOError;
	// @@protoc_insertion_point(imports)

	// @@protoc_insertion_point(class_metadata)
	public dynamic final class ListRoleRequest extends com.protobuf.Message {
		/**
		 *  @private
		 */
		public static const WORLD:FieldDescriptor$TYPE_INT32 = new FieldDescriptor$TYPE_INT32("lotill.t3.protocol.ListRoleRequest.world", "world", (3 << 3) | com.protobuf.WireType.VARINT);

		private var world$field:int;

		private var hasField$0:uint = 0;

		public function clearWorld():void {
			hasField$0 &= 0xfffffffe;
			world$field = new int();
		}

		public function get hasWorld():Boolean {
			return (hasField$0 & 0x1) != 0;
		}

		public function set world(value:int):void {
			hasField$0 |= 0x1;
			world$field = value;
		}

		public function get world():int {
			return world$field;
		}

		/**
		 *  @private
		 */
		override com.protobuf.used_by_generated_code final function writeToBuffer(output:com.protobuf.WritingBuffer):void {
			if (hasWorld) {
				com.protobuf.WriteUtils.writeTag(output, com.protobuf.WireType.VARINT, 3);
				com.protobuf.WriteUtils.write$TYPE_INT32(output, world$field);
			}
			for (var fieldKey:* in this) {
				super.writeUnknown(output, fieldKey);
			}
		}

		/**
		 *  @private
		 */
		override com.protobuf.used_by_generated_code final function readFromSlice(input:flash.utils.IDataInput, bytesAfterSlice:uint):void {
			var world$count:uint = 0;
			while (input.bytesAvailable > bytesAfterSlice) {
				var tag:uint = com.protobuf.ReadUtils.read$TYPE_UINT32(input);
				switch (tag >> 3) {
				case 3:
					if (world$count != 0) {
						throw new flash.errors.IOError('Bad data format: ListRoleRequest.world cannot be set twice.');
					}
					++world$count;
					this.world = com.protobuf.ReadUtils.read$TYPE_INT32(input);
					break;
				default:
					super.readUnknown(input, tag);
					break;
				}
			}
		}

	}
}
