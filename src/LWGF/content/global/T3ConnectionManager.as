package LWGF.content.global
{
	import BlackCatWorkshop.ASDispatcher.DataEvent;
	
	import com.protobuf.Message;
	
	import LWGF.framework.network.ConnectManager;
	import LWGF.framework.network.MessageEndpoint;
	import LWGF.framework.network.WrappedSocket;
	import LWGF.framework.utils.Log;
	import LWGF.content.protocol.LotillMsg;
	
	/**
	 * 连接管理器
	 */
	public class T3ConnectionManager extends ConnectManager
	{
		private static var _instance:T3ConnectionManager;
		
		private var _accountSocket:WrappedSocket;
		private var _gameSocket:WrappedSocket;
		
		public function T3ConnectionManager()
		{
			super();
			
			addSocket("account", LotillMsg);
			addSocket("game", LotillMsg);
			
			_accountSocket = getSocket("account");
			_gameSocket = getSocket("game");
		}
		
		public static function get instance():T3ConnectionManager
		{
			if(_instance == null)
			{
				_instance = new T3ConnectionManager();
			}
			
			return _instance;
		}
		
		private var accountHost:String;
		private var accountPort:uint;
		private var gameHost:String;
		private var gamePort:uint;
		public function initInfo(accountHost:String, accountPort:uint, gameHost:String, gamePort:uint):void
		{
			this.accountHost = accountHost;
			this.accountPort = accountPort;
			this.gameHost = gameHost;
			this.gamePort = gamePort;
		}
		
		/**
		 * 连接账号服务器的套接字对象
		 */
		public function get accountSocket():WrappedSocket
		{
			return _accountSocket;
		}
		
		/**
		 * 连接游戏服务器的套接字对象
		 */
		public function get gameSocket():WrappedSocket
		{
			return _gameSocket;
		}
		
		public function accountConnect():void
		{
			var host:String = accountHost;
			var port:uint = accountPort;
			
			_accountSocket.connect(host, port);
		}
		
		public function accountDiscount():void
		{
			_accountSocket.disconnect();
			
			Log.traceLog("Account Server Disconnected.", "network");
		}
		
		public function gameConnect():void
		{
			var host:String = gameHost;
			var port:uint = gamePort;
			
			_gameSocket.connect(host, port);
		}
		
		override protected function onMessage(event:DataEvent):void
		{
			super.onMessage(event);
			
			var msg:LotillMsg = event.data as LotillMsg;
			Log.traceLog("msg_id=" + msg.msgid, "network"); 
		}
		
		override protected function dispatch(msg:Message):void
		{
			var message:LotillMsg = msg as LotillMsg;
			for each(var endpoint:MessageEndpoint in _dispatcher[message.msgid])
			{
				endpoint.process(msg);
			}
//			var dispatcher:MessageDispatcher = _dispatcher[message.msgid] as MessageDispatcher;
//			dispatcher.dispatch(message);
		}
	}
}