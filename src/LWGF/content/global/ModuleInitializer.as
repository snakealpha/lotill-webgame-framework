package LWGF.content.global
{
	import LWGF.framework.controller.Controller;
	import LWGF.content.global.data.ModuleConsts;
	import LWGF.content.global.data.config.ModuleConfig;
	import LWGF.content.vo.ModuleInformation;

	/**
	 * 模块管理器
	 * 主要用于在游戏启动时根据各模块信息初始化各模块状态
	 */
	public class ModuleInitializer
	{	
		private static var _instance:ModuleInitializer;
		
		public static function get instance():ModuleInitializer
		{
			if(_instance == null)
			{
				_instance = new ModuleInitializer();
			}
			
			return _instance;
		}
		
		public function ModuleInitializer()
		{
		}
		
		public function initModules():void
		{
			var configs:ModuleConfig = DataManager.getConfig(ModuleConsts.module) as ModuleConfig;
			
			for each(var config:ModuleInformation in configs.moduleInfos)
			{
				var controller:Controller = GameControllers.instance.getControllerByModuleName(config.name);
				
				if(config.autoDisplay)
				{
					controller.showView();
				}
			}
		}
	}
}