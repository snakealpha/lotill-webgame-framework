package LWGF.content.global
{
	import LWGF.framework.utils.configLoader.ConfigManager;
	
	public class T3ConfigManager extends LWGF.framework.utils.configLoader.ConfigManager
	{
		private static var _instance:T3ConfigManager
		
		public static const baseConfigFile:String = "config.data";
		
		public function T3ConfigManager()
		{
			super();
			
			load(baseConfigFile);
		}
		
		public static function get instance():T3ConfigManager
		{
			if(_instance == null)
			{
				_instance = new T3ConfigManager();
			}
			
			return _instance;
		}
		
		public function getConfigTab(tabName:String):Array
		{
			var tab:Array = configs[tabName+"_Tab"] as Array;
			return tab;
		}
	}
}