package LWGF.content.global
{
	import LWGF.framework.global.Controllers;
	import LWGF.content.module.debug.DebugController;
	import LWGF.content.module.navigationBar.NaviBarController;
	import LWGF.content.module.port.PortController;
	import LWGF.content.module.worldMap.WorldMapController;
	
	/**
	 * 游戏中控制器的集合
	 */
	public class GameControllers extends Controllers
	{
		protected static var _instance:GameControllers;
		
		public function GameControllers()
		{
			super();
			
			addController(new DebugController());
			addController(new NaviBarController());
			addController(new WorldMapController());
			addController(new PortController());
		}
		
		public static function get instance():GameControllers
		{
			if(_instance == null)
			{
				_instance = new GameControllers();
			}
			
			return _instance;
		}
	}
}