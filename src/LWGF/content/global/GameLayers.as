package LWGF.content.global
{
	import LWGF.framework.BaseComponent.layer.Layer;
	import LWGF.framework.global.GlobalInfo;
	
	/**
	 * 用于保存游戏中的所有层次
	 * 
	 * 适用于可能要移出Starling支持的情况：
	 * 该类包含Starling支持代码
	 */
	public class GameLayers
	{
		private static var _instance:GameLayers;
		
		public var senceLayer:Layer = new Layer();
		public var windowLayer:Layer = new Layer();
		public var uiLayer:Layer = new Layer();
		public var notifyLayer:Layer = new Layer();
		public var tooltipLayer:Layer = new Layer();
//		
//		public var starlingLayer:StarlingLayer;
		
		public function GameLayers()
		{
			super();
			
			init();
		}
		
		private function init():void
		{
			GlobalInfo.stage.addChild(senceLayer);
			GlobalInfo.stage.addChild(windowLayer);
			GlobalInfo.stage.addChild(uiLayer);
			GlobalInfo.stage.addChild(notifyLayer);
			GlobalInfo.stage.addChild(tooltipLayer);
		}
		
		public static function get instance():GameLayers
		{
			if(_instance == null)
			{
				_instance = new GameLayers();
			}
			
			return _instance;
		}
	}
}