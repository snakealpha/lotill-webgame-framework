package LWGF.content.global.network
{
	import LWGF.content.global.network.proxy.LoginProxy;
	import LWGF.content.global.network.proxy.RoleProxy;

	/**
	 * 此处用于收集独立的Proxys
	 */
	public class GameProxys
	{		
		public static var loginProxy:LoginProxy = new LoginProxy();
		public static var roleProxy:RoleProxy = new RoleProxy();
		
		public function GameProxys()
		{
		}
	}
}