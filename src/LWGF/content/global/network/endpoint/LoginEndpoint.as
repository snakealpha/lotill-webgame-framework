package LWGF.content.global.network.endpoint
{
	import com.protobuf.Message;
	
	import LWGF.framework.network.MessageEndpoint;
	import LWGF.content.protocol.MessageID;
	
	public class LoginEndpoint extends MessageEndpoint
	{
		public function LoginEndpoint()
		{
			super();
		}
		
		override public function get supportMessageTypes():Array
		{
			return [MessageID.MSGID_LOGIN_RESPONSE,
					MessageID.MSGID_LOGIN_SHIP_NOTIFY,
					MessageID.MSGID_LOGOUTSERVER_NOTIFY];
		}
		
		override public function get moduleName():String
		{
			return "login";
		}
	}
}