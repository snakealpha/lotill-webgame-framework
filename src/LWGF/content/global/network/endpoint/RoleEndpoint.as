package LWGF.content.global.network.endpoint
{
	import com.protobuf.Message;
	
	import LWGF.framework.network.MessageEndpoint;
	import LWGF.content.protocol.MessageID;
	
	public class RoleEndpoint extends MessageEndpoint
	{
		public function RoleEndpoint()
		{
			super();
		}
		
		override public function get moduleName():String
		{
			return "role";
		}
		
		override public function get supportMessageTypes():Array
		{
			return [MessageID.MSGID_LISTROLE_RESPONSE,
					MessageID.MSGID_CREATEROLE_RESPONSE];
		}
	}
}