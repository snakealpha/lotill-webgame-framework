package LWGF.content.global.network.proxy
{
	import LWGF.content.global.T3ConnectionManager;
	import LWGF.content.global.network.T3Proxy;
	import LWGF.content.protocol.LoginPlatform;
	import LWGF.content.protocol.LoginRequest;
	import LWGF.content.protocol.LotillMsg;
	import LWGF.content.protocol.MessageID;
	
	public class LoginProxy extends T3Proxy
	{
		public function LoginProxy()
		{
			super(T3ConnectionManager.instance.gameSocket);
		}
		
		public function requestLogin(account:String, uin:uint):void
		{
			var msg:LotillMsg = new LotillMsg();
			msg.msgid = MessageID.MSGID_LOGIN_REQUEST;
			msg.uin = uin;
			msg.platform = LoginPlatform.LOGIN_PLATFORM_LOTILL;
			msg.account = account;
			
			var req:LoginRequest = new LoginRequest();
			req.uin = uin;
			
			msg.loginRequest = req;
			send(msg);
		}
	}
}