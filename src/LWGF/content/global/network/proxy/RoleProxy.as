package LWGF.content.global.network.proxy
{
	import LWGF.framework.global.GlobalInfo;
	import LWGF.content.global.T3ConnectionManager;
	import LWGF.content.global.network.T3Proxy;
	import LWGF.content.protocol.CreateRoleRequest;
	import LWGF.content.protocol.ListRoleRequest;
	import LWGF.content.protocol.LoginPlatform;
	import LWGF.content.protocol.LotillMsg;
	import LWGF.content.protocol.MessageID;
	
	/**
	 * 用于列出角色以及创建角色的Proxy
	 */
	public class RoleProxy extends T3Proxy
	{
		public function RoleProxy()
		{
			super(T3ConnectionManager.instance.accountSocket);
		}
		
		public function requestRole(account:String):void
		{
			var msg:LotillMsg = new LotillMsg();
			msg.msgid = MessageID.MSGID_LISTROLE_REQUEST;
			msg.account = account;
			msg.platform = LoginPlatform.LOGIN_PLATFORM_LOTILL;
			msg.uin = 0;
			
			var req:ListRoleRequest = new ListRoleRequest();
			
			msg.listRoleRequest = req;
			send(msg);
		}
		
		public function createRole(account:String, gender:int, avatar:int, name:String):void
		{
			var msg:LotillMsg = new LotillMsg();
			msg.msgid = MessageID.MSGID_CREATEROLE_REQUEST;
			msg.account = account;
			msg.platform = LoginPlatform.LOGIN_PLATFORM_LOTILL;
			msg.uin = 0;
			
			var req:CreateRoleRequest = new CreateRoleRequest();
			req.account = account;
			req.avatar = avatar;
			req.gender = gender;
			req.name = name;
			
			msg.createRoleRequest = req;
			send(msg);
		}
	}
}