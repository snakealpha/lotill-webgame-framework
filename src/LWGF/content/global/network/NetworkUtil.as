package LWGF.content.global.network
{
	import LWGF.framework.global.GlobalInfo;
	import lotill.t3.global.data.GameEntities;
	import LWGF.content.protocol.LoginPlatform;
	import LWGF.content.protocol.LotillMsg;

	public class NetworkUtil
	{
		public function NetworkUtil()
		{
		}
		
		public static function getEnptyMessage(msgId:uint):LotillMsg
		{
			var msg:LotillMsg = new LotillMsg();
			msg.account = GlobalInfo.arguments.account;
			msg.uin = GameEntities.instance.user.uin;
			msg.platform = LoginPlatform.LOGIN_PLATFORM_LOTILL;
			msg.msgid = msgId;
			
			return msg;
		}
	}
}