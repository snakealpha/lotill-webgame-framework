package LWGF.content.global.network
{
	
	import com.protobuf.Message;
	
	import LWGF.framework.network.Proxy;
	import LWGF.framework.network.WrappedSocket;
	import LWGF.framework.utils.Log;
	import LWGF.content.protocol.LotillMsg;

	public class T3Proxy extends Proxy
	{
		public function T3Proxy(socket:WrappedSocket)
		{
			super(socket);
		}
		
		override protected function send(msg:Message):void
		{
			super.send(msg);
			
			var lotillMsg:LotillMsg = msg as LotillMsg;
			Log.traceLog("send message id="+lotillMsg.msgid);
		}
	}
}