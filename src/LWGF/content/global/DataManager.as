package LWGF.content.global
{
	import flash.utils.Dictionary;
	
	import LWGF.framework.data.Config;
	import LWGF.framework.data.Entity;

	/**
	 * 该类提供对于所有模块的Config类和Entity类的引用。
	 * 因T3采用各模块功能自制策略而非与hug类似的集中管理策略，引入该类以减少不同模块控制器与视图之间的引用。
	 */
	public class DataManager
	{
		protected static var _instance:DataManager;
		
		protected var configs:Dictionary = new Dictionary();
		protected var entities:Dictionary = new Dictionary();
		
		public function DataManager()
		{
		}
		
		protected static function get instance():DataManager
		{
			if(_instance == null)
			{
				_instance = new DataManager();
			}
			
			return _instance;
		}
		
		public static function registerConfig(config:Config):void
		{
			instance.configs[config.moduleName] = config;
		}
		
		public static function registerEntity(entity:Entity):void
		{
			instance.entities[entity.moduleName] = entity;
		}
		
		public static function getConfig(module:String):Config
		{
			return instance.configs[module] as Config;
		}
		
		public static function getEntity(module:String):Entity
		{
			return instance.entities[module] as Entity;
		}
	}
}