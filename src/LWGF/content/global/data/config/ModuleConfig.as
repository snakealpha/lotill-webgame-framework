package LWGF.content.global.data.config
{
	import flash.utils.Dictionary;
	
	import LWGF.framework.data.Config;
	import LWGF.framework.utils.ObjectUtils;
	import LWGF.content.global.T3ConfigManager;
	import LWGF.content.global.data.ModuleConsts;
	import LWGF.content.vo.ModuleInformation;
	
	/**
	 * 模块信息配置
	 */
	public class ModuleConfig extends Config
	{
		private var _moduleInfo:Dictionary = new Dictionary();
		
		public function ModuleConfig()
		{
			super();
		}
		
		override protected function loadConfig():void
		{
			var configTab:Array = T3ConfigManager.instance.getConfigTab("ModuleInformation");
			for each(var infoObj:Object in configTab)
			{
				var info:ModuleInformation = new ModuleInformation();
				ObjectUtils.putObject(infoObj, info);
				_moduleInfo[info.name] = info;
			}
			configTab.length = 0;
		}
		
		override public function get configName():String
		{
			return T3ConfigManager.baseConfigFile;
		}
		
		override public function get moduleName():String
		{
			return ModuleConsts.module;
		}
		
		/**
		 * 通过模块名称获取模块信息
		 */
		public function getModuleInfoByName(name:String):ModuleInformation
		{
			if(_moduleInfo.hasOwnProperty(name))
			{
				return _moduleInfo[name] as ModuleInformation;
			}
			
			return null;
		}
		
		/**
		 * 所有模块名称的字典
		 */
		public function get moduleInfos():Dictionary
		{
			return _moduleInfo;
		}
	}
}