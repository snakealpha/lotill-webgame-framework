package LWGF.content.global.data.config
{
	import flash.utils.Dictionary;
	
	import LWGF.framework.data.Config;
	import LWGF.framework.utils.ObjectUtils;
	import LWGF.content.global.T3ConfigManager;
	import LWGF.content.global.data.ModuleConsts;
	import LWGF.content.vo.ErrorInfo;
	
	public class ErrorConfig extends Config
	{
		private var _errors:Dictionary = new Dictionary();
		
		public function ErrorConfig()
		{
			super();
		}
		
		override protected function loadConfig():void
		{			
			var configTab:Array = T3ConfigManager.instance.getConfigTab("ErrorInfo");
			for each(var infoObj:Object in configTab)
			{
				var info:ErrorInfo = new ErrorInfo();
				ObjectUtils.putObject(infoObj, info);
				_errors[info.id] = info;
			}
			configTab.length = 0;
		}
		
		public function getDescWithId(err_id:uint):String
		{
			return _errors[err_id].desc;
		}
		
		override public function get configName():String
		{
			return "config.data";
		}
		
		override public function get moduleName():String
		{
			return ModuleConsts.error;
		}
	}
}