package LWGF.content.global.data.entity
{
	import LWGF.framework.data.Entity;
	import LWGF.content.global.data.ModuleConsts;
	
	/**
	 * 该实体用于保存登陆与用户信息 
	 */
	public class UserEntity extends Entity
	{
		public var uin:uint = 0;
		
		public var gender:int;
		
		public var avatar:int;
		
		public var name:String;
		
		public var createTime:uint;
		
		public var lastLogin:uint;
		
		public var lastLogout:uint;
		
		public var exp:uint;
		
		public var lv:uint;
		
		public function UserEntity()
		{
			super();
		}
		
		override public function get moduleName():String
		{
			return ModuleConsts.user;
		}
	}
}