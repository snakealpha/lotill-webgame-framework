package LWGF.content.global.data
{
	public class ModuleConsts
	{
		/**
		 * 模块常数
		 */
		public static const debug:String = "debugModule";
		public static const navigationBar:String = "naviBarModule";
		public static const worldMap:String = "worldMapModule";
		public static const port:String = "portModule";
		
		/**
		 * 独立于模块的数据和网络设施常数
		 */
		public static const error:String = "error";
		public static const module:String = "module";
		public static const user:String = "user";
		
		public function ModuleConsts()
		{
		}
	}
}