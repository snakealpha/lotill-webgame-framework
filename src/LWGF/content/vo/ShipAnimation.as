package LWGF.content.vo
{
    public class ShipAnimation
    {
        public var name:String;

        public var res:String;

        public var frameNum:uint;

        public var stay:uint;

        public var basePointX:uint;

        public var basePointY:uint;

        public var rawFloat:uint;

        public var float:Boolean;

        public var rawFloatX:String;

        public var rawFloatY:String;

        public var floatX:Array = [];

        public var floatY:Array = [];

    }
}