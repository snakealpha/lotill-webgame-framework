package LWGF.content.vo
{
    public class PortInfo
    {
        public var id:uint;

        public var name:String;

        public var scale:String;

        public var type:String;

        public var countary:String;

        public var posX:int;

        public var posY:int;

    }
}