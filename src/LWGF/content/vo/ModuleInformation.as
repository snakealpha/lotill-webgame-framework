package LWGF.content.vo
{
    public class ModuleInformation
    {
        public var name:String;

        public var autoDisplay:Boolean;

        public var isCenter:Boolean;

        public var posX:int;

        public var posY:int;

        public var resource:String;

    }
}