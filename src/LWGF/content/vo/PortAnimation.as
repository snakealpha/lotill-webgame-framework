package LWGF.content.vo
{
    public class PortAnimation
    {
        public var name:String;

        public var res:String;

        public var frameWidth:Number;

        public var frameHeight:Number;

        public var frameNum:uint;

        public var stay:uint;

        public var useBasePoint:Boolean;

        public var basePointX:uint;

        public var basePointY:uint;

    }
}