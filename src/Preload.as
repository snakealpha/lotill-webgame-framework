package
{
	import com.protobuf.Message;
	
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.Socket;
	import flash.net.URLRequest;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;
	import flash.text.TextField;
	import flash.utils.ByteArray;
	import flash.utils.Endian;
	
	import LWGF.framework.utils.Log;
	import LWGF.content.protocol.CreateRoleRequest;
	import LWGF.content.protocol.CreateRoleResponse;
	import LWGF.content.protocol.Gender;
	import LWGF.content.protocol.ListRoleRequest;
	import LWGF.content.protocol.ListRoleResponse;
	import LWGF.content.protocol.LoginPlatform;
	import LWGF.content.protocol.LotillMsg;
	import LWGF.content.protocol.MessageID;
	import LWGF.content.protocol.RoleSummary;
	
	/**
	 * 加载器类
	 */	
	[SWF(width="1000", height="600", frameRate="30")]
	public class Preload extends Sprite
	{
		private var loadingBar:MovieClip;
		private var processField:TextField;
		
		private var game:Object;
		
		private var isAccountInfoGot:Boolean = false;
		
		private var isGameLoaded:Boolean = false;
		private var isCreateRoleLoaded:Boolean = false;
		
		private var enteringCreateRole:Boolean = false;
		
		private var globalStage:Stage;
		
		private var socket:Socket;
		
		private var gameLoader:Loader;
		private var createLoader:Loader;
		
		private var createView:CreateRole;
		
		private var uin:uint;
		
		/**
		 * 预加载器
		 */
		public function Preload()
		{
			super();
			
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
					
			// 开始加载初始loading条
			gameLoader = new Loader();			
			gameLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onPreloaderLoaded);
			gameLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onGameIoError);
			gameLoader.contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onGameSecrityError);			
			gameLoader.load(new URLRequest("asserts/loading.swf"), new LoaderContext(false, ApplicationDomain.currentDomain));
			
			//初始化网络监听
			socket = new Socket(loaderInfo.parameters.accountServerIp, parseInt(loaderInfo.parameters.accountPort));
			socket.addEventListener(Event.CONNECT, onAccountConnected);
			socket.addEventListener(IOErrorEvent.IO_ERROR, onAccountIoError);
			socket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onAccountSecurityError);
			socket.addEventListener(Event.CONNECT, onAccountConnected);
			socket.addEventListener(Event.CONNECT, onAccountConnected);
			socket.connect(loaderInfo.parameters.accountServerIp, parseInt(loaderInfo.parameters.accountPort));
			
//			T3ConnectionManager.instance.addDispatcher(new RoleDispatcher());
//			
//			T3ConnectionManager.instance.accountConnect();
//			T3ConnectionManager.instance.accountSocket.addEventListener(WrappedSocket.Connected, onAccountConnected);
		}
		
		private function onAccountIoError(event:IOErrorEvent):void
		{
			
		}
		
		private function onAccountSecurityError(event:SecurityErrorEvent):void
		{
			
		}
		
		private function onAdded(event:Event):void
		{
			globalStage = stage;
		}
		
		// Loading条加载完成，开始加载全局界面资源
		private function onPreloaderLoaded(event:Event):void
		{
			loadingBar = gameLoader.content as MovieClip;
			processField = loadingBar.processField as TextField;			
			addChild(loadingBar);
			
			gameLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onPreloaderLoaded);
			gameLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onGlobalResLoaded);
			gameLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onGlobalResProcess);
			gameLoader.load(new URLRequest("asserts/globalUI.swf"), new LoaderContext(false, ApplicationDomain.currentDomain));
		}
		// 全局界面资源加载完成，开始加载游戏本体
		private function onGlobalResLoaded(event:Event):void
		{
			gameLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onGlobalResLoaded);
			gameLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onGameLoaded);
			gameLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onGameProcess);
			gameLoader.load(new URLRequest("game.swf"), new LoaderContext(false, ApplicationDomain.currentDomain));
		}
		// 游戏本体加载完成，开司尝试登录游戏
		private function onGameLoaded(event:Event):void
		{
			isGameLoaded = true;
			
			game = gameLoader.content;
			
			checkAndLogin();
		}
		protected function onGameIoError(event:IOErrorEvent):void
		{
			
		}
		// 全局资源加载进度维护
		protected function onGlobalResProcess(event:ProgressEvent):void
		{
			processField.text = ((event.bytesLoaded / event.bytesTotal) * 50).toFixed() + "%";
		}
		// 游戏本体加载进度维护
		protected function onGameProcess(event:ProgressEvent):void
		{
			processField.text = ((event.bytesLoaded / event.bytesTotal) * 50 + 50).toFixed() + "%";
		}
		protected function onGameSecrityError(event:SecurityErrorEvent):void
		{
			
		}
		// 游戏已启动，
		private function startGame():void
		{
			if(parent == globalStage)
			{
				globalStage.removeChild(this);
			}
			globalStage.addChild(gameLoader.content);
		}
		
		//进入创建角色流程
		private function enterCreateRole():void
		{
			createLoader = new Loader();
			
			createLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onCreateRoleLoaded);
			createLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onGameIoError);
			createLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onGameProcess);
			createLoader.contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onGameSecrityError);
			
			createLoader.load(new URLRequest("createRole.swf"), new LoaderContext(false, ApplicationDomain.currentDomain));
		}
		
		//已加载创建角色资源
		private function onCreateRoleLoaded(event:Event):void
		{
			if(loadingBar.parent == this)
			{
				removeChild(loadingBar);
			}
			
			createView = new CreateRole(createLoader.content as MovieClip);
			createView.createButton.addEventListener(MouseEvent.CLICK, onCreateRole);
			addChild(createView);
		}
		
		private function onCreateRole(event:MouseEvent):void
		{
			createRole(loaderInfo.parameters.account, createView.genderSelector.selected?Gender.FEMAN:Gender.MAN, 0, createView.nameField.text);
		}
		
		private function onCreatedRole(msg:LotillMsg):void
		{			
			if(msg.msgid != MessageID.MSGID_CREATEROLE_RESPONSE)
			{
				Log.traceLog("Create Role Response Error. No CreateRoleResponse Received.", "role");
				return;
			}
			
			var resp:CreateRoleResponse = msg.createRoleResponse;
			if(resp.result != 0)
			{
				Log.traceLog("Create Role Response Error. Error ID Is Not Zero.", "role");
				return;
			}
			
			removeChild(createView);
			isAccountInfoGot = true;
			checkAndLogin();
		}
		
		//该事件处理程序在与帐号服务器连接之后调用
		private function onAccountConnected(event:Event):void
		{
			Log.traceLog("[Preload] account server connected.", "network");
			socket.removeEventListener(Event.CONNECT, onAccountConnected);
			socket.addEventListener(ProgressEvent.SOCKET_DATA, onSocketData);
			requestRole(loaderInfo.parameters.account);
		}
		
		//获取列出角色信息后处理
		private function onListedRole(/*event:DataEvent*/msg:LotillMsg):void
		{
			Log.traceLog("[Preload] list role response received.", "network");
			
			if(msg.msgid != MessageID.MSGID_LISTROLE_RESPONSE)
			{
				Log.traceLog("List Role Response Error. No ListRoleResponse Received.", "role");
				return;
			}
			
			var resp:ListRoleResponse = msg.listRoleResponse;
			if(resp.result != 0)
			{
				Log.traceLog("List Role Response Error. Error ID Is Not Zero.", "role");
				return;
			}
			
			if(resp.roleSummary.length == 0)
			{
				enterCreateRole();
			}
			else
			{
				var role:RoleSummary = resp.roleSummary[0] as RoleSummary;
				
				uin = role.uin;
				
				socket.close();
				Log.traceLog("Account Disconnected.", "role");
				isAccountInfoGot = true;
				
				checkAndLogin();
			}
		}
		
		//开启登陆流程
		private function checkAndLogin():void
		{
			if(game == null || !isAccountInfoGot)
				return;
			
			game.params = loaderInfo.parameters;
			game.params.uin = uin;
			
			(game as IEventDispatcher).addEventListener("logined", onLogined);
			game.login();
		}
		
		private function onLogined(event:Event):void
		{		
			Log.traceLog("[Preload] login response received.", "network");
			(game as IEventDispatcher).removeEventListener("logined", onLogined);
			startGame();
		}
		
		//为减小体积，使用自己的网络系方法		
		private function send(msg:Message):void
		{
			var buffer:ByteArray = new ByteArray();
			msg.writeTo(buffer);
			socket.endian = Endian.BIG_ENDIAN;
			socket.writeShort(buffer.length + 2);
			socket.writeBytes(buffer);
			socket.flush();
		}
		
		private function requestRole(account:String):void
		{
			var msg:LotillMsg = new LotillMsg();
			msg.msgid = MessageID.MSGID_LISTROLE_REQUEST;
			msg.account = account;
			msg.platform = LoginPlatform.LOGIN_PLATFORM_LOTILL;
			msg.uin = 0;
			
			var req:ListRoleRequest = new ListRoleRequest();
			
			msg.listRoleRequest = req;
			
			Log.traceLog("[Preload] list role request sent.", "role");
			
			send(msg);
		}
		
		private function createRole(account:String, gender:int, avatar:int, name:String):void
		{
			var msg:LotillMsg = new LotillMsg();
			msg.msgid = MessageID.MSGID_CREATEROLE_REQUEST;
			msg.account = account;
			msg.platform = LoginPlatform.LOGIN_PLATFORM_LOTILL;
			msg.uin = 0;
			
			var req:CreateRoleRequest = new CreateRoleRequest();
			req.account = account;
			req.avatar = avatar;
			req.gender = gender;
			req.name = name;
			
			msg.createRoleRequest = req;		
			Log.traceLog("[Preload] create role request sent.", "network");
			send(msg);
		}
		
		private function onSocketData(event:Event):void
		{
			var msgQueue:Array = [];
			var msg:LotillMsg;
			
			while(socket.bytesAvailable != 0)
			{
				msg = new LotillMsg();
				msg.mergeFrom(socket);
				msgQueue.push(msg);
			}
			
			while(msgQueue.length != 0)
			{
				msg = msgQueue.shift() as LotillMsg;
				
				Log.traceLog("get message id:" + msg.msgid, "network");
				
				switch(msg.msgid)
				{
					case MessageID.MSGID_LISTROLE_RESPONSE:
					{
						onListedRole(msg);
						break;
					}
					case MessageID.MSGID_CREATEROLE_RESPONSE:
					{
						onCreatedRole(msg);
						break;
					}
				}
			}
		}
	}
}