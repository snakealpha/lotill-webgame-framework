package
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.text.TextField;
	
	import LWGF.framework.uiComponent.Button;
	import LWGF.framework.uiComponent.utils.ButtonUtils;
	import LWGF.framework.utils.assertManager.AssertManager;
	import LWGF.framework.utils.assertManager.LoadingInfo;
	
	public class CreateRole extends Sprite
	{
		private var _mc:MovieClip;
		
		private var _genderSelector:Button;
		private var _nameField:TextField;
		private var _create:Button;
		
		public function CreateRole(displayRes:MovieClip)
		{
			super();
			
			_mc = displayRes;
			initView();
			addChild(_mc);
		}
		
		private function initView():void
		{
			_genderSelector = ButtonUtils.replaceMovieClipWithCheckedButton(_mc.genderSelector, "gender");
			_nameField = _mc.nameField;
			_create = ButtonUtils.replaceMovieClipWithButton(_mc.createRole);
		}
		
		public function get genderSelector():Button
		{
			return _genderSelector;
		}
		
		public function get nameField():TextField
		{
			return _nameField;
		}
		
		public function get createButton():Button
		{
			return _create;
		}
	}
}