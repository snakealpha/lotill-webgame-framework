package
{
	import BlackCatWorkshop.ASBinding.BindingUtils;
	import BlackCatWorkshop.ASBinding.OneWayBinding;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	
	import LWGF.framework.animation.Animation;
	import LWGF.framework.animation.AnimationMode;
	import LWGF.framework.animation.defaultDrivers.ComplexAnimationDriver;
	import LWGF.framework.animation.defaultDrivers.SimpleAnimationDriver;
	import LWGF.framework.uiComponent.Button;
	import LWGF.framework.uiComponent.utils.ButtonUtils;
	import LWGF.framework.utils.ScaleBitmap;
	import LWGF.framework.utils.assertManager.AssertManager;
	import LWGF.framework.utils.assertManager.LoadingInfo;
	import LWGF.framework.utils.timer.TimerManager;
	
	[SWF(width="550", height="400", frameRate="30")]
	public class Test extends Sprite
	{
		public function Test()
		{
			AssertManager.load("asserts/fakeRes.swf", LoadingInfo.MovieClip, onGotFakeRes);
//			ConfigManager.instance.load("config.data");
		}
		
		private var binding:OneWayBinding;
		private var fakeHolder:Object;
		public function onGotFakeRes(loading:LoadingInfo):void
		{
			var fakeRes:MovieClip = AssertManager.getMovieClip("asserts/fakeRes.swf");
			var fakeButton:Button = ButtonUtils.replaceMovieClipWithButton(fakeRes.fakeButton);
			addChild(fakeRes);
			
			fakeHolder = {time:0};
			binding = BindingUtils.createOneWayBinding(fakeHolder, "time", fakeRes.textTest, "text", onAddEnd);
			
			TimerManager.instance.registerTimer(1000, 0, onTimer);
			
			var bm:ScaleBitmap = AssertManager.getScaleBitmap("sb_test");
			bm.scale9Grid = new Rectangle(15, 15, 150, 115);
			addChild(bm);
			bm.width = 250;
			bm.height = 250;
			
			var ani1:Animation = new Animation(138, 136, AssertManager.getBitmapData("boat"));
			var driver1:SimpleAnimationDriver = ani1.driver as SimpleAnimationDriver;
			driver1.maxFrame = 5;
			driver1.width = 138;
			driver1.height = 136;
			driver1.stayTime = 600;
			ani1.currentFrame = 1;
			addChild(ani1);
			ani1.play(1, true);
			
			var ani2:Animation = new Animation(83, 45, AssertManager.getBitmapData("fa_ani"), AnimationMode.Complex);
			var driver2:ComplexAnimationDriver = ani2.driver as ComplexAnimationDriver;
			driver2.frameInfo = [	{rectage:new Rectangle(0, 0, 83, 45), stayTime:350},
									{rectage:new Rectangle(83, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(166, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(249, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(332, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(415, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(498, 0, 83, 45), stayTime:140},
									{rectage:new Rectangle(581, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(664, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(747, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(830, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(913, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(996, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(1079, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(1162, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(1245, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(1328, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(1411, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(1494, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(1577, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(1660, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(1743, 0, 83, 45), stayTime:70}
								];
			ani2.currentFrame = 1;
			ani2.x = 22;
			ani2.y = 65;
			addChild(ani2);
			ani2.play(1, true);
			
			var ani3:Animation = new Animation(83, 45, AssertManager.getBitmapData("fa_ani"), AnimationMode.Complex);
			var driver3:ComplexAnimationDriver = ani3.driver as ComplexAnimationDriver;
			driver3.frameInfo = [	{rectage:new Rectangle(0, 0, 83, 45), stayTime:350},
//									{rectage:new Rectangle(83, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(166, 0, 83, 45), stayTime:105},
//									{rectage:new Rectangle(249, 0, 83, 45), stayTime:70},
//									{rectage:new Rectangle(332, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(415, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(498, 0, 83, 45), stayTime:175},
//									{rectage:new Rectangle(581, 0, 83, 45), stayTime:70},
//									{rectage:new Rectangle(664, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(747, 0, 83, 45), stayTime:70},
//									{rectage:new Rectangle(830, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(913, 0, 83, 45), stayTime:70},
//									{rectage:new Rectangle(996, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(1079, 0, 83, 45), stayTime:125},
//									{rectage:new Rectangle(1162, 0, 83, 45), stayTime:70},
//									{rectage:new Rectangle(1245, 0, 83, 45), stayTime:70},
//									{rectage:new Rectangle(1328, 0, 83, 45), stayTime:70},
//									{rectage:new Rectangle(1411, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(1494, 0, 83, 45), stayTime:100},
									{rectage:new Rectangle(1577, 0, 83, 45), stayTime:70},
//									{rectage:new Rectangle(1660, 0, 83, 45), stayTime:70},
									{rectage:new Rectangle(1743, 0, 83, 45), stayTime:70}
								];
			ani3.currentFrame = 1;
			ani3.x = 22;
			ani3.y = 120;
			addChild(ani3);
			ani3.play(1, true);
		}
		
		private function onAddEnd(source:Object):Object
		{
			return source.toString()+" cats";
		}
		
		private function onTimer():void
		{
			binding.source.setValue(fakeHolder.time + 1);
		}
	}
}