package
{
	import BlackCatWorkshop.ASDispatcher.DataEvent;
	import BlackCatWorkshop.ASDispatcher.Dispatcher;
	
	import flash.display.Sprite;
	import flash.events.Event;
	
	import LWGF.framework.global.GlobalInfo;
	import LWGF.framework.network.WrappedSocket;
	import LWGF.framework.uiComponent.tooltip.TooltipManager;
	import LWGF.framework.utils.Log;
	import LWGF.framework.utils.configLoader.ConfigManager;
	import LWGF.content.global.DataManager;
	import LWGF.content.global.GameControllers;
	import LWGF.content.global.GameLayers;
	import LWGF.content.global.ModuleInitializer;
	import LWGF.content.global.T3ConfigManager;
	import LWGF.content.global.T3ConnectionManager;
	import LWGF.content.global.data.ModuleConsts;
	import LWGF.content.global.data.config.ErrorConfig;
	import LWGF.content.global.data.config.ModuleConfig;
	import LWGF.content.global.data.entity.UserEntity;
	import LWGF.content.global.network.GameProxys;
	import LWGF.content.global.network.endpoint.LoginEndpoint;
	import LWGF.content.protocol.LoginResponse;
	import LWGF.content.protocol.LotillMsg;
	import LWGF.content.protocol.MessageID;
	import LWGF.content.uiComponent.utils.alert.AlertCallbackParameter;
	import LWGF.content.uiComponent.utils.stateTip.BasicStateTip;
	import LWGF.content.uiComponent.utils.stateTip.StateTipManager;
	
	/**
	 * 适用于可能要移出Starling支持的情况：
	 * 该类包含Starling支持代码
	 */
	
	[SWF(width="1000", height="600", frameRate="25")]
	public class Game extends Sprite
	{
		public function Game()
		{
			initGame();
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void
		{
			GlobalInfo.stage = this.stage;
			GlobalInfo.arguments = loaderInfo.parameters;
			
			initGameView();
		}
		
		private function initGame():void
		{			
			//初始化网络组件	
			T3ConnectionManager.instance.addDispatcher(new LoginEndpoint());
			
			//初始化独立配置和实体
			DataManager.registerConfig(new ModuleConfig());
			DataManager.registerConfig(new ErrorConfig());
			DataManager.registerEntity(new UserEntity());
		}
		
		/**
		 * 通过依序建构游戏的各个主要组件建构游戏框架。
		 * 各调用顺序不应搅乱。
		 */
		private function initGameView():void
		{
			var layers:GameLayers = GameLayers.instance;
			var configManager:T3ConfigManager = T3ConfigManager.instance;
			var gameController:GameControllers = GameControllers.instance;
			
			// 初始化各模块的工作需要在配置完成加载以后再进行。
			// 因为各配置类监听的事件优先级高于默认优先级，故该方法还是会在所有配置管理类读取配置完成之后调用。
			Dispatcher.addEventListener(ConfigManager.ConfigLoaded, onConfigLoaded, "config");
			
			//配置Tooltip层
			TooltipManager.instance.tooltipLayer = GameLayers.instance.tooltipLayer;
		}
		
		private function onConfigLoaded(event:DataEvent):void
		{
			var configs:ModuleConfig = DataManager.getConfig(ModuleConsts.module) as ModuleConfig;
			ModuleInitializer.instance.initModules();
			
			//world map test
			GameControllers.instance.getControllerByModuleName(ModuleConsts.worldMap).showView();
//			StateTipManager.instance.popTip(BasicStateTip, "测试，测试，测试...喵了个咪的");
			//end test
		}
		
//		private function onInitStarlingLayer(event:starling.events.Event):void
//		{
//			GameLayers.instance.starlingLayer = GlobalInfo.starlingContext.root;
//		}
		
		//启动登陆流程
		public function login():void
		{
			T3ConnectionManager.instance.gameSocket.addEventListener(WrappedSocket.Connected, onGameConnected);
			T3ConnectionManager.instance.gameConnect();
		}
		
		//与游戏服务器建立连接后调用
		private function onGameConnected(event:DataEvent):void
		{			
			Dispatcher.addEventListener(WrappedSocket.MessageArrived, onLogined, "login");
			GameProxys.loginProxy.requestLogin(params.account, params.uin);
		}
		
		private var _params:Object = new Object();
		public function set params(value:Object):void
		{
			_params = value;
			
			T3ConnectionManager.instance.initInfo(	_params.accountServerIp, 
													parseInt(_params.accountPort), 
													_params.gameServerIp, 
													parseInt(_params.gamePort));
		}
		public function get params():Object
		{
			return _params;
		}
		
		private function onLogined(event:DataEvent):void
		{
			Dispatcher.removeEventListener(WrappedSocket.MessageArrived, onLogined, "login");
			
			var msg:LotillMsg = event.data as LotillMsg;
			if(msg.msgid != MessageID.MSGID_LOGIN_RESPONSE)
			{
				Log.traceLog("Login Response Error. No LoginResponse Received.", "login");
				return;
			}
			
			var resp:LoginResponse = msg.loginResponse;
			if(resp.result != 0)
			{
				Log.traceLog("Login Response Error. Error ID Is Not Zero.", "login");
				return;
			}
			
			var user:UserEntity = DataManager.getEntity(ModuleConsts.user) as UserEntity;
			user.uin = resp.roleinfo.uin;
			user.name = resp.roleinfo.name;
			user.avatar = resp.roleinfo.avatar;
			user.gender = resp.roleinfo.gender;
			user.createTime = resp.roleinfo.createTime;
			user.lastLogin = resp.roleinfo.lastLoginTime;
			user.lastLogout = resp.roleinfo.last_LogoutTime;
			user.exp = resp.roleinfo.exp;
			user.lv = resp.roleinfo.level;
			
			dispatchEvent(new Event("logined"));
		}
		
		private function onAlertReturn(param:AlertCallbackParameter):void
		{
			
		}
	}
}