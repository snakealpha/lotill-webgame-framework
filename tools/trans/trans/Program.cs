﻿using System;
using System.Text;
using System.Xml;
using System.Xml.Linq;

using BlackCatWorkshop.Merge;

namespace trans
{
    class Program
    {
        static void Main(string[] args)
        {
            string configFile;

            if (args.Length != 0)
            {
                configFile = args[0];
                StartTrans(configFile);
            }
        }

        private static void StartTrans(string configPath)
        {
            XElement rootElement = XElement.Load(configPath);
            ConvertContext context = ConvertContext.Parse(rootElement);
            foreach (ConvertTree tree in context.ConvertTrees)
            {
                foreach (ConvertNode node in tree)
                {
                    foreach (ResourceInformation info in node)
                    {
                        XmlWrapper.Generate(info);
                        info.GenerateAS3Type();
                        info.GenerateHeadType();
                    }
                }
            }

            context.GenerateASConsts();
            context.GenerateCConsts();
        }
    }
}
