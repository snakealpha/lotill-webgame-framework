﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace ConfigPackager
{
    class Program
    {
        static void Main(string[] args)
        {
            string configPath = "";
            string outputFile = "config.data";

            if (args.Length >= 1)
            {
                configPath = args[0];
            }
            if (args.Length >= 2)
            {
                outputFile = args[1];
            }

            GenerateDatafile(configPath, outputFile);
        }

        private static void GenerateDatafile(string configsPath, string outputFile)
        {
            DirectoryInfo configDir = new DirectoryInfo(configsPath);
            XElement root = new XElement("ConfigRoot");
            foreach (FileInfo file in configDir.GetFiles("*.xml"))
            {
                XElement config = XElement.Load(file.FullName);
                root.Add(config);
            }

            string outputText = root.ToString() + Environment.NewLine;

            FileStream outputStream = new FileStream(outputFile, FileMode.Create);
            ZLibNet.ZLibStream zlibStream = new ZLibNet.ZLibStream(outputStream, ZLibNet.CompressionMode.Compress);
            MemoryStream inputStream = new MemoryStream();
            StreamWriter writer = new StreamWriter(inputStream, Encoding.UTF8);
            writer.AutoFlush = true;
            writer.Write(outputText);

            byte[] buffer = inputStream.GetBuffer();
            zlibStream.Write(buffer, 0, buffer.Length);

            zlibStream.Flush();
        }
    }
}
